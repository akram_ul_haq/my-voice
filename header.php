<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!--Wrapper Content Start-->
<div class="tnit-wrapper">

    <!--Popup Overlay Outer Start-->
    <div class="tnit-popup-outer">
        <!-- Modal Box Start-->
        <div class="modal fade tnit-modal-outer" id="tnit-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="top">
                            <h5 class="modal-title"><a
                                        href="#"><?php esc_html_e( 'New to site?', 'myvoice' ); ?></a> <?php esc_html_e( 'Creat an Account', 'myvoice' ); ?>
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <h3><?php esc_html_e( 'Register', 'myvoice' ); ?></h3>
                    </div>
                    <div class="modal-body">
                        <!--Modal Form Start-->
						<?php echo do_shortcode( '[cr_custom_registration]' ); ?>
                        <!--Modal Form End-->
                    </div>

                </div>
            </div>
        </div><!-- Modal Box End-->
    </div>
    <!--Popup Overlay Outer End-->

    <!--Header Content Start-->
    <header class="tnit-header tnit-inner-header">
        <!--Topbar Row Start-->
        <div class="tnit-topbar-row">
            <div class="container">
                <!--Topbar content Start-->
                <div class="tnit-topbar-holder">
                    <!--Top Left Box Start-->
	                <?php if ( get_option( 'theme_header_phone' ) || get_option( 'theme_header_phone' ) ) : ?>
                        <!--Top Left Box Start-->
                        <ul class="tp-left-listed">
			                <?php
			                if ( get_option( 'theme_header_phone' ) ) {
				                echo '<li><a href="' .esc_html__('tel:', 'myvoice') . esc_html( get_option( 'theme_header_phone' ) ) . '"><i class="fa fa-phone" aria-hidden="true"></i>' . esc_html( get_option( 'theme_header_phone' ) ) . '</a></li>';
			                }
			                if ( get_option( 'theme_header_email' ) ) {
				                echo '<li><a href="mailto:' . esc_html( get_option( 'theme_header_email' ) ) . '" target="_top"><i class="fa fa-envelope" aria-hidden="true"></i>' . esc_html( get_option( 'theme_header_email' ) ) . '</a></li>';
			                }
			                ?>
                        </ul> <!--Top Left Box End-->
	                <?php endif; ?>
                    <!--Top Left Box End-->
                    <!--Top Right Start-->
                    <div class="tp-right">
		                <?php
		                if ( get_option( 'my_voice_login_text' ) && ! is_user_logged_in() ) {
			                echo '<a href="#" class="btn-account" data-toggle="modal" data-target="#tnit-modal">' . esc_html( get_option( 'my_voice_login_text' ) ) . '</a>';
		                } else {
			                $current_user = wp_get_current_user();
			                echo '<a href="' . esc_url( get_edit_user_link( $current_user->ID ) ) . '" class="btn-account">' . esc_html( $current_user->display_name ) . '</a>';
		                }
		                ?>

                        <!--Social Links start-->
		                <?php
		                $show_social = get_option( 'theme_show_social_menu' );
		                if ( 'true' === $show_social ) {
			                $sl_facebook  = get_option( 'theme_facebook_link' );
			                $sl_twitter   = get_option( 'theme_twitter_link' );
			                $sl_linkedin  = get_option( 'theme_linkedin_link' );
			                $sl_google    = get_option( 'theme_google_link' );
			                $sl_instagram = get_option( 'theme_instagram_link' );
			                $sl_skype     = get_option( 'theme_skype_username' );
			                $sl_youtube   = get_option( 'theme_youtube_link' );
			                $sl_pinterest = get_option( 'theme_pinterest_link' );
			                $sl_rss       = get_option( 'theme_rss_link' );
			                ?>
                            <ul class="tnit-social-links">
				                <?php
				                if ( ! empty( $sl_facebook ) ) {
					                ?>
                                    <li class="facebook">
                                        <a target="_blank" href="<?php echo esc_url( $sl_facebook ); ?>"><i
                                                    class="fa fa-facebook fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_twitter ) ) {
					                ?>
                                    <li class="twitter">
                                        <a target="_blank" href="<?php echo esc_url( $sl_twitter ); ?>"><i
                                                    class="fa fa-twitter fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_linkedin ) ) {
					                ?>
                                    <li class="linkedin">
                                        <a target="_blank" href="<?php echo esc_url( $sl_linkedin ); ?>"><i
                                                    class="fa fa-linkedin fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_google ) ) {
					                ?>
                                    <li class="gplus">
                                        <a target="_blank" href="<?php echo esc_url( $sl_google ); ?>"><i
                                                    class="fa fa-google-plus fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_instagram ) ) {
					                ?>
                                    <li class="instagram">
                                        <a target="_blank" href="<?php echo esc_url( $sl_instagram ); ?>"> <i
                                                    class="fa fa-instagram fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_youtube ) ) {
					                ?>
                                    <li class="youtube">
                                        <a target="_blank" href="<?php echo esc_url( $sl_youtube ); ?>"> <i
                                                    class="fa fa-youtube-square fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_skype ) ) {
					                ?>
                                    <li class="skype">
                                        <a target="_blank" href="skype:<?php echo esc_attr( $sl_skype ); ?>?add"> <i
                                                    class="fa fa-skype fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_pinterest ) ) {
					                ?>
                                    <li class="pinterest">
                                        <a target="_blank" href="<?php echo esc_url( $sl_pinterest ); ?>"> <i
                                                    class="fa fa-pinterest fa-lg"></i></a>
                                    </li>
					                <?php

				                }

				                if ( ! empty( $sl_rss ) ) {
					                ?>
                                    <li class="rss">
                                        <a target="_blank" href="<?php echo esc_url( $sl_rss ); ?>"> <i
                                                    class="fa fa-rss fa-lg"></i></a>
                                    </li>
					                <?php
				                }
				                ?>
                            </ul>
			                <?php
		                }
		                if ( function_exists( 'WC' ) ) {
			                ?>
                            <!--Social Links End-->
                            <!--Cart Holder Start-->
                            <div class="tnit-cart-holder">
                                <!--Button Cart-->
                                <div class="btn-cart">
                                    <span class="fa fa-cart-plus"></span>
                                    <span class="num"><?php echo esc_html( WC()->cart->get_cart_contents_count() ); ?></span>
                                    <!-- Cart Listed Start-->
                                    <ul class="tnit-cart-listed">
                                        <li>
                                            <div class="inner-cart">
                                                <div class="text">
                                                    <a href="<?php echo esc_url( wc_get_cart_url() ); ?>"
                                                       class="tnit-btn-cart">
                                                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                                                        Cart
                                                    </a>
                                                    <strong class="price">
										                <?php
										                esc_html_e( 'Total: ', 'myvoice' );
										                echo esc_html( WC()->cart->get_cart_total() );
										                ?>
                                                    </strong>
                                                </div>
                                            </div>
                                        </li>
                                    </ul><!-- Cart Listed End-->
                                </div><!--Button Cart End-->
                            </div>
		                <?php } ?>
                        <!--Cart Holder End-->
                    </div>
                </div>
                <!--Topbar content End-->
            </div>
        </div><!--Topbar Row Start-->
        <div class="tnit-logo-row">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
				        <?php

				        if ( ! has_custom_logo() ) :
					        if ( is_front_page() ) :
						        ?>
                                <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"
                                                          rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					        <?php else : ?>
                                <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"
                                                         rel="home"><?php bloginfo( 'name' ); ?></a></p>
					        <?php
					        endif;
				        else :
					        ?>
                            <!--Logo Start-->
                            <strong class="tnit-logo">
						        <?php the_custom_logo(); ?>
                            </strong><!--Logo End-->
				        <?php endif; ?>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-1">

                        <!--Nav Holder Start-->
                        <div class="tnit-navbar-holder">
                            <nav class="navbar navbar-default">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                            data-target="#tnit-navbar-collapse" aria-expanded="false"><span
                                                class="sr-only"><?php esc_html_e( 'Menu', 'myvoice' ); ?></span>
                                        <span class="icon-bar"></span> <span class="icon-bar"></span> <span
                                                class="icon-bar"></span></button>
                                </div>
                                <!-- Collect the nav links, forms, and other content for toggling -->
						        <?php if ( has_nav_menu( 'top' ) ) : ?>
                                    <div class="collapse navbar-collapse" id="tnit-navbar-collapse">
								        <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
                                    </div><!-- .navigation-top -->
						        <?php endif; ?>
                                <!-- /.navbar-collapse -->
                            </nav>
                            <!--Nav Right Start-->
					        <?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>
                            <ul class="tnit-nav-right">
                                <li class="tnit-search-box">
                                    <a href="#" id="trigger-tnit-search"><i class="fa fa-search" aria-hidden="true"></i></a>
                                    <!-- Search Form Outer Start-->
                                    <form role="search" method="get" class="tnit-search-form1"
                                          action="<?php echo esc_url( home_url( '/' ) ); ?>">
                                        <input type="text" id="<?php echo esc_html( $unique_id ); ?>"
                                               placeholder="<?php echo esc_attr_x( 'Type your keywords', 'placeholder', 'myvoice' ); ?>"
                                               value="<?php echo get_search_query(); ?>" name="s"/>
                                        <button class="btn-submit" type="submit">
                                            <i class="fa fa-search" aria-hidden="true"></i>
                                        </button>
                                    </form>
                                    <!-- Search Form Outer End-->
                                </li>
                            </ul>
                            <!--Nav Right End-->
                        </div><!--Nav Holder End-->
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--Header Content End-->