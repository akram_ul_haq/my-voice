<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package My Voice
 * @since 1.0
 */

?>
<!--Footer Content Start-->
<footer class="tnit-footer">
	<!--Footer Top Row start-->
	<?php
	/**
	 * Footer Widgets
	 */
	get_template_part( 'template-parts/footer/footer', 'widgets' );

	/**
	 * Site Info
	 */
	get_template_part( 'template-parts/footer/site', 'info' );
	?>

</footer><!--Footer Content End-->

</div><!--Wrapper Content End-->
<?php wp_footer(); ?>

</body>
</html>