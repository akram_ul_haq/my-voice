<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

get_header();

// Banner Image.
$banner_image_path = '';

$banner_image_path = get_option( 'theme_general_banner_image' );
if ( empty( $banner_image_path ) ) {
	$banner_image_path = get_template_directory_uri() . '/assets/images/banner-img-02.jpg';
}
?>
	<!--Inner Banner Content Start-->
	<div class="tnit-inner-banner tnit-banner-image_v2" style="background-repeat: no-repeat; background-position: center top; background-image: url('<?php echo esc_url( $banner_image_path ); ?>'); background-size: cover; ">
		<div class="container">
			<h2>
				<?php esc_html_e( 'Search', 'myvoice' ); ?>
			</h2>
			<!-- /.rh_banner__title -->

			<!--BreadCrumb Listed Start-->
			<?php custom_breadcrumbs(); ?>
			<!--BreadCrumb Listed End-->
		</div>
	</div><!--Inner Banner Content End-->

	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Video Section Start-->
		<section class="tnit-blog-section tnit-blog-section_v2 pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 col-xs-12">
						<!--Inner Video Outer Start-->
						<div class="tnit-video-inner-outer tnit-blog-inner-outer">
							<!--Heading Outer start-->
							<div class="tnit-heading-outer">
								<h2 class="page-title"><?php printf( __( 'Search Results for: %s', 'myvoice' ), '<span>' . get_search_query() . '</span>' ); ?></h2>
							</div><!--Heading Outer End-->
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<?php
									if ( have_posts() ) :

										/* Start the Loop */
										while ( have_posts() ) :
											the_post();

											/*
											 * Include the Post-Format-specific template for the content.
											 * If you want to override this in a child theme, then include a file
											 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
											 */
											get_template_part( 'template-parts/post/content', get_post_format() );

										endwhile;

									else :

										get_template_part( 'template-parts/post/content', 'none' );

									endif;
									?>
								</div>
							</div>
							<!--Pagination Row Start-->
							<div class="tnit-pagination-row">
								<nav aria-label="navigation">
									<?php theme_pagination(); ?>
								</nav>
							</div>
							<!--Pagination Row End-->
						</div><!--Inner Video Outer End-->

					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php get_sidebar(); ?>
						</aside>
						<!--Sidebar Outer End-->
					</div>
				</div>
			</div>
		</section><!--Video Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>
