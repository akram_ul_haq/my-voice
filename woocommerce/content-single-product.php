<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
/**
 * woocommerce_before_single_product hook.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form();

	return;
}
global $product;
$single_product = json_decode( $product, true );
?>
<div class="row">
	<div class="col-md-9 col-sm-12 col-xs-12">
		<!--Product Info Start-->
		<div class="tnit-pro-info-outer">
			<!--Product Item Start-->
			<div class="tnit-product-item tnit-product-detail-item">
				<figure class="tnit-thumb" style="width: 350px;">
					<img src="<?php echo esc_url( get_the_post_thumbnail_url( $single_product['id'] ) ); ?>" alt="">
				</figure>
				<div class="tnit-text-outer">
					<h3><?php echo $single_product['name']; ?></h3>
					<?php
					$postcat = get_the_terms( $single_product['id'], 'product_cat' );
					if ( ! empty( $postcat ) ) {
						echo '<span class="small">' . esc_html( $postcat[0]->name ) . '</span>';
					}
					?>
					<div class="bottom-text">
						<div class="left">
							<?php
							if ( ! empty( count( $single_product['rating_counts'] ) ) ) {
								?>
								<span>(<?php echo count( $single_product['rating_counts'] ); ?> <?php esc_html_e( 'reviews', 'myvoice' ); ?>
									)</span>
								<span class="tnit-rating">
									<?php
									$rating_avg = $single_product['average_rating'];
									$whole      = ceil( $rating_avg );
									for ( $i = 1; $i <= $whole; $i ++ ) {
										echo ' <i class="fa fa-star" aria-hidden="true"></i>';
									}
									$remained = 5 - $whole;
									if ( $remained > 0 ) {
										for ( $j = 1; $j <= $remained; $j ++ ) {
											echo ' <i class="fa fa-star-o" aria-hidden="true"></i>';
										}
									}
									?>
								</span>
								<?php
							} else {
								?>
								<span>( 0 <?php esc_html_e( 'reviews', 'myvoice' ); ?>)</span>
								<span class="tnit-rating">
									<i class="fa fa-star-o" aria-hidden="true"></i>
									<i class="fa fa-star-o" aria-hidden="true"></i>
									<i class="fa fa-star-o" aria-hidden="true"></i>
									<i class="fa fa-star-o" aria-hidden="true"></i>
									<i class="fa fa-star-o" aria-hidden="true"></i>
								</span>
								<?php
							}
							?>

						</div>
						<?php if ( $price_html = $product->get_price_html() ) : ?>
							<div class="right"><?php echo $price_html; ?></div>
						<?php endif; ?>
					</div>
					<ul class="tnit-btns-listed">
						<li class="share-holder">
							<a href="#">
								<i class="fa fa-share-alt" aria-hidden="true"></i>
								<?php esc_html_e( 'Share', 'myvoice' ); ?>
							</a>
							<ul class="tnit-social-links_v3" id="tnit-social-overlay">
								<li class="btn-facebook active">
									<a target="_blank"
									   href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>">
										<i class="fa fa-facebook" aria-hidden="true"></i>
										<?php esc_html_e( 'FACEBOOK', 'myvoice' ); ?>
									</a>
								</li>
								<li class="btn-twitter">
									<a target="_blank"
									   href="https://twitter.com/share?url=<?php the_permalink(); ?>&via=<?php the_author(); ?>&text=<?php the_title(); ?>">
										<i class="fa fa-twitter" aria-hidden="true"></i>
										<?php esc_html_e( 'TWITTER', 'myvoice' ); ?>
									</a>
								</li>
								<li class="btn-gplus">
									<a target="_blank"
									   href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
										<i class="fa fa-google-plus" aria-hidden="true"></i>
										<?php esc_html_e( 'GOOGLE+', 'myvoice' ); ?>
									</a>
								</li>
								<li class="btn-linkedin">
									<a target="_blank"
									   href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>">
										<i class="fa fa-linkedin" aria-hidden="true"></i>
										<?php esc_html_e( 'LINKEDIN', 'myvoice' ); ?>
									</a>
								</li>
							</ul>
						</li>
						<li class="btn-heart">
							<?php echo get_simple_likes_button( get_the_ID() ); ?>
						</li>

					</ul>
					<p> <?php echo $single_product['description']; ?></p>
					<div class="bottom-holder2">
						<?php do_action( 'woocommerce_' . $product->get_type() . '_add_to_cart' ); ?>
					</div>

				</div>

			</div><!--Product Item End-->
		</div><!--Product Info End-->
		<!--Inner Video Outer Start-->
		<div class="tnit-product-inner-outer">
			<?php do_action( 'woocommerce_after_single_product' ); ?>
			<!--Tabs Outer Start-->
			<div class="tl-tabs-outer">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs">
					<li role="presentation" class="active">
						<a href="#tl-tab-list1" aria-controls="tl-tab-list1" role="tab"
						   data-toggle="tab"><?php esc_html_e( 'Description', 'myvoice' ); ?></a></li>
					<li role="presentation">
						<a href="#tl-tab-list2" aria-controls="tl-tab-list2" role="tab" data-toggle="tab">
							(<?php echo ! empty( count( $single_product['rating_counts'] ) ) ? count( $single_product['rating_counts'] ) : '0'; ?>
							) <?php esc_html_e( 'reviews', 'myvoice' ); ?>
						</a>
					</li>
				</ul><!-- Nav tabs End-->

				<!-- Tab Content Start -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="tl-tab-list1">
						<!--Leave Review Outer Start-->
						<div class="tl-review-outer">
							<p><?php echo $single_product['short_description']; ?></p>
						</div><!--Leave Review Outer End-->
					</div>
					<div role="tabpanel" class="tab-pane " id="tl-tab-list2">
						<?php comments_template( 'woocommerce/single-product-reviews' ); ?>
					</div>
				</div>
			</div><!-- Tab Content End -->
		</div><!--Tabs Outer End-->
		<?php
		$args = array(
			'posts_per_page' => 3,
			'columns'        => 3,
			'orderby'        => 'rand',
		);
		woocommerce_related_products( apply_filters( 'woocommerce_output_related_products_args', $args ) );
		?>
	</div><!--Inner Video Outer End-->

	<div class="col-md-3 col-sm-12 col-xs-12">
		<!--Sidebar Outer Start-->
		<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		do_action( 'woocommerce_sidebar' );
		?><!--Sidebar Outer End-->
	</div>
</div>
