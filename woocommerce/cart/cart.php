<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="tnit-cart-section pd-tb70">
    <ul class="tnit-pro-btn-listed">
        <li>
            <div class="btn-inner-holder">
                <a href="#">
                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                    <span><?php esc_html_e( 'Your Cart', 'myvoice' ); ?></span>
                </a>
            </div>
        </li>
        <li>
            <div class="btn-inner-holder">
                <a href="#">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span><?php esc_html_e( 'Check Out', 'myvoice' ); ?></span>
                </a>
            </div>
        </li>
        <li>
            <div class="btn-inner-holder">
                <a href="#">
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                    <span><?php esc_html_e( 'Order Complete', 'myvoice' ); ?></span>
                </a>
            </div>
        </li>
    </ul>
    <div class="clearfix"></div>
	<?php
	wc_print_notices();

	do_action( 'woocommerce_before_cart' ); ?>

    <form class="tnit-cart-inner-holder" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">

		<?php do_action( 'woocommerce_before_cart_table' ); ?>

        <table class="tnit-cart-table" cellspacing="0">
            <tbody>
            <tr>
                <th class="tb-close">&nbsp;</th>
                <th class="tb-product"><?php esc_html_e( 'Product', 'myvoice' ); ?></th>
                <th class="tb-price"><?php esc_html_e( 'Price', 'myvoice' ); ?></th>
                <th class="tb-quantity"><?php esc_html_e( 'Quantity', 'myvoice' ); ?></th>
                <th class="tb-total"><?php esc_html_e( 'Total', 'myvoice' ); ?></th>
            </tr>
			<?php do_action( 'woocommerce_before_cart_contents' ); ?>

			<?php
			foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
				$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
				$product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

				if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
					?>
                    <tr>
                        <td class="tb-close">
		                    <?php
		                    echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
			                    '<a href="%s" class="btn-close" title="%s" data-product_id="%s" data-product_sku="%s"><i class="fa fa-times"></i></a>',
			                    esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
			                    esc_html__( 'Remove this item', 'myvoice' ),
			                    esc_attr( $product_id ),
			                    esc_attr( $_product->get_sku() )
		                    ), $cart_item_key );
		                    ?>
                        </td>

                        <td class="tb-product">
                            <div class="inner-holder">
                                <div class="cart-thumb">
									<?php
									$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

									if ( ! $product_permalink ) {
										echo $thumbnail;
									} else {
										printf( '<a style="display: block; width: 90px;" href="%s">%s</a>', esc_url( $product_permalink ), $thumbnail );
									}
									?>
                                </div>
                                <span>
                                    <?php
                                    if ( ! $product_permalink ) {
                                        echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                                    } else {
                                        echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_title() ), $cart_item, $cart_item_key );
                                    }

                                    // Meta data
                                    echo WC()->cart->get_item_data( $cart_item );

                                    // Backorder notification
                                    if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                        echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'myvoice' ) . '</p>';
                                    }
                                    ?>
                                    </span>
                            </div>
                        </td>

                        <td class="tb-price" data-title="<?php esc_html_e( 'Price', 'myvoice' ); ?>">
							<?php
							echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key );
							?>
                        </td>

                        <td class="tb-quantity" data-title="<?php esc_html_e( 'Quantity', 'myvoice' ); ?>">
							<?php
							if ( $_product->is_sold_individually() ) {
								$product_quantity = sprintf( '1 <input type="hidden" name="cart[%s][qty]" value="1" />', $cart_item_key );
							} else {
								$product_quantity = woocommerce_quantity_input( array(
									'input_name'  => "cart[{$cart_item_key}][qty]",
									'input_value' => $cart_item['quantity'],
									'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
									'min_value'   => '0'
								), $_product, false );
							}

							echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
							?>
                        </td>

                        <td class="tb-total" data-title="<?php esc_html_e( 'Total', 'myvoice' ); ?>">
							<?php
							echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key );
							?>
                        </td>



                    </tr>
					<?php
				}
			}

			do_action( 'woocommerce_cart_contents' );
			?>
            <tr>
                <td>
                    <div class="left-holder">

			            <?php if ( wc_coupons_enabled() ) { ?>

                            <div class="coupon-form">
                                <input type="text" name="coupon_code" class="input-text" value="" />
                                <input type="submit" class="btn-coupon btn-cart2" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'myvoice' ); ?>"/>
					            <?php do_action( 'woocommerce_cart_coupon' ); ?>
                            </div>
			            <?php } ?>
                    </div>
                    <div class="pull-right">

                        <input type="submit" class="btn-cart2" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'myvoice' ); ?>"/>
                    </div>
		            <?php do_action( 'woocommerce_cart_actions' ); ?>

		            <?php wp_nonce_field( 'woocommerce-cart' ); ?>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="right-holder">
	                    <?php woocommerce_cart_totals(); ?>
                    </div>
                </td>
            </tr>

			<?php do_action( 'woocommerce_after_cart_contents' ); ?>
            </tbody>
        </table>

		<?php do_action( 'woocommerce_after_cart_table' ); ?>

    </form>

	<?php do_action( 'woocommerce_after_cart' ); ?>
</div>