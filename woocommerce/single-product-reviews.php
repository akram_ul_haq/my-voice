<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     3.2.0
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews" class="woocommerce-Reviews">
    <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div id="comments" class="tl-comments-box">
                <h4 class="woocommerce-Reviews-title"><?php
					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' && ( $count = $product->get_review_count() ) ) {
						/* translators: 1: reviews count 2: product name */
						printf( esc_html( _n( '%1$s review for %2$s', '%1$s reviews for %2$s', $count, 'myvoice' ) ), esc_html( $count ), '<span>' . get_the_title() . '</span>' );
					} else {
						_e( 'Reviews', 'myvoice' );
					}
					?></h4>

				<?php if ( have_comments() ) : ?>

                    <ol class="commentlist tl-comments-listed">
						<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
                    </ol>

					<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
						echo '<nav class="woocommerce-pagination">';
						paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
							'prev_text' => '&larr;',
							'next_text' => '&rarr;',
							'type'      => 'list',
						) ) );
						echo '</nav>';
					endif; ?>

				<?php else : ?>

                    <p class="woocommerce-noreviews"><?php _e( 'There are no reviews yet.', 'myvoice' ); ?></p>

				<?php endif; ?>
            </div>
			<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div id="review_form_wrapper" class="tl-review-outer">
                <div id="review_form">
					<?php
					$commenter = wp_get_current_commenter();

					$comment_form = array(
						'title_reply'         => have_comments() ? __( 'Leave a Reply', 'myvoice' ) : sprintf( __( 'Be the first to review &ldquo;%s&rdquo;', 'myvoice' ), get_the_title() ),
						'title_reply_to'      => __( 'Leave a Reply to %s', 'myvoice' ),
						'title_reply_before'  => '<h5 id="reply-title" class="comment-reply-title">',
						'title_reply_after'   => '</h5>',
						'comment_notes_after' => '',
						'fields'              => array(
							'author' => '<div class="comment-form-author inner-holder">' . '<label for="author">' . esc_html__( 'Name', 'myvoice' ) . ' <span class="required">*</span></label> ' .
							            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" required /></div>',
							'email'  => '<div class="comment-form-email inner-holder"><label for="email">' . esc_html__( 'Email', 'myvoice' ) . ' <span class="required">*</span></label> ' .
							            '<input id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" aria-required="true" required /></div>',
						),
						'label_submit'        => __( 'Add Review', 'myvoice' ),
						'class_form'          => 'tl-review-form',
						'class_submit'        => 'btn-submit',
						'logged_in_as'        => '',
						'comment_field'       => '',
					);

					if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a review.', 'myvoice' ), esc_url( $account_page_url ) ) . '</p>';
					}

					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<div class="comment-form-rating rating-holder"><label for="rating">' . esc_html__( 'Your rating', 'myvoice' ) . '</label><select name="rating" id="rating" aria-required="true" required>
													<option value="">' . esc_html__( 'Rate&hellip;', 'myvoice' ) . '</option>
													<option value="5">' . esc_html__( 'Perfect', 'myvoice' ) . '</option>
													<option value="4">' . esc_html__( 'Good', 'myvoice' ) . '</option>
													<option value="3">' . esc_html__( 'Average', 'myvoice' ) . '</option>
													<option value="2">' . esc_html__( 'Not that bad', 'myvoice' ) . '</option>
													<option value="1">' . esc_html__( 'Very poor', 'myvoice' ) . '</option>
												</select></div>';
					}

					$comment_form['comment_field'] .= '<div class="comment-form-comment inner-holder"><label for="comment">' . esc_html__( 'Message', 'myvoice' ) . ' <span class="required">*</span></label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" required></textarea></div>';

					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
					?>
                </div>
            </div>

			<?php else : ?>

                <p class="woocommerce-verification-required"><?php _e( 'Only logged in customers who have purchased this product may leave a review.', 'myvoice' ); ?></p>

			<?php endif; ?>
        </div>
    </div>
</div>
