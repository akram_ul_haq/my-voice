<?php
/**
 * The template to display the reviewers star rating in reviews
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/review-rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $comment;
$rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );

if ( $rating && 'yes' === get_option( 'woocommerce_enable_review_rating' ) ) {
	?>
	<span class="tnit-rating">
		<?php
		$rating_avg = $rating;
		$whole      = ceil( $rating_avg );
		for ( $i = 1; $i <= $whole; $i ++ ) {
			echo ' <i class="fa fa-star" aria-hidden="true"></i>';
		}
		$remained = 5 - $whole;
		if ( $remained > 0 ) {
			for ( $j = 1; $j <= $remained; $j ++ ) {
				echo ' <i class="fa fa-star-o" aria-hidden="true"></i>';
			}
		}
		?>
	</span>
	<?php
}
