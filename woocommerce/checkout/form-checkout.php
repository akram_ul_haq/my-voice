<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see        https://docs.woocommerce.com/document/template-structure/
 * @author        WooThemes
 * @package    WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="tnit-checkout-section pd-tb70">
    <ul class="tnit-pro-btn-listed">
        <li>
            <div class="btn-inner-holder">
                <a href="#">
                    <i class="fa fa-cart-plus" aria-hidden="true"></i>
                    <span><?php esc_html_e( 'Your Cart', 'myvoice' ); ?></span>
                </a>
            </div>
        </li>
        <li>
            <div class="btn-inner-holder">
                <a href="#">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                    <span><?php esc_html_e( 'Check Out', 'myvoice' ); ?></span>
                </a>
            </div>
        </li>
        <li>
            <div class="btn-inner-holder">
                <a href="#">
                    <i class="fa fa-credit-card" aria-hidden="true"></i>
                    <span><?php esc_html_e( 'Order Complete', 'myvoice' ); ?></span>
                </a>
            </div>
        </li>
    </ul>
    <div class="clearfix"></div>
	<?php
	wc_print_notices();

	do_action( 'woocommerce_before_checkout_form', $checkout );

	// If checkout registration is disabled and not logged in, the user cannot checkout
	if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
		echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', esc_html__( 'You must be logged in to checkout.', 'myvoice' ) );

		return;
	}
	?>
    <div class="row">

        <div class="tl-review-outer tnit-proceed-outer">
            <form name="checkout" method="post" class="tl-review-form tnit-proceed-form" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
                <div class="col-md-6 col-sm-12 col-xs-12">
	                <?php if ( sizeof( $checkout->checkout_fields ) > 0 ) : ?>
                        <div class="row">
			                <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
			                <?php do_action( 'woocommerce_checkout_billing' ); ?>
			                <?php do_action( 'woocommerce_checkout_shipping' ); ?>
			                <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
                        </div>
	                <?php endif; ?>
					<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                <div class="tnit-order-outer">
                    <h3><?php esc_html_e( 'Your order', 'myvoice' ); ?></h3>

	                <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

	                <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                </div>
                </div>
            </form>

        </div>

		<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
    </div>
</div>