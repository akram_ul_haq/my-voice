<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

/**
 * woocommerce_before_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_open - 10
 */
do_action( 'woocommerce_before_shop_loop_item' );


/**
 * woocommerce_before_shop_loop_item_title hook.
 *
 * @hooked woocommerce_show_product_loop_sale_flash - 10
 * @hooked woocommerce_template_loop_product_thumbnail - 10
 */
do_action( 'woocommerce_before_shop_loop_item_title' );
?>
<div class="col-md-4 col-sm-6 col-xs-12">
    <div class="tnit-product-item">
        <figure class="tnit-thumb">
            <a href="<?php the_permalink(); ?>" class="wrapper-thumb-product">
				<?php the_post_thumbnail( 'shop_catalog', array( 'class' => 'img-responsive' ) ); ?>
				<?php if ( $product->is_on_sale() ) : ?>
                    <figcaption class="tnit-caption">
            <span class="btn-sale">
			    <?php esc_html_e( 'Sale!', 'myvoice' ); ?>
            </span>
                    </figcaption>
				<?php endif; ?>
            </a>
        </figure>
		<?php

		/**
		 * woocommerce_shop_loop_item_title hook.
		 *
		 * @hooked woocommerce_template_loop_product_title - 10
		 */
		do_action( 'woocommerce_shop_loop_item_title' );

		$single_product = json_decode( $product, true );

		?>
        <div class="tnit-text-outer">
			<?php

			$postcat = get_the_terms( $single_product['id'], 'product_cat' );
			if ( ! empty( $postcat ) ) {
				echo '<span class="small">' . esc_html( $postcat[0]->name ) . '</span>';
			}
			?>
            <h4 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
            <div class="bottom-text">
                <div class="left">
					<?php
					if ( ! empty( count( $single_product['rating_counts'] ) ) ) {
						?>
                        <span>(<?php echo count( $single_product['rating_counts'] ); ?> <?php esc_html_e( 'reviews', 'myvoice' ); ?>
                            )</span>
                        <span class="tnit-rating">
                    <?php
                    $rating_avg = $single_product['average_rating'];
                    $whole      = ceil( $rating_avg );
                    for ( $i = 1; $i <= $whole; $i ++ ) {
	                    echo ' <i class="fa fa-star" aria-hidden="true"></i>';
                    }
                    $remained = 5 - $whole;
                    if ( $remained > 0 ) {
	                    for ( $j = 1; $j <= $remained; $j ++ ) {
		                    echo ' <i class="fa fa-star-o" aria-hidden="true"></i>';
	                    }
                    }
                    ?>
                </span>
						<?php
					} else {
						?>
                        <span>( 0 <?php esc_html_e( 'reviews', 'myvoice' ); ?>)</span>
                        <span class="tnit-rating">
                <i class="fa fa-star-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
                <i class="fa fa-star-o" aria-hidden="true"></i>
            </span>
						<?php
					}
					?>

                </div>
				<?php
				/**
				 * woocommerce_after_shop_loop_item_title hook.
				 *
				 * @hooked woocommerce_template_loop_rating - 5
				 * @hooked woocommerce_template_loop_price - 10
				 */
				//  do_action( 'woocommerce_after_shop_loop_item_title' );

				?>
				<?php if ( $price_html = $product->get_price_html() ) : ?>
                    <div class="right"><?php echo $price_html; ?></div>
				<?php endif; ?>

            </div>
			<?php

			/**
			 * woocommerce_after_shop_loop_item hook.
			 *
			 * @hooked woocommerce_template_loop_product_link_close - 5
			 * @hooked woocommerce_template_loop_add_to_cart - 10
			 */
			do_action( 'woocommerce_after_shop_loop_item' );
			?>
        </div>
    </div>
</div>
