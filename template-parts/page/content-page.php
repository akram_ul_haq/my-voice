<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'tnit-blog-item' ); ?>>
	<div class="row">
		<div class="col-xs-12">
			<!--Blog Text Start-->
			<div class="tnit-text">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				<!--Meta Listed Start-->
				<?php the_content(); ?>
			</div><!--Blog Text End-->
		</div>
	</div>
</div><!-- #post-## -->
