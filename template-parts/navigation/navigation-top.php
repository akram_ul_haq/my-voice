<?php
/**
 * Displays top navigation
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

wp_nav_menu( array(
	'theme_location' => 'top',
	'menu_id'        => 'top-menu',
	'menu_class'     => 'nav navbar-nav',
) );
?>