<?php
/**
 * Template part for displaying image video posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */
?>
<div class="col-md-4 col-sm-6 col-xs-12">
	<!--Video Item Start-->
	<div id="post-<?php the_ID(); ?>" <?php post_class( 'tnit-video-item' ); ?>>
		<figure class="video-thumb">
			<?php
			$video_code = get_post_meta( get_the_ID(), 'MY_VOICE_embed_code', true );
			if ( '' !== get_the_post_thumbnail() ) :
				the_post_thumbnail( 'my-voice-video-featured-image' );
			else :
				the_post_thumbnail( 'my-voice-single-featured-image' );
			endif;
			?>
			<figcaption class="tnit-caption">
                <a class="icon-box" href="<?php the_permalink(); ?>">
                    <i class="fa fa-play" aria-hidden="true"></i>
                </a>
				<div class="bottom-text">
					<!--Meta Listed Start-->
					<ul class="tnit-meta-listed">
						<li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><?php echo get_the_date(); ?></li>
						<li><i class="fa fa-comments-o" aria-hidden="true"></i> <?php comments_number( '0', '1', '%' ); ?></li>
					</ul><!--Meta Listed End-->
				</div>
			</figcaption>
		</figure>
		<div class="tnit-text-outer">
			<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
		</div>
	</div><!--Video Item End-->
</div>