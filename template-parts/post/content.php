<?php
/**
 * Template part for displaying image posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */
global $post;

if ( is_singular() ) :
	$post_class = 'tnit-blog-item';
else:
	$post_class = 'tnit-blog-item tnit-blog-item_v2';
endif;
?>
<!--Blog Item Start-->
<div id="post-<?php the_ID(); ?>" <?php post_class( $post_class ); ?>>
    <div class="row">
		<?php if ( '' !== get_the_post_thumbnail() ) : ?>
            <div class="col-md-4 col-sm-5 col-xs-12">
                <!--Blog Thumb Start-->
                <figure class="tnit-thumb">
					<?php
					if ( '' !== get_the_post_thumbnail() && ! is_single() ) :
						the_post_thumbnail( 'my-voice-featured-image' );
					else :
						the_post_thumbnail( 'my-voice-related-featured-image' );
					endif;
					?>
                    <figcaption class="tnit-caption">
                        <div class="date-box entry-date published">
                            <span><?php the_time( 'd' ); ?></span>
							<?php the_time( 'M' ); ?>
                        </div>
                        <a class="tnit-play-btn" href="
					<?php
						$video_url = get_post_meta( get_the_ID(), 'MY_VOICE_embed_url', true );
						echo $video_url ? esc_url( $video_url ) : esc_url( get_the_permalink() );
						?>
					">
							<?php echo ! empty( $video_url ) ? '<span class="icomoon icon-play-rounded-button"></span>' : ''; ?>
                        </a>
                    </figcaption>
                </figure><!--Blog Thumb End-->
            </div>
		<?php endif; ?>
        <div class="<?php echo ( '' !== get_the_post_thumbnail() ) ? 'col-md-8 col-sm-7' : 'no-featured-image'; ?> col-xs-12">
            <!--Blog Text Start-->
            <div class="tnit-text">
				<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
                <!--Meta Listed Start-->
                <ul class="tnit-meta-listed tnit-meta-listed2">
                    <li><?php my_voice_posted_on(); ?></li>
					<?php
					$separate_meta = __( ' , ', 'myvoice' );

					// Get Categories for posts.
					$categories_list = get_the_category_list( $separate_meta );
					if ( $categories_list ) :
						echo "<li>$categories_list </li>";
					endif;
					?>
                    <li><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></li>
                </ul><!--Meta Listed End-->
                <p><?php the_excerpt_max_char_length( 200 ); ?></p>
                <a href="<?php the_permalink(); ?>" class="tnit-btn-style1"><?php esc_html_e( 'View More', 'myvoice' ); ?></a>
                <br><br>
            </div><!--Blog Text End-->
        </div>
    </div>
</div>
<!--Blog Item End-->