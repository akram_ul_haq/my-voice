<?php
/**
 * Template part for displaying course
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */
?>
<div class="col-md-4 co-sm-6 col-xs-12">
	<!--Courses Item Start-->
	<div class="tnit-courses-item">
		<figure class="tnit-thumb">
			<?php
			if ( '' !== get_the_post_thumbnail() ) :
				the_post_thumbnail( 'my-voice-course-image' );
			endif;

			?>
			<!--Courses Caption Start-->
			<figcaption class="tnit-caption">
				<a href="<?php the_permalink(); ?>"
				   class="tnit-btn-style1"><?php esc_html_e( 'Join Class', 'myvoice' ); ?></a>
			</figcaption><!--Courses Caption End-->
		</figure>
		<div class="tnit-text">
			<div class="top-holder">
				<?php echo get_avatar( get_the_author_meta( 'user_email' ), 30 ); ?>
				<h5>
					<?php
					the_author();
					$course_price = get_post_meta( get_the_ID(), 'MY_VOICE_course_price', true );
					if ( empty( $course_price ) ) :
						?>
						<span class="free">
						<?php esc_html_e( 'Free', 'myvoice' ); ?>
					</span>
					<?php else : ?>
						<span class="free price">
						<?php echo esc_html( $course_price ); ?>
					</span>
					<?php endif; ?>
				</h5>
			</div>
			<strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
			<div class="bottom-holder">
				<?php
				$rating_class = Stars_Rating();
				if( isset( $rating_class ) && ! empty( $rating_class ) ) :
				$rating_class->rating_average();
				endif;
				?>
				<span class="users">
					<i class="fa fa-users" aria-hidden="true"></i>
					<?php
					$args = array(
						'meta_query'  => array(
							array(
								'key'     => 'my_voice_course',
								'value'   => get_the_ID(),
								'compare' => '=',
							),
						),
						'count_total' => true,
					);

					$users = new WP_User_Query( $args );
					echo esc_html( $users->get_total() );
					?>
				</span>
			</div>
		</div>
	</div><!--Courses Item End-->
</div>