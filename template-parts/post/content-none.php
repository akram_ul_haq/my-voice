<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class( 'tnit-blog-item' ); ?>>
	<div class="row">
		<div class="col-xs-12">
			<!--Blog Text Start-->
			<div class="tnit-text">
				<h1><?php esc_html_e( 'Nothing Found', 'myvoice' ); ?></h1>
				<!--Meta Listed Start-->
				<?php
				if ( is_home() && current_user_can( 'publish_posts' ) ) :
					?>

					<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'myvoice' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

				<?php else : ?>

					<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'myvoice' ); ?></p>
					<br><br>
					<?php
					get_search_form();

				endif; ?>
			</div><!--Blog Text End-->
		</div>
	</div>
</div>
