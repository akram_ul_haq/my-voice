<?php
/**
 * Template part for displaying events
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

$image_url = get_the_post_thumbnail_url( $post->ID, 'my-voice-events-image' );
if ( $image_url ) {
	?>
    <style>
        <?php echo '.event-item-'.get_the_ID().':after'; ?>
        {
            background-image: url("<?php echo esc_url( $image_url ); ?>") !important
        ;
        }
    </style>

<?php } ?>
<!--Even Item Start-->
<li class="tnit-event-item <?php echo 'event-item-' . get_the_ID(); ?>">
    <div class="row">
        <div class="col-md-2 col-sm-2 col-xs-12">
            <div class="date-box">
				<?php
				$event_date_time = get_post_meta( get_the_ID(), 'MY_VOICE_event_date_time', true );
				if ( $event_date_time ) {
					$interval = date_parse( $event_date_time );
					echo '<span>' . esc_html( $interval['day'] ) . '</span>';
					$date_obj   = DateTime::createFromFormat( '!m', $interval['month'] );
					$month_name = $date_obj->format( 'M' );
					echo esc_html( $month_name );
				} else {
					echo '<span>' . esc_html( get_the_time( 'd' ) ) . '</span>';
					the_time( 'M' );
				}
				?>
            </div>
        </div>
        <div class="col-md-7 col-sm-7 col-xs-12">
            <div class="tnit-text">
                <h4><?php the_title(); ?></h4>
				<?php
				$event_location = get_post_meta( get_the_ID(), 'MY_VOICE_event_location', true );
				if ( $event_location ) {
					echo '<span><i class="fa fa-map-signs" aria-hidden="true"></i>' . esc_html( $event_location ) . '</span>';
				}
				?>
            </div>
        </div>
        <div class="col-md-3 col-sm-3 col-xs-12">
            <a href="<?php the_permalink(); ?>"
               class="tnit-btn-style1"><?php esc_html_e( 'View Event', 'myvoice' ); ?></a>
        </div>
    </div>
</li><!--Even Item End-->