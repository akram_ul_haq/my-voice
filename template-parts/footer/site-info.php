<?php
/**
 * Displays footer site info
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

global $my_voice_allowed_tags;
?>
<!--CopyRight Row Start-->
<div class="tnit-copyright-row">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">

			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<p><?php echo wp_kses( get_option( 'theme_footer_copyright' ), $my_voice_allowed_tags ); ?></p>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-12">
				<!--Social Links start-->
				<?php
				$show_social = get_option( 'theme_show_social_menu' );
				if ( 'true' === $show_social ) {
					$sl_facebook  = get_option( 'theme_facebook_link' );
					$sl_twitter   = get_option( 'theme_twitter_link' );
					$sl_linkedin  = get_option( 'theme_linkedin_link' );
					$sl_google    = get_option( 'theme_google_link' );
					$sl_instagram = get_option( 'theme_instagram_link' );
					$sl_skype     = get_option( 'theme_skype_username' );
					$sl_youtube   = get_option( 'theme_youtube_link' );
					$sl_pinterest = get_option( 'theme_pinterest_link' );
					$sl_rss       = get_option( 'theme_rss_link' );
					?>
					<ul class="tnit-social-links tnit-social-links_v2">
						<?php
						if ( ! empty( $sl_facebook ) ) {
							?>
							<li class="facebook">
								<a target="_blank" href="<?php echo esc_url( $sl_facebook ); ?>"><i
											class="fa fa-facebook"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_twitter ) ) {
							?>
							<li class="twitter">
								<a target="_blank" href="<?php echo esc_url( $sl_twitter ); ?>"><i
											class="fa fa-twitter"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_linkedin ) ) {
							?>
							<li class="linkedin">
								<a target="_blank" href="<?php echo esc_url( $sl_linkedin ); ?>"><i
											class="fa fa-linkedin"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_google ) ) {
							?>
							<li class="gplus">
								<a target="_blank" href="<?php echo esc_url( $sl_google ); ?>"><i
											class="fa fa-google-plus"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_instagram ) ) {
							?>
							<li class="instagram">
								<a target="_blank" href="<?php echo esc_url( $sl_instagram ); ?>"> <i
											class="fa fa-instagram"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_youtube ) ) {
							?>
							<li class="youtube">
								<a target="_blank" href="<?php echo esc_url( $sl_youtube ); ?>"> <i
											class="fa fa-youtube-square"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_skype ) ) {
							?>
							<li class="skype">
								<a target="_blank" href="skype:<?php echo esc_attr( $sl_skype ); ?>?add"> <i
											class="fa fa-skype"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_pinterest ) ) {
							?>
							<li class="pinterest">
								<a target="_blank" href="<?php echo esc_url( $sl_pinterest ); ?>"> <i
											class="fa fa-pinterest"></i></a>
							</li>
							<?php

						}

						if ( ! empty( $sl_rss ) ) {
							?>
							<li class="rss">
								<a target="_blank" href="<?php echo esc_url( $sl_rss ); ?>"> <i
											class="fa fa-rss"></i></a>
							</li>
							<?php

						}
						?>
					</ul>
					<?php
				}
				?>
				<!--Socail Links End-->
			</div>
		</div>
	</div>
</div><!--CopyRight Row End-->