<?php
/**
 * Displays footer widgets if assigned
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

if ( is_active_sidebar( 'footer-sidebar-one' ) || is_active_sidebar( 'footer-sidebar-two' ) || is_active_sidebar( 'footer-sidebar-three' ) ) :
	?>
	<div class="tnit-footer-tprow">
		<div class="container">
			<div class="row">
				<?php
				if ( is_active_sidebar( 'footer-sidebar-one' ) ) {
					?>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<?php dynamic_sidebar( 'footer-sidebar-one' ); ?>
					</div>
					<?php
				}
				if ( is_active_sidebar( 'footer-sidebar-two' ) ) {
					?>
					<div class="col-md-2 col-sm-12 col-xs-12">
						<?php dynamic_sidebar( 'footer-sidebar-two' ); ?>
					</div>
					<?php
				}
				if ( is_active_sidebar( 'footer-sidebar-three' ) ) {
					?>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<?php dynamic_sidebar( 'footer-sidebar-three' ); ?>
					</div>
				<?php
				}
				if ( is_active_sidebar( 'footer-sidebar-three' ) ) {
					?>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<?php dynamic_sidebar( 'footer-sidebar-four' ); ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</div><!--Footer Top Row End-->

<?php endif; ?>

