<?php
/**
 * Slider template part for home
 *
 * Page template for homepage.
 *
 * @since 1.0.0
 * @package My Voice
 */

$arg  = array(
	'post_type'      => 'sliders',
	'posts_per_page' => - 1,
);
$loop = new WP_Query( $arg );

if ( $loop->have_posts() ) :
	?>
	<!--Banner Content Start-->
	<div class="tnit-banner">
		<!--Banner Slider Start-->
		<div id="tnit-banner-slider" class="owl-carousel">
			<?php
			global $post;
			/* Start the Loop */
			while ( $loop->have_posts() ) :
				$loop->the_post();
				?>
				<!--Banner Slider Item-->
				<div class="slider-item">
					<?php
					$slider_sub_title     = get_post_meta( $post->ID, 'MY_VOICE_slider_sub_title', true );
					$slider_bg_image      = get_post_meta( $post->ID, 'MY_VOICE_slider_bg_image', true );
					$slider_teacher_image = get_post_meta( $post->ID, 'MY_VOICE_slider_teacher_image', true );
					$slider_button_text   = get_post_meta( $post->ID, 'MY_VOICE_slider_button_text', true );
					$slider_button_url    = get_post_meta( $post->ID, 'MY_VOICE_slider_button_url', true );

					?>
					<?php if ( $slider_bg_image ) : ?>
						<img src="<?php echo esc_url( wp_get_attachment_image_url( $slider_bg_image, 'full' ) ); ?>"
							 alt="Image">
					<?php endif; ?>
					<!--Banner Slider Caption Start-->
					<div class="banner-caption">
						<div class="container">
							<div class="row">
								<div class="">
									<div class="top-text">
										<h2><?php echo esc_html( the_title() ); ?></h2>
										<?php if ( $slider_sub_title ) : ?>
											<h4><?php echo wp_kses( $slider_sub_title, array( 'span' => array() ) ); ?></h4>
										<?php endif; ?>
										<?php if ( $slider_button_url ) : ?>
											<a href="<?php echo esc_url( $slider_button_url ); ?>"
											   class="btn-appiontment"><?php echo esc_html( $slider_button_text ); ?></a>
										<?php endif; ?>

									</div>
								</div>
								<?php if ( $slider_teacher_image ) : ?>
									<div class="col-md-6 col-sm-7 col-xs-12 col-md-offset-6 col-sm-offset-5">
										<div class="banner-man-img">
											<img src="<?php echo esc_url( wp_get_attachment_image_url( $slider_teacher_image, 'full' ) ); ?>"
												 alt="Image">
										</div>
									</div>
								<?php endif; ?>

							</div>
							<!--  <img src="images/banner-man-img.png" alt=""> -->
						</div>
					</div><!--Banner Slider Caption End-->
				</div>
				<!--Banner Slider End-->
			<?php
			endwhile;
			?>
		</div><!--Banner Slider End-->
		<?php if ( 'true' === get_option( 'my_voice_home_social_list' ) ) : ?>
			<!--Social Row Start-->
			<div class="tnit-social-row">
				<div class="container">
                    <div class="tnit-inner-social-holder">
                        <div class="row">
		                    <?php
		                    $slider_social_list = get_theme_mod( 'my_voice_home_slider_social' );
		                    if( ! empty( $slider_social_list )){
			                    /*This returns a json so we have to decode it*/
			                    $slider_social_list_decoded = json_decode( $slider_social_list );
			                    foreach ( $slider_social_list_decoded as $repeater_item ) {
				                    ?>
                                    <div class="col-md-3 col-sm-3 col-xs-12">
                                        <!--Socail Box Start-->
                                        <div class="tnit-social-box">
                                            <a href="#"><i class="fa <?php echo esc_html( $repeater_item->icon_value ); ?>" aria-hidden="true"></i></a>
                                            <div class="tnit-text">
                                                <strong class="tnit-social-counter"><?php echo do_shortcode( htmlspecialchars_decode( $repeater_item->shortcode ) ); ?></strong>
                                                <span><?php echo esc_html( $repeater_item->title ); ?></span>
                                            </div>
                                        </div><!--Socail Box End-->
                                    </div>

				                    <?php

			                    }
		                    }
		                    ?>
                        </div>
                    </div>

				</div>
			</div><!--Social Row End-->
		<?php
		endif;
		?>
	</div><!--Banner Content End-->
<?php
endif;
?>