<?php
/**
 * Features template part for home
 *
 * Page template for homepage.
 *
 * @since    1.0.0
 * @package My Voice
 */

if ( 'true' === get_option( 'my_voice_home_facts' ) ) :
	if ( get_option( 'my_voice_home_features_watermark_image' ) ) :
		?>
        <style>
            .tnit-features-section .tnit-container-before:before {
                background-image: url("<?php echo esc_url( get_option( 'my_voice_home_features_watermark_image' ) ); ?>");
            }
        </style>

	<?php endif; ?>
	<!--Features Section Start-->
	<section class="tnit-features-section pd-tb70">
		<div class="container tnit-container-before">
			<!--Heading Outer start-->
			<div class="tnit-heading-outer text-center">
				<h2><?php echo esc_html( get_option( 'my_voice_home_features_title' ) ); ?></h2>
			</div><!--Heading Outer End-->
			<div class="row">

				<?php
				$home_features_list = get_theme_mod( 'my_voice_home_features_details' );
				/*This returns a json so we have to decode it*/
				$home_features_list_decoded = json_decode( $home_features_list );
				foreach ( $home_features_list_decoded as $repeater_item ) {
					?>
					<div class="col-md-4 col-sm-6 col-xs-12">
						<!--Feature Item Start-->
						<div class="tnit-feature-item">
                                    <span class="icon-box">
                                        <i class="icomoon <?php echo esc_html( $repeater_item->subtitle ); ?>" aria-hidden="true"></i>
                                    </span>
							<h4><?php echo esc_html( $repeater_item->title ); ?></h4>
							<p><?php echo esc_textarea( $repeater_item->text ); ?></p>
						</div><!--Feature Item End-->
					</div>
					<?php
				}
				?>
			</div>
		</div>
	</section><!--Features Section End-->
<?php endif; ?>

