<?php
/**
 * About template part for home
 *
 * Page template for homepage.
 *
 * @since    1.0.0
 * @package My Voice
 */
if ( 'true' === get_option( 'my_voice_home_about' ) ) :
if ( get_option( 'my_voice_home_about_watermark_image' ) ) :
    ?>
    <style>
        .tnit-container-after:after {
            background-image: url("<?php echo esc_url( get_option( 'my_voice_home_about_watermark_image' ) ); ?>");
        }
    </style>

<?php endif; ?>
	<!--About Section Start-->
	<section class="tnit-about-section">
		<div class="container tnit-container-after">
			<div class="row">
				<div class="col-md-4 col-sm-5">
					<!--About Thumb Start-->
					<figure class="tnit-about-video">
						<?php if ( get_option( 'my_voice_home_about_image' ) ) : ?>
							<img src="<?php echo esc_url( get_option( 'my_voice_home_about_image' ) ); ?>" alt="">
						<?php endif; ?>
						<?php if ( get_option( 'my_voice_home_about_image_url' ) ) : ?>
							<figcaption class="tnit-caption">
								<a href="<?php echo esc_url( get_option( 'my_voice_home_about_image_url' ) ); ?>"
								   class="btn-play">
									<!-- <i class="fa fa-play-circle-o" aria-hidden="true"></i> -->
									<span class="icon-play-button icomoon"></span>
								</a>
								<div id="tnit-videoPlayer-outer">
									<div class="tnit-videoplay-inner"></div>
								</div>
							</figcaption>
						<?php endif; ?>
					</figure><!--About Thumb End-->
				</div>
				<div class="col-md-8 col-sm-7">
					<!--About Text Start-->
					<div class="tnit-about-text">
						<?php if ( get_option( 'my_voice_home_about_title' ) ) : ?>
							<h2><?php echo esc_html( get_option( 'my_voice_home_about_title' ) ); ?></h2>
						<?php endif; ?>
						<?php if ( get_option( 'my_voice_home_about_textarea' ) ) : ?>
							<p><?php echo wp_kses( get_option( 'my_voice_home_about_textarea' ), array( 'strong' => array() ) ); ?></p>
						<?php endif; ?>
						<?php if ( get_option( 'my_voice_home_about_pages' ) ) : ?>
							<a href="<?php echo esc_url( get_permalink( get_option( 'my_voice_home_about_pages' ) ) ); ?>"
							   class="tnit-btn-readmore"><?php echo esc_html( get_option( 'my_voice_home_about_button_title' ) ); ?></a>
						<?php endif; ?>
					</div><!--About Text End-->
				</div>
			</div>
		</div>
	</section>
	<!--About Section End-->
<?php endif; ?>
