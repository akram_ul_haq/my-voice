<?php
/**
 * Facts template part for home
 *
 * Page template for homepage.
 *
 * @since 1.0.0
 * @package My Voice
 */

?>
<?php if ( 'true' === get_option( 'my_voice_general_facts_hide' ) ) : ?>
	<!--Facts Section Start-->
	<section class="tnit-facts-section">
		<div class="container">
			<!--Facts Inner Outer Start-->
			<div class="facts-inner-outer">
				<?php if ( ! empty( get_option( 'my_voice_general_facts' ) ) ) : ?>
					<ul class="tnit-facts-listed">
						<?php
						/*This returns a json so we have to decode it*/
						$slider_facts_list_decoded = json_decode( get_option( 'my_voice_general_facts' ) );
						foreach ( $slider_facts_list_decoded as $repeater_item ) {
							?>
							<li class="facts-item">
								<i class="<?php echo esc_html( $repeater_item->icon_value ); ?>" aria-hidden="true"></i>
								<div class="tnit-text">
									<strong class="tnit-counter"><?php echo esc_html( $repeater_item->subtitle ); ?></strong>
									<span><?php echo esc_html( $repeater_item->title ); ?></span>
								</div>
							</li>
							<?php
						}
						?>
					</ul>
				<?php endif; ?>
			</div><!--Facts Inner Outer End-->
		</div>
	</section><!--Facts Section End-->
<?php endif; ?>
