<?php
/**
 * Courses template part for home
 *
 * Page template for homepage.
 *
 * @since    1.0.0
 * @package My Voice
 */

global $wpdb;
/* Get post IDs of all pages using "template-faq.php" */
$course_pages = $wpdb->get_results( "SELECT `post_id` FROM $wpdb->postmeta WHERE `meta_key` ='_wp_page_template' AND `meta_value` = 'templates/courses.php' ", ARRAY_A );
/* Get permalink using post ID of first page in the results */
$course_permalink = get_permalink( $course_pages[0]['post_id'] );
$arg              = array(
	'post_type'      => 'courses',
	'posts_per_page' => 4,
);
$loop             = new WP_Query( $arg );

if ( $loop->have_posts() && 'true' === get_option( 'my_voice_home_courses_posts' ) ) :
	if ( get_option( 'my_voice_home_courses_watermark_image' ) ) :
		?>
        <style>
            .tnit-courses-section .tnit-container-before:before {
                background-image: url("<?php echo esc_url( get_option( 'my_voice_home_courses_watermark_image' ) ); ?>");
            }
        </style>

	<?php endif; ?>

	<!--Courses Section Start-->
	<section class="tnit-courses-section pd-tb70">
		<div class="container">
			<?php if ( get_option( 'my_voice_home_courses_title' ) ) : ?>
				<!--Heading Outer start-->
				<div class="tnit-heading-outer text-center">
					<h2><?php echo esc_html( get_option( 'my_voice_home_courses_title' ) ); ?></h2>
				</div><!--Heading Outer End-->
			<?php endif; ?>
			<div class="row">
				<?php

				/* Start the Loop */
				while ( $loop->have_posts() ) :
					$loop->the_post();
					?>
					<div class="col-md-3 co-sm-6 col-xs-12">
						<!--Courses Item Start-->
						<div class="tnit-courses-item">
							<figure class="tnit-thumb">
								<?php
								if ( '' !== get_the_post_thumbnail() ) :
									the_post_thumbnail( 'my-voice-course-image' );
								endif;

								?>
								<!--Courses Caption Start-->
								<figcaption class="tnit-caption">
									<a href="<?php the_permalink(); ?>"
									   class="tnit-btn-style1"><?php esc_html_e( 'Join Class', 'myvoice' ); ?></a>
								</figcaption><!--Courses Caption End-->
							</figure>
							<div class="tnit-text">
								<div class="top-holder">
									<?php echo get_avatar( get_the_author_meta( 'user_email' ), 30 ); ?>
									<h5>
										<?php
										the_author();
										$course_price = get_post_meta( get_the_ID(), 'MY_VOICE_course_price', true );
										if ( empty( $course_price ) ) :
											?>
											<span class="free"><?php esc_html_e( 'Free', 'myvoice' ); ?></span>
										<?php else : ?>
											<span class="free price"><?php echo esc_html( $course_price ); ?></span>
										<?php endif; ?>
									</h5>
								</div>
								<strong><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></strong>
								<div class="bottom-holder">
									<?php
									$rating_class = Stars_Rating();
									$rating_class->rating_average();
									?>
									<span class="users">
					<i class="fa fa-users" aria-hidden="true"></i>
										<?php
										$args = array(
											'meta_query'  => array(
												array(
													'key'     => 'my_voice_course',
													'value'   => get_the_ID(),
													'compare' => '=',
												),
											),
											'count_total' => true,
										);

										$users = new WP_User_Query( $args );
										echo esc_html( $users->get_total() );
										?>
				</span>
								</div>
							</div>
						</div><!--Courses Item End-->
					</div>
				<?php
				endwhile;
				?>
			</div>
            <div class="tnit-btn-group text-center">
                <a href="<?php echo esc_url( $course_permalink ); ?>" class="tnit-btn-style1"><?php esc_html_e( 'View All Classes', 'myvoice' ); ?></a>
            </div>
		</div>
	</section><!--Courses Section End-->
<?php
endif;
?>

