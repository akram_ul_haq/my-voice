<?php
/**
 * Events template part for home
 *
 * Page template for homepage.
 *
 * @since    1.0.0
 * @package My Voice
 */
?>
<!--Section Start-->
<section class="tnit-section pd-tb70">
	<div class="container">
		<div class="row">

			<?php
			$arg = array(
				'post_type'      => 'events',
				'posts_per_page' => 4,
			);
			if ( get_option( 'my_voice_home_events_category' ) ) :
				$arg['tax_query'] = array(
					array(
						'taxonomy' => 'event-month',
						'field'    => 'slug',
						'terms'    => get_option( 'my_voice_home_events_category' ),
					),
				);
			endif;
			$loop = new WP_Query( $arg );

			if ( $loop->have_posts() && 'true' === get_option( 'my_voice_home_events_posts' ) ) :
				?>
				<div class="col-md-7 col-sm-12 col-xs-12">
					<?php if ( get_option( 'my_voice_home_events_title' ) ) : ?>
						<!--Heading Outer start-->
						<div class="tnit-heading-outer">
							<h3><?php echo esc_html( get_option( 'my_voice_home_events_title' ) ); ?></h3>
						</div><!--Heading Outer End-->
					<?php endif; ?>

					<!--Event Listed Start-->
					<ul class="tnit-even-listed">
						<?php

						/* Start the Loop */
						while ( $loop->have_posts() ) :
							$loop->the_post();
							get_template_part( 'template-parts/post/content', 'event' );
						endwhile;
						?>
					</ul><!--Event Listed End-->
					<?php if ( get_option( 'my_voice_home_events_pages' ) ) : ?>
						<a href="<?php echo esc_url( get_permalink( get_option( 'my_voice_home_events_pages' ) ) ); ?>" class="tnit-btn-style_v2 btn-event"><?php echo esc_html( get_option( 'my_voice_home_events_button_title' ) ); ?></a>
					<?php endif; ?>
				</div>
			<?php
			endif;
			?>
			<div class="col-md-5 col-sm-12 col-xs-12">
				<?php
				if ( is_active_sidebar( 'default-homepage-events-sidebar' ) ) {
					dynamic_sidebar( 'default-homepage-events-sidebar' );
				}
				?>
			</div>
		</div>
	</div>
</section><!--Section End-->
