<?php
/**
 * Partners template part for home
 *
 * Page template for homepage.
 *
 * @since 1.0.0
 * @package My Voice
 */
if ( 'true' === get_option( 'my_voice_home_facts' ) ) :
?>
<!--Partners Section start-->
<section class="tnit-partners-section pd-b70">
	<div class="container">
		<!--Partners Slider Start-->
		<div id="tnit-partners-slider" class="owl-carousel">
			<?php
			$home_partners_list = get_theme_mod( 'my_voice_home_partners_details' );
			/*This returns a json so we have to decode it*/
			$home_partners_list_decoded = json_decode( $home_partners_list );
			foreach ( $home_partners_list_decoded as $repeater_item ) {
				?>
				<!--Partners Item Start-->
				<div class="item">
					<a href="<?php echo esc_url( $repeater_item->link ); ?>"><img src="<?php echo esc_url( $repeater_item->image_url ); ?>" alt=""></a>
				</div><!--Partners Item End-->
				<?php
			}
			?>
		</div><!--Partners Slider end-->
	</div>
</section><!--Partners Section End-->
<?php endif; ?>

