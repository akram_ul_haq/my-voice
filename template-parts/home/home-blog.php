<?php
/**
 * Blog template part for home
 *
 * Page template for homepage.
 *
 * @since    1.0.0
 * @package My Voice
 */


$arg = array(
	'post_type'      => 'post',
	'posts_per_page' => - 1,
);
if ( get_option( 'my_voice_home_blog_category' ) ) :
	$arg['tax_query'] = array(
		array(
			'taxonomy' => 'category',
			'field'    => 'slug',
			'terms'    => get_option( 'my_voice_home_blog_category' ),
		),
	);
endif;
$loop = new WP_Query($arg);

if ( $loop->have_posts() && 'true' === get_option( 'my_voice_home_blog_posts' ) ) :
	if ( get_option( 'my_voice_home_blog_watermark_image' ) ) :
		?>
        <style>
            .tnit-blog-section .tnit-container-before:before {
                background-image: url("<?php echo esc_url( get_option( 'my_voice_home_blog_watermark_image' ) ); ?>");
            }
        </style>

	<?php endif; ?>
	<!--Blog Section Start-->
	<section class="tnit-blog-section pd-tb70">
		<div class="container tnit-container-before">
			<?php if ( get_option( 'my_voice_home_blog_title' ) ) : ?>
				<!--Heading Outer start-->
				<div class="tnit-heading-outer text-center">
					<h2><?php echo esc_html( get_option( 'my_voice_home_blog_title' ) ); ?></h2>
				</div><!--Heading Outer End-->
			<?php endif; ?>

			<!--Blog Slider Start-->
			<div id="tnit-blog-slider" class="owl-carousel">
				<?php

				/* Start the Loop */
				while ( $loop->have_posts() ) :
					$loop->the_post();
					?>
					<!--Blog Item Start-->
					<div class="item">
                        <div id="post-<?php the_ID(); ?>" <?php post_class( 'tnit-blog-item' ); ?>>
                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <!--Blog Thumb Start-->
                                    <figure class="tnit-thumb">
										<?php
										if ( '' !== get_the_post_thumbnail() ) :
											the_post_thumbnail( 'my-voice-home-featured-image' );
										endif;
										?>
                                        <figcaption class="tnit-caption">
                                            <div class="date-box entry-date published">
                                                <span><?php the_time( 'd' ); ?></span>
												<?php the_time( 'M' ); ?>
                                            </div>
                                            <a class="tnit-play-btn" href="
					<?php
											$video_url = get_post_meta( get_the_ID(), 'MY_VOICE_embed_url', true );
											echo $video_url ? esc_url( $video_url ) : esc_url( get_the_permalink() );
											?>
					">
                                                <span class="icomoon icon-play-rounded-button"></span>
                                            </a>
                                        </figcaption>
                                    </figure><!--Blog Thumb End-->
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <!--Blog Text Start-->
                                    <div class="tnit-text">
										<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
                                        <!--Meta Listed Start-->
                                        <ul class="tnit-meta-listed tnit-meta-listed2">
                                            <li><?php my_voice_posted_on(); ?></li>
											<?php
											$separate_meta = __( ' , ', 'myvoice' );

											// Get Categories for posts.
											$categories_list = get_the_category_list( $separate_meta );
											if ( $categories_list ) :
												echo "<li>$categories_list </li>";
											endif;
											?>
                                            <li><?php comments_number( '0 Comment', '1 Comment', '% Comments' ); ?></li>
                                        </ul><!--Meta Listed End-->
                                        <p><?php the_excerpt_max_char_length( 100 ); ?></p>
                                    </div><!--Blog Text End-->
                                </div>
                            </div>
                        </div>
                    </div>
					<!--Blog Item End-->
				<?php
				endwhile;
				?>
			</div><!--Blog Slider End-->
            <div class="tnit-btn-group text-center">
                <a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ); ?>" class="tnit-btn-style1"><?php esc_html_e( 'View All Articles', 'myvoice' ); ?></a>
            </div>
		</div>
	</section>
	<!--Blog Section End-->
<?php
endif;
?>
