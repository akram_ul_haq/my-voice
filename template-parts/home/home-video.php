<?php
/**
 * Video template part for home
 *
 * Page template for homepage.
 *
 * @since    1.0.0
 * @package My Voice
 */

global $wpdb;
/* Get post IDs of all pages using "template-faq.php" */
$faq_pages = $wpdb->get_results( "SELECT `post_id` FROM $wpdb->postmeta WHERE `meta_key` ='_wp_page_template' AND `meta_value` = 'templates/video.php' ", ARRAY_A );
/* Get permalink using post ID of first page in the results */
$video_permalink = get_permalink( $faq_pages[0]['post_id'] );
$arg             = array(
	'post_type'      => 'video',
	'posts_per_page' => 4,
);
if ( get_option( 'my_voice_home_videos_category' ) ) :
	$arg['tax_query'] = array(
		array(
			'taxonomy' => 'video-type',
			'field'    => 'slug',
			'terms'    => get_option( 'my_voice_home_videos_category' ),
		),
	);
endif;
$loop = new WP_Query( $arg );
if ( $loop->have_posts() && 'true' === get_option( 'my_voice_home_videos_posts' ) ) :
	if ( get_option( 'my_voice_home_videos_watermark_image' ) ) :
		?>
        <style>
            .tnit-video-section .tnit-container-before:before {
                background-image: url("<?php echo esc_url( get_option( 'my_voice_home_videos_watermark_image' ) ); ?>");
            }
        </style>

	<?php endif; ?>
	<!--Video Section Start-->
	<section class="tnit-video-section pd-tb70">
		<div class="container tnit-container-before">
			<?php if ( get_option( 'my_voice_home_videos_title' ) ) : ?>
				<!--Heading Outer start-->
				<div class="tnit-heading-outer text-center">
					<h2><?php echo esc_html( get_option( 'my_voice_home_videos_title' ) ); ?></h2>
				</div><!--Heading Outer End-->
			<?php endif; ?>
			<div class="row">
				<?php

				/* Start the Loop */
				while ( $loop->have_posts() ) :
					$loop->the_post();
					?>
					<div class="col-md-3 col-sm-6 col-xs-12">
						<!--Video Item Start-->
						<div id="post-<?php the_ID(); ?>" <?php post_class( 'tnit-video-item' ); ?>>
							<figure class="video-thumb">
								<?php
								$video_code = get_post_meta( get_the_ID(), 'MY_VOICE_embed_code', true );
								if ( '' !== get_the_post_thumbnail() ) :
									the_post_thumbnail( 'my-voice-video-featured-image' );
								else :
									the_post_thumbnail( 'my-voice-single-featured-image' );
								endif;
								?>
								<figcaption class="tnit-caption">
                                    <a class="icon-box" href="<?php the_permalink(); ?>">
                                        <i class="fa fa-play" aria-hidden="true"></i>
                                    </a>
									<div class="bottom-text">
										<!--Meta Listed Start-->
										<ul class="tnit-meta-listed">
											<li><i class="fa fa-calendar-minus-o" aria-hidden="true"></i><?php echo get_the_date(); ?></li>
											<li><i class="fa fa-comments-o" aria-hidden="true"></i> <?php comments_number( '0', '1', '%' ); ?></li>
										</ul><!--Meta Listed End-->
									</div>
								</figcaption>
							</figure>
							<div class="tnit-text-outer">
								<?php the_title( '<h4 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' ); ?>
							</div>
						</div><!--Video Item End-->
					</div>
				<?php
				endwhile;
				?>
			</div>
            <div class="tnit-btn-group text-center">
                <a href="<?php echo esc_url( $video_permalink ); ?>" class="tnit-btn-style1"><?php esc_html_e( 'View All Lactures', 'myvoice' ); ?></a>
            </div>
		</div>
	</section><!--Video Section End-->
<?php
endif;
?>
