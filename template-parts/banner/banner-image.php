<?php
/**
 * Banner: Image
 *
 * Image banner for page templates.
 *
 * @since 1.0
 * @package My Voice
 */

global $post;

// Banner Image.
$banner_image_path = '';

/* If posts page is set in Reading Settings */
$page_for_posts  = get_option( 'page_for_posts' );
$banner_image_id = get_post_meta( $post->ID, 'MY_VOICE_page_banner_image', true );

if ( is_blog() ) {
	$banner_image_id = get_post_meta( $page_for_posts, 'MY_VOICE_page_banner_image', true );
	if ( $banner_image_id ) {
		$banner_image_path = wp_get_attachment_url( $banner_image_id );
	} else {
		$banner_image_path = get_option( 'theme_general_banner_image' );
	}
	// Banner Title.
	$banner_title = get_post_meta( $page_for_posts, 'MY_VOICE_banner_title', true );
	if ( empty( $banner_title ) ) {
		$banner_title = __( 'Blog', 'myvoice' );
	}
} else {
	if ( $banner_image_id ) {
		$banner_image_path = wp_get_attachment_url( $banner_image_id );
	} else {
		$banner_image_path = get_option( 'theme_general_banner_image' );
	}
	// Banner Title.
	$banner_title = get_post_meta( $post->ID, 'MY_VOICE_banner_title', true );
	if ( empty( $banner_title ) ) {
		$banner_title = get_the_title( $post->ID );
	}
}


// Show banner title.
$theme_banner_titles = get_option( 'theme_banner_titles' );
if ( empty( $banner_image_path ) ) {
	$banner_image_path = get_template_directory_uri() . '/assets/images/banner-img-02.jpg';
}
?>

<!--Inner Banner Content Start-->
<div class="tnit-inner-banner tnit-banner-image_v2" style="background-repeat: no-repeat;background-position: center top;background-image: url('<?php echo esc_url( $banner_image_path ); ?>'); background-size: cover; ">
    <div class="container">
		<?php if ( empty( $theme_banner_titles ) || 'false' === $theme_banner_titles ) : ?>
            <h2>
				<?php echo esc_html( $banner_title ); ?>
            </h2>
            <!-- /.rh_banner__title -->
		<?php endif; ?>
        <!--BreadCrumb Listed Start-->
		<?php custom_breadcrumbs(); ?>
        <!--BreadCrumb Listed End-->
    </div>
</div><!--Inner Banner Content End-->