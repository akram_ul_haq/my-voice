<?php

if ( ! function_exists( 'my_voice_import_files' ) ) {
	/**
	 * Demo contents import
	 */
	function my_voice_import_files() {
		return array(
			array(
				'import_file_name'             => esc_html__( 'My Voice Demo Import', 'myvoice' ),
				'local_import_file'            => trailingslashit( get_template_directory() ) . 'assets/demo/import/contents.xml',
				'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'assets/demo/import/widgets.wie',
				'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'assets/demo/import/customizer.dat',
				'import_notice'                => htmlspecialchars_decode( esc_html__( 'Do not close the window or refresh the page until the data is imported. After demo import, setup the permalinks settings to <strong>Post name</strong> in Settings > Permalinks page.', 'myvoice' ) ),
			)
		);
	}

	add_filter( 'pt-ocdi/import_files', 'my_voice_import_files' );
}

if ( ! function_exists( 'my_voice_after_import' ) ) {
	function my_voice_after_import() {
		// Assign menus to their locations.
		$main_menu = get_term_by( 'name', 'Top Menu', 'nav_menu' );

		set_theme_mod( 'nav_menu_locations', array(
				'top' => $main_menu->term_id,
			)
		);

		// Assign front page and posts page (blog page).
		$front_page_id = get_page_by_title( 'Home' );
		$blog_page_id  = get_page_by_title( 'Blog' );

		update_option( 'show_on_front', 'page' );
		update_option( 'page_on_front', $front_page_id->ID );
		update_option( 'page_for_posts', $blog_page_id->ID );
	}

	add_action( 'pt-ocdi/after_import', 'my_voice_after_import' );
}

function ocdi_change_time_of_single_ajax_call() {
	return 10;
}
add_action( 'pt-ocdi/time_for_one_ajax_call', 'ocdi_change_time_of_single_ajax_call' );
