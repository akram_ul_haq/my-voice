<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

get_header();

/*
* Banner
*/
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Video Section Start-->
		<section class="tnit-video-section tnit-video-section-detail pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 col-xs-12">
						<!--Video Holder Start-->
						<?php
						while ( have_posts() ) :
						the_post();
						echo '<div class="tnit-video-holder">';
						$video_code = get_post_meta( get_the_ID(), 'MY_VOICE_embed_code', true );
						if ( $video_code ) :
							echo wp_kses( $video_code, array(
									'iframe' => array(
										'src'             => array(),
										'allowfullscreen' => array(),
									),
								)
							);
						else :
							the_post_thumbnail( 'my-voice-single-featured-image' );
						endif;
						echo '</div>';
						?>
						<!--Video Holder End-->
						<!--Video Text Start-->
						<div class="tnit-video-text">
							<div class="top">
								<h3><?php the_title(); ?></h3>
								<ul class="tnit-meta-listed">
									<li>
										<i class="fa fa-user" aria-hidden="true"></i>
										<a href="#"><?php my_voice_posted_on(); ?></a>
									</li>
									<li>
										<i class="fa fa-calendar-minus-o" aria-hidden="true"></i>
										<?php the_date(); ?>
									</li>
									<li><i class="fa fa-comments-o" aria-hidden="true"></i>
										<?php comments_number( '0', '1', '%' ); ?>
									</li>
								</ul>
							</div>
							<?php the_content(); ?>
							<div class="bottom-text">
								<div class="row">
									<div class="col-md-8 col-sm-8 col-xs-12">
										<div class="tnit-tags-outer">
											<?php
											$separate_tag_meta = __( ' , ', 'myvoice' );
											// Get Tags for posts.
											$tags_list = get_the_tag_list( '<ul class="widget-tags"><li>', '</li><li>', '</ul>' );
											if ( $tags_list ) {
												echo '<h4>' . esc_html__( 'Tags', 'myvoice' ) . '</h4>';
												echo $tags_list;
											}
											?>
										</div>
									</div>
									<div class="col-md-4 co-sm-4 col-xs-12">
										<ul class="tnit-btns-listed">
											<li class="share-holder">
												<a href="#">
													<i class="fa fa-share-alt" aria-hidden="true"></i>
													<?php esc_html_e( 'Share', 'myvoice' ); ?>
												</a>
												<ul class="tnit-social-links_v3" id="tnit-social-overlay">
													<li class="btn-facebook active">
														<a target="_blank"
														   href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>">
															<i class="fa fa-facebook" aria-hidden="true"></i>
															<?php esc_html_e( 'FACEBOOK', 'myvoice' ); ?>
														</a>
													</li>
													<li class="btn-twitter">
														<a target="_blank"
														   href="https://twitter.com/share?url=<?php the_permalink(); ?>&via=<?php the_author(); ?>&text=<?php the_title(); ?>">
															<i class="fa fa-twitter" aria-hidden="true"></i>
															<?php esc_html_e( 'TWITTER', 'myvoice' ); ?>
														</a>
													</li>
													<li class="btn-gplus">
														<a target="_blank"
														   href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
															<i class="fa fa-google-plus" aria-hidden="true"></i>
															<?php esc_html_e( 'GOOGLE+', 'myvoice' ); ?>
														</a>
													</li>
													<li class="btn-linkedin">
														<a target="_blank"
														   href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>">
															<i class="fa fa-linkedin" aria-hidden="true"></i>
															<?php esc_html_e( 'LINKEDIN', 'myvoice' ); ?>
														</a>
													</li>
												</ul>
											</li>
											<li class="btn-heart">
												<?php echo get_simple_likes_button( get_the_ID() ); ?>
											</li>

										</ul>
									</div>
								</div>
							</div>
							<?php endwhile; // End of the loop. ?>
							<!--Tabs Outer Start-->
							<div class="tl-tabs-outer">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs">
									<li role="presentation" class="active">
										<a href="#tl-tab-list1" aria-controls="tl-tab-list1" role="tab"
										   data-toggle="tab">
											<?php esc_html_e( 'Leave A Replly', 'myvoice' ); ?>
										</a>
									</li>
									<li role="presentation">
										<a href="#tl-tab-list2" aria-controls="tl-tab-list2" role="tab"
										   data-toggle="tab">
											<?php esc_html_e( 'Comments', 'myvoice' ); ?>
										</a>
									</li>
								</ul>
								<!-- Nav tabs End-->

								<!-- Tab Content Start -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="tl-tab-list1">
										<!--Leave Review Outer Start-->
										<div class="tl-review-outer">
											<?php
											if ( is_user_logged_in() ) :
												$commets_calss = 'col-xs-12';
											else :
												$commets_calss = 'col-md-6 col-sm-6 col-xs-12';
											endif;

											$args          = '';
											$commenter     = wp_get_current_commenter();
											$user          = wp_get_current_user();
											$user_identity = $user->exists() ? $user->display_name : '';

											$args = wp_parse_args( $args );
											if ( ! isset( $args['format'] ) ) {
												$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
											}

											$req           = get_option( 'require_name_email' );
											$aria_req      = ( $req ? " aria-required='true'" : '' );
											$html_req      = ( $req ? " required='required'" : '' );
											$html5         = 'html5' === $args['format'];
											$custom_fields = array(
												'author' => '<div class="col-md-6 col-sm-6 col-xs-12"><div class="inner-holder">' . '<label for="author">' . __( 'Name', 'myvoice' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
												            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245" ' . $aria_req . $html_req . ' /></div>',
												'email'  => '<div class="inner-holder"><label for="email">' . __( 'Email', 'myvoice' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' . '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req . ' /></div></div>',
												'url'    => '',
											);

											$comments_arg = array(
												'fields'               => $custom_fields,
												'class_form'           => 'tl-review-form row',
												'title_reply'          => __( 'Leave a REPLY', 'myvoice' ),
												'title_reply_to'       => __( 'Leave a REPLY to %s', 'myvoice' ),
												'class_submit'         => 'btn-submit',
												'comment_notes_before' => '<p class="col-xs-12 comment-notes"><span id="email-notes">' . __( 'Your email address will not be published.', 'myvoice' ) . '</span></p>',
												'submit_field'         => '<div class="col-xs-12 form-submit">%1$s %2$s</div>',
												'title_reply_before'   => '<div class="block-title"><h3 id="reply-title" class="comment-reply-title">',
												'title_reply_after'    => '</h3></div>',
												'comment_field'        => '<div class="' . $commets_calss . '"> <div class="inner-holder"><label for="comment">' . _x( 'Message *', 'noun', 'myvoice' ) . '</label> <textarea id="comment" name="comment" aria-required="true" required="required"></textarea></div></div>',

											);
											comment_form( $comments_arg );
											?>
										</div><!--Leave Review Outer End-->

									</div>
									<div role="tabpanel" class="tab-pane" id="tl-tab-list2">
										<div class="row">
											<?php if ( is_single() && isset( $post->post_author ) ) : ?>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<!--Comment Box Start-->
													<div class="tl-comments-box">
														<ul class="tl-comments-listed">
															<li>
																<div class="tl-comments-inner">
																	<figure class="tl-thumb">
																		<?php echo get_avatar( get_the_author_meta( 'user_email' ), 60 ); ?>
																	</figure>
																	<div class="text-holder">
																		<h4>
																			<?php
																			the_author();
																			$u_data     = get_userdata( $user->ID );
																			$registered = $u_data->user_registered;
																			?>
																			–
																			<span class="date">
																					<?php echo date( "M d, Y", strtotime( $registered ) ); ?>
																				</span>
																		</h4>
																		<?php
																		$user_description = get_the_author_meta( 'user_description', $post->post_author );
																		if ( $user_description ) {
																			echo '<p>' . esc_html( $user_description ) . '</p>';
																		}
																		?>
																	</div>
																</div>
															</li>
														</ul>
													</div><!--Comment Box End-->
												</div>
											<?php endif; ?>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<!--Comment Box Start-->
												<?php
												if ( comments_open() || get_comments_number() ) :
													comments_template();
												endif;
												?>
												<!--Comment Box End-->
											</div>

										</div>
									</div>
								</div><!-- Tab Content End -->
							</div><!--Tabs Outer End-->
						</div>
						<!--Inner Video Outer Start-->
						<?php
						$cats = array();
						if ( get_option( 'my_voice_related_video_category' ) ) :
							$cats[] = get_option( 'my_voice_related_video_category' );
						else :
							$terms = get_the_terms( get_the_ID(), 'video-type' );
							foreach ( $terms as $term ) {
								$cats[] = $term->slug;
							}
						endif;
						$loop = new WP_Query(
							array(
								'posts_per_page' => 3,
								'post__not_in'   => array( get_the_ID() ),
								'tax_query'      => array(
									array(
										'taxonomy' => 'video-type',
										'field'    => 'term_id',
										'terms'    => $cats,
									),
								),
							)
						);

						if ( $loop->have_posts() && 'true' === get_option( 'my_voice_related_video_posts' ) ) :
							?>
							<div class="tnit-video-inner-outer border-none">
								<!--Heading Outer start-->
								<?php if ( get_option( 'my_voice_related_video_title' ) ) : ?>
									<div class="tnit-heading-outer">
										<h3><?php echo esc_html( get_option( 'my_voice_related_video_title' ) ); ?></h3>
									</div>
								<?php endif; ?>
								<!--Heading Outer End-->
								<div class="row">
									<?php
									/* Start the Loop */
									while ( $loop->have_posts() ) :
										$loop->the_post();
										get_template_part( 'template-parts/post/content', 'video' );
									endwhile;
									?>
								</div>
							</div>
						<?php
						endif;
						?>
					</div>
					<!--Inner Video Outer End-->
					<div class="col-md-3 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php
							if ( is_active_sidebar( 'default-video-sidebar' ) ) {
								dynamic_sidebar( 'default-video-sidebar' );
							}
							?>
						</aside><!--Sidebar Outer End-->
					</div>
				</div>
		</section><!--Video Section End-->

	</div><!--Main Content End-->
<?php get_footer();
