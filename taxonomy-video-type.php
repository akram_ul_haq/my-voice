<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

get_header();

// Banner Image.
$banner_image_path = '';

$banner_image_path = get_option( 'theme_general_banner_image' );
if ( empty( $banner_image_path ) ) {
	$banner_image_path = get_template_directory_uri() . '/assets/images/banner-img-02.jpg';
}
?>
	<!--Inner Banner Content Start-->
	<div class="tnit-inner-banner tnit-banner-image_v2"
	     style="background-repeat: no-repeat; background-position: center top; background-image: url('<?php echo esc_url( $banner_image_path ); ?>'); background-size: cover; ">
		<div class="container">
			<h2>
				<?php
				if ( is_category() ) {
					esc_html_e( 'Category', 'myvoice' );
				} elseif ( is_tag() ) {
					/* translators: Tag archive title. 1: Tag name */
					esc_html_e( 'Tag', 'myvoice' );
				} elseif ( is_author() ) {
					/* translators: Author archive title. 1: Author name */
					esc_html_e( 'Author', 'myvoice' );
				} elseif ( is_year() ) {
					/* translators: Yearly archive title. 1: Year */
					esc_html_e( 'Year', 'myvoice' );
				} elseif ( is_month() ) {
					/* translators: Monthly archive title. 1: Month name and year */
					esc_html_e( 'Month', 'myvoice' );
				} elseif ( is_day() ) {
					/* translators: Daily archive title. 1: Date */
					esc_html_e( 'Day', 'myvoice' );
				} else {
					esc_html_e( 'Archive', 'myvoice' );
				}
				?>
			</h2>
			<!-- /.rh_banner__title -->

			<!--BreadCrumb Listed Start-->
			<?php custom_breadcrumbs(); ?>
			<!--BreadCrumb Listed End-->
		</div>
	</div><!--Inner Banner Content End-->

	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Video Section Start-->
		<section class="tnit-video-section tnit-video-section_v2 pd-tb70">
			<div class="container">
				<!--Inner Video Outer Start-->
				<div class="tnit-blog-inner-outer">
					<!--Heading Outer start-->
					<div class="tnit-heading-outer">
						<?php the_archive_title( '<h2 class="page-title">', '</h2>' ); ?>
					</div><!--Heading Outer End-->
					<div class="row">
						<?php
						if ( have_posts() ) :

							/* Start the Loop */
							while ( have_posts() ) :
								the_post();

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */
								get_template_part( 'template-parts/post/content', 'video' );

							endwhile;
						endif;
						?>
					</div>
				</div><!--Inner Video Outer End-->

			</div>
		</section><!--Video Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>
