<?php
/**
 * Single Event
 *
 * Template for all event post.
 *
 * @since    1.0.0
 * @package My Voice
 */

get_header();


/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Event Section Start-->
		<section class="tnit-event-section pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 col-xs-12">
						<!--Event Outer Start-->
						<div class="tnit-event-outer">
							<?php
							while ( have_posts() ) :
								the_post();
								?>
								<h2><?php the_title(); ?></h2>
								<!--Event Thumb Start-->
								<figure class="tl-event-thumb">
									<?php the_post_thumbnail( 'my-voice-single-featured-image' ); ?>
									<figcaption class="tl-caption">

										<?php
										$event_date_time = get_post_meta( get_the_ID(), 'MY_VOICE_event_date_time', true );
										$interval        = date_diff( new DateTime(), new DateTime( $event_date_time ) );
										?>
										<div class="top-holder">
											<!--Counter Listed Start-->
											<ul class="tl-counter-listed">
												<!--Counter Item Start-->
												<li class="tl-counter-item">
													<span class="tnit-counter"><?php echo esc_html( $interval->days ); ?></span>
													<span class="info"><?php esc_html_e( 'Days', 'myvoice' ); ?></span>
												</li><!--Counter Item End-->
												<!--Counter Item Start-->
												<li class="tl-counter-item">
													<span class="tnit-counter"><?php echo esc_html( $interval->h ); ?></span>
													<span class="info"><?php esc_html_e( 'Hours', 'myvoice' ); ?></span>
												</li><!--Counter Item End-->
												<!--Counter Item Start-->
												<li class="tl-counter-item">
													<span class="tnit-counter"><?php echo esc_html( $interval->m ); ?></span>
													<span class="info"><?php esc_html_e( 'Minutes', 'myvoice' ); ?></span>
												</li><!--Counter Item End-->
												<!--Counter Item Start-->
												<li class="tl-counter-item">
													<span class="tnit-counter"><?php echo esc_html( $interval->s ); ?></span>
													<span class="info"><?php esc_html_e( 'Seconds', 'myvoice' ); ?></span>
												</li><!--Counter Item End-->
											</ul><!--Counter Listed End-->
										</div>
										<div class="bottom-text">
											<!--Meta Listed Start-->
											<ul class="tnit-meta-listed">
												<li>
													<i class="fa fa-user" aria-hidden="true"></i>
													<div class="text">
														<span><?php esc_html_e( 'Organizer', 'myvoice' ); ?></span>
														<span><?php the_author(); ?></span>
													</div>
												</li>
												<?php
												$event_timing = get_post_meta( get_the_ID(), 'MY_VOICE_event_timing', true );
												if ( $event_timing ) {
													?>
													<li>
														<i class="fa fa-map-signs" aria-hidden="true"></i>
														<div class="text">
															<span><?php esc_html_e( 'Timing', 'myvoice' ); ?></span>
															<span><?php echo esc_html( $event_timing ); ?></span>
														</div>
													</li>
												<?php } ?>
												<?php
												$event_location = get_post_meta( get_the_ID(), 'MY_VOICE_event_location', true );
												if ( $event_location ) {
													?>
													<li>
														<i class="fa fa-map-signs" aria-hidden="true"></i>
														<div class="text">
															<span><?php esc_html_e( 'Location', 'myvoice' ); ?></span>
															<span><?php echo esc_html( $event_location ); ?></span>
														</div>
													</li>
												<?php } ?>
											</ul><!--Meta Listed End-->
											<!--Button Listed Start-->
											<ul class="tnit-btns-listed">
												<li class="share-holder">
													<a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i> Share</a>
													<ul class="tnit-social-links_v3" id="tnit-social-overlay">
														<li class="btn-facebook active">
															<a target="_blank"
															   href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php the_title(); ?>">
																<i class="fa fa-facebook"
																   aria-hidden="true"></i>
																<?php esc_html_e( 'FACEBOOK', 'myvoice' ); ?>
															</a>
														</li>
														<li class="btn-twitter">
															<a target="_blank"
															   href="https://twitter.com/share?url=<?php the_permalink(); ?>&via=<?php the_author(); ?>&text=<?php the_title(); ?>">
																<i class="fa fa-twitter" aria-hidden="true"></i>
																<?php esc_html_e( 'TWITTER', 'myvoice' ); ?>
															</a>
														</li>
														<li class="btn-gplus">
															<a target="_blank"
															   href="https://plus.google.com/share?url=<?php the_permalink(); ?>">
																<i class="fa fa-google-plus"
																   aria-hidden="true"></i>
																<?php esc_html_e( 'GOOGLE+', 'myvoice' ); ?>
															</a>
														</li>
														<li class="btn-linkedin">
															<a target="_blank"
															   href="http://www.linkedin.com/shareArticle?mini=true&amp;title=<?php the_title(); ?>&amp;url=<?php the_permalink(); ?>">
																<i class="fa fa-linkedin"
																   aria-hidden="true"></i>
																<?php esc_html_e( 'LINKEDIN', 'myvoice' ); ?>
															</a>
														</li>
													</ul>
												</li>
												<li class="btn-heart">
													<?php echo get_simple_likes_button( get_the_ID() ); ?>
												</li>
											</ul><!--Button Listed End-->
										</div>
									</figcaption>
								</figure><!--Event Thumb End-->
								<?php the_content(); ?>

							<?php endwhile; // End of the loop. ?>
							<!--Tabs Outer Start-->
							<div class="tl-tabs-outer">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs">
									<li role="presentation" class="active">
										<a href="#tl-tab-list1" aria-controls="tl-tab-list1" role="tab"
										   data-toggle="tab">
											<?php esc_html_e( 'Leave A Replly', 'myvoice' ); ?>
										</a>
									</li>
									<li role="presentation">
										<a href="#tl-tab-list2" aria-controls="tl-tab-list2" role="tab"
										   data-toggle="tab">
											<?php esc_html_e( 'Comments', 'myvoice' ); ?>
										</a>
									</li>
								</ul><!-- Nav tabs End-->

								<!-- Tab Content Start -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="tl-tab-list1">
										<!--Leave Review Outer Start-->
										<div class="tl-review-outer">
											<?php
											if ( is_user_logged_in() ) :
												$commets_calss = 'col-xs-12';
											else :
												$commets_calss = 'col-md-6 col-sm-6 col-xs-12';
											endif;

											$args          = '';
											$commenter     = wp_get_current_commenter();
											$user          = wp_get_current_user();
											$user_identity = $user->exists() ? $user->display_name : '';

											$args = wp_parse_args( $args );
											if ( ! isset( $args['format'] ) ) {
												$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
											}

											$req           = get_option( 'require_name_email' );
											$aria_req      = ( $req ? " aria-required='true'" : '' );
											$html_req      = ( $req ? " required='required'" : '' );
											$html5         = 'html5' === $args['format'];
											$custom_fields = array(
												'author' => '<div class="col-md-6 col-sm-6 col-xs-12"><div class="inner-holder">' . '<label for="author">' . __( 'Name', 'myvoice' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
												            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245" ' . $aria_req . $html_req . ' /></div>',
												'email'  => '<div class="inner-holder"><label for="email">' . __( 'Email', 'myvoice' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' . '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req . ' /></div></div>',
												'url'    => '',
											);

											$comments_arg = array(
												'fields'               => $custom_fields,
												'class_form'           => 'tl-review-form row',
												'title_reply'          => __( 'Leave a REPLY', 'myvoice' ),
												'title_reply_to'       => __( 'Leave a REPLY to %s', 'myvoice' ),
												'class_submit'         => 'btn-submit',
												'comment_notes_before' => '<p class="col-xs-12 comment-notes"><span id="email-notes">' . __( 'Your email address will not be published.', 'myvoice' ) . '</span></p>',
												'submit_field'         => '<div class="col-xs-12 form-submit">%1$s %2$s</div>',
												'title_reply_before'   => '<div class="block-title"><h3 id="reply-title" class="comment-reply-title">',
												'title_reply_after'    => '</h3></div>',
												'comment_field'        => '<div class="' . $commets_calss . '"> <div class="inner-holder"><label for="comment">' . _x( 'Message *', 'noun', 'myvoice' ) . '</label> <textarea id="comment" name="comment" aria-required="true" required="required"></textarea></div></div>',

											);
											comment_form( $comments_arg );
											?>
										</div><!--Leave Review Outer End-->

									</div>
									<div role="tabpanel" class="tab-pane" id="tl-tab-list2">
										<div class="row">
											<?php if ( is_single() && isset( $post->post_author ) ) : ?>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<!--Comment Box Start-->
													<div class="tl-comments-box">
														<ul class="tl-comments-listed">
															<li>
																<div class="tl-comments-inner">
																	<figure class="tl-thumb">
																		<?php echo get_avatar( get_the_author_meta( 'user_email' ), 60 ); ?>
																	</figure>
																	<div class="text-holder">
																		<h5>
																			<?php
																			echo '<h4>';
																			the_author();
																			$u_data     = get_userdata( $user->ID );
																			$registered = $u_data->user_registered;
																			?>
																			–
																			<span class="date">
																					<?php echo date( "M d, Y", strtotime( $registered ) ); ?>
																				</span>
																			</h4>
																		</h5>
																		<?php
																		$user_description = get_the_author_meta( 'user_description', $post->post_author );
																		if ( $user_description ) {
																			echo '<p>' . esc_html( $user_description ) . '</p>';
																		}
																		?>
																	</div>
																</div>
															</li>
														</ul>
													</div><!--Comment Box End-->
												</div>
											<?php endif; ?>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<!--Comment Box Start-->
												<?php
												if ( comments_open() || get_comments_number() ) :
													comments_template();
												endif;
												?>
												<!--Comment Box End-->
											</div>

										</div>
									</div>
								</div><!-- Tab Content End -->
							</div>
							<!--Tabs Outer End-->
							<?php
							$cats = array();
							if ( get_option( 'my_voice_related_event_category' ) ) :
								$cats[] = get_option( 'my_voice_related_event_category' );
							else :
								$terms = get_the_terms( get_the_ID(), 'event-month' );
								foreach ( $terms as $term ) {
									$cats[] = $term->slug;
								}
							endif;
							$loop = new WP_Query(
								array(
									'posts_per_page' => 3,
									'post__not_in'   => array( get_the_ID() ),
									'tax_query'      => array(
										array(
											'taxonomy' => 'event-month',
											'field'    => 'term_id',
											'terms'    => $cats,
										),
									),
								)
							);

							if ( $loop->have_posts() && 'true' === get_option( 'my_voice_related_event_posts' ) ) :
								?>
								<!--Event Listed Start-->
								<ul class="tnit-even-listed">
									<!--Heading Outer start-->
									<?php if ( get_option( 'my_voice_related_event_title' ) ) : ?>
										<div class="tnit-heading-outer">
											<h3><?php echo esc_html( get_option( 'my_voice_related_event_title' ) ); ?></h3>
										</div>
									<?php endif; ?>
									<!--Heading Outer End-->
									<?php
									/* Start the Loop */
									while ( $loop->have_posts() ) :
										$loop->the_post();
										get_template_part( 'template-parts/post/content', 'event' );
									endwhile;
									?>
								</ul>
								<!--Event Listed End-->
							<?php
							endif;
							?>
						</div><!--Event Outer end-->
					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php
							if ( is_active_sidebar( 'default-events-sidebar' ) ) {
								dynamic_sidebar( 'default-events-sidebar' );
							}
							?>

						</aside>
						<!--Sidebar Outer End-->
					</div>
				</div>
			</div>
		</section><!--Event Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>