<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

get_header();

/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );

?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Video Section Start-->
		<section class="tnit-blog-section tnit-blog-section_v2 pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 col-xs-12">
						<!--Inner Video Outer Start-->
						<div class="tnit-video-inner-outer tnit-blog-inner-outer">
							<!--Heading Outer start-->
							<div class="tnit-heading-outer">
								<h2><?php echo esc_html( get_option( 'my_voice_page_blog_title' ) ); ?></h2>
							</div><!--Heading Outer End-->
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<?php
									while ( have_posts() ) :
										the_post();

										get_template_part( 'template-parts/page/content', 'page' );

									endwhile; // End of the loop.
									?>
								</div>
							</div>
							<!--Pagination Row Start-->
							<div class="tnit-pagination-row">
								<nav aria-label="navigation">
									<?php theme_pagination(); ?>
								</nav>
							</div>
							<!--Pagination Row End-->
						</div><!--Inner Video Outer End-->

					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php
							if ( is_active_sidebar( 'default-sidebar' ) ) {
								dynamic_sidebar( 'default-sidebar' );
							}
							?>

						</aside>
						<!--Sidebar Outer End-->
					</div>
				</div>
			</div>
		</section><!--Video Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>
