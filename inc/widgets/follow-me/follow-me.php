<?php

/********* Starting Twitter Widget for Footer *********/
if ( ! class_exists( 'My_Voice_Social_Info_Widget' ) ) {
	class My_Voice_Social_Info_Widget extends WP_Widget {
		function __construct() {


			$widget_ops = array(
				'classname' => 'widget-tweets',
				array( 'description' => __( 'This widget is to show social information.', 'myvoice' ) ),
				// Args.
			);
			parent::__construct( 'My_Voice_Social_Info_Widget', __( 'My Voice: Follow Me', 'myvoice' ), $widget_ops );
		}

		/********* Starting Twitter Widget Function *********/

		public function widget( $args, $instance ) {
			extract( $args );
			echo $before_widget;
			$title = false;

			$myvoice_social_info_title = $instance['myvoice_social_info_title'];
			$slider_social_list        = get_theme_mod( 'my_voice_home_slider_social' );
			if ( ! empty( $slider_social_list ) ) {
				?>
                <h3><?php echo esc_html( $myvoice_social_info_title ); ?></h3>
                <div class="row tnit-social-row_v2">
					<?php
					/*This returns a json so we have to decode it*/
					$slider_social_list_decoded = json_decode( $slider_social_list );
					foreach ( $slider_social_list_decoded as $repeater_item ) {
						?>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <!--Socail Box Start-->
                            <div class="tnit-social-box after-line">
                                <a href="#"><i class="fa <?php echo esc_html( $repeater_item->icon_value ); ?>" aria-hidden="true"></i></a>
                                <div class="tnit-text">
                                    <strong><?php echo do_shortcode( htmlspecialchars_decode( $repeater_item->shortcode ) ); ?></strong>
                                    <span><?php echo esc_html( $repeater_item->title ); ?></span>
                                </div>
                            </div><!--Socail Box End-->
                        </div>
						<?php
					}
					?>
                </div>
				<?php
			}

			echo $after_widget;
		}

		/********* Starting Twitter Widget Admin Form *********/

		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array(
				'myvoice_social_info_title' => 'Follow Me',
			) );

			$myvoice_social_info_title = $instance['myvoice_social_info_title'];
			?>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_social_info_title' ) ); ?>"><?php esc_html_e( 'Title', 'myvoice' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_social_info_title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_social_info_title' ) ); ?>" type="text" value="<?php echo esc_html( $myvoice_social_info_title ); ?>"/>
			</p>
			<?php
		}


		/**
		 * Starting Twitter Widget Update Function.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance                              = $old_instance;
			$instance['myvoice_social_info_title'] = strip_tags( $new_instance['myvoice_social_info_title'] );

			return $instance;
		}

	}
}
register_widget( 'My_Voice_Social_Info_Widget' );
?>
