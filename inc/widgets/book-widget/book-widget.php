<?php

/********* Starting Twitter Widget for Footer *********/
if ( ! class_exists( 'My_Voice_Book_Info_Widget' ) ) {
	class My_Voice_Book_Info_Widget extends WP_Widget {
		function __construct() {
			// Add Widget scripts.
			add_action( 'admin_enqueue_scripts', array( $this, 'scripts' ) );
			$widget_ops = array(
				'classname' => 'widget-book',
				array( 'description' => __( 'This widget is to show information about book.', 'myvoice' ) ),
				// Args.
			);
			parent::__construct( 'My_Voice_Book_Info_Widget', __( 'My Voice: Book Info Widget', 'myvoice' ), $widget_ops );
		}

		public function scripts() {
			wp_enqueue_script( 'media-upload' );
			wp_enqueue_media();
			wp_enqueue_script( 'book-script', get_template_directory_uri() . '/inc/widgets/book-widget/script.js', array( 'jquery' ) );
		}

		/********* Starting Twitter Widget Function *********/

		public function widget( $args, $instance ) {
			extract( $args );
			$myvoice_book_widget_bg_image = $instance['myvoice_book_widget_bg_image'];
			if ( ! empty( $myvoice_book_widget_bg_image ) ) {
				?>
                <style>
                    .widget-book {
                        background-image: url("<?php echo esc_url( $myvoice_book_widget_bg_image ); ?>");
                    }
                </style>
				<?php
			}
			echo $before_widget;
			$title = false;

			$myvoice_book_image       = $instance['myvoice_book_image'];
			$myvoice_book_info        = $instance['myvoice_book_info'];
			$myvoice_book_button_text = $instance['myvoice_book_button_text'];
			$myvoice_book_url         = $instance['myvoice_book_url'];
			if ( ! empty( $myvoice_book_image ) ) {
				?>
                <img src="<?php echo esc_url( $myvoice_book_image ); ?>" alt="<?php bloginfo( 'name' ); ?>"/>
				<?php
			}
			if ( ( ! empty( $myvoice_book_info ) ) ) {
				echo wp_kses_post( $myvoice_book_info );
			}
			if ( ! empty( $myvoice_book_button_text ) && ! empty( $myvoice_book_url ) ) {
				?>
                <a class="tnit-btn-download" href="<?php echo esc_url( $myvoice_book_url ); ?>"><i class="fa fa-download" aria-hidden="true"></i><?php echo esc_html( $myvoice_book_button_text ); ?>
                </a>
				<?php
			}
			echo $after_widget;
		}

		/********* Starting Twitter Widget Admin Form *********/

		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array(
				'myvoice_book_image'           => get_template_directory_uri() . '/assets/images/widget-book-img.png',
				'myvoice_book_widget_bg_image' => get_template_directory_uri() . '/assets/images/book-bg-img.jpg',
				'myvoice_book_info'            => '<h5>New 2017 Book From michle addams</h5><span>Available From June 12th 2017</span>',
				'myvoice_book_button_text'     => 'Download Book',
				'myvoice_book_url'             => '#',
			) );

			$myvoice_book_image           = $instance['myvoice_book_image'];
			$myvoice_book_info            = $instance['myvoice_book_info'];
			$myvoice_book_widget_bg_image = $instance['myvoice_book_widget_bg_image'];
			$myvoice_book_button_text     = $instance['myvoice_book_button_text'];
			$myvoice_book_url             = $instance['myvoice_book_url'];
			?>
            <p>
                <label for="<?php echo $this->get_field_id( 'myvoice_book_image' ); ?>"><?php _e( 'Book Image', 'myvoice' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'myvoice_book_image' ); ?>" name="<?php echo $this->get_field_name( 'myvoice_book_image' ); ?>" type="text" value="<?php echo esc_url( $myvoice_book_image ); ?>"/>
                <button class="upload_book_image button button-primary"><?php esc_html_e( 'Upload Book Image', 'myvoice' ); ?></button>
            </p>

            <p>
                <label for="<?php echo $this->get_field_id( 'myvoice_book_widget_bg_image' ); ?>"><?php _e( 'Background Image', 'myvoice' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'myvoice_book_widget_bg_image' ); ?>" name="<?php echo $this->get_field_name( 'myvoice_book_widget_bg_image' ); ?>" type="text" value="<?php echo esc_url( $myvoice_book_widget_bg_image ); ?>"/>
                <button class="upload_book_bg_image button button-primary"><?php esc_html_e( 'Upload Book Image', 'myvoice' ); ?></button>
            </p>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_book_info' ) ); ?>"><?php esc_html_e( 'Information', 'myvoice' ); ?></label>
                <textarea class="widefat" rows="5" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_book_info' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_book_info' ) ); ?>"><?php echo wp_kses_post( $myvoice_book_info ); ?></textarea>
            </p>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_book_button_text' ) ); ?>"><?php esc_html_e( 'Book Button Text', 'myvoice' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_book_button_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_book_button_text' ) ); ?>" type="text" value="<?php echo esc_html( $myvoice_book_button_text ); ?>"/>
            </p>
            <p>
                <label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_book_url' ) ); ?>"><?php esc_html_e( 'Book Download link', 'myvoice' ); ?></label>
                <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_book_url' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_book_url' ) ); ?>" type="text" value="<?php echo esc_url( $myvoice_book_url ); ?>"/>
            </p>
			<?php
		}


		/**
		 * Starting Twitter Widget Update Function.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance                                 = $old_instance;
			$instance['myvoice_book_image']           = strip_tags( $new_instance['myvoice_book_image'] );
			$instance['myvoice_book_widget_bg_image'] = strip_tags( $new_instance['myvoice_book_widget_bg_image'] );
			$instance['myvoice_book_info']            = wp_kses_post( $new_instance['myvoice_book_info'] );
			$instance['myvoice_book_button_text']     = esc_html( $new_instance['myvoice_book_button_text'] );
			$instance['myvoice_book_url']             = esc_url( $new_instance['myvoice_book_url'] );

			return $instance;
		}

	}
}
register_widget( 'My_Voice_Book_Info_Widget' );
?>
