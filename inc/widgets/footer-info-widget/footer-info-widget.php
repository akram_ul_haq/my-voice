<?php

/********* Starting Twitter Widget for Footer *********/
if ( ! class_exists( 'My_Voice_Footer_Info_Widget' ) ) {
	class My_Voice_Footer_Info_Widget extends WP_Widget {
		function __construct() {
			// Add Widget scripts.
			add_action('admin_enqueue_scripts', array($this, 'scripts'));
			$widget_ops = array(
				'classname' => 'about',
				array( 'description' => __( 'This widget is to show information with logo in footer.', 'myvoice' ) ),
				// Args.
			);
			parent::__construct( 'My_Voice_Footer_Info_Widget', __( 'My Voice: Footer Info Widget', 'myvoice' ), $widget_ops );
		}

        public function scripts()
        {
            wp_enqueue_script( 'media-upload' );
            wp_enqueue_media();
            wp_enqueue_script('our_admin', get_template_directory_uri() . '/inc/widgets/footer-info-widget/script.js', array('jquery'));
        }

		/********* Starting Twitter Widget Function *********/

		public function widget( $args, $instance ) {
			extract( $args );
			echo $before_widget;
            $title = false;

            $myvoice_image               = $instance['myvoice_image'];
            $myvoice_info_widget_text    = $instance['myvoice_info_widget_text'];
            $myvoice_info_widget_address = $instance['myvoice_info_widget_address'];
            $myvoice_info_widget_number  = $instance['myvoice_info_widget_number'];
            $myvoice_info_widget_email   = $instance['myvoice_info_widget_email'];
			if ( ! empty( $myvoice_image ) ) {
				?>
				<strong class="ft-logo">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo esc_url( $myvoice_image ); ?>" alt="<?php bloginfo( 'name' ); ?>" class="footer-logo"/></a>
				</strong>
				<?php
			}
			?>
			<p><?php echo esc_attr( $myvoice_info_widget_text ); ?>...</p>
			<ul class="about-info-listed">
			<?php
			if ( ! empty( $myvoice_info_widget_address ) ) {
				?>
				<li><i class="fa fa-map-marker" aria-hidden="true"></i><?php echo esc_attr( $myvoice_info_widget_address ); ?></li>
				<?php
			}
			if ( ! empty( $myvoice_info_widget_number ) ) {
				?>
				<li><a href="<?php esc_html_e('tel:', 'myvoice') ; echo esc_attr( $myvoice_info_widget_number ); ?>"><i class="fa fa-phone" aria-hidden="true"></i><?php echo esc_html( $myvoice_info_widget_number ); ?></li>
				<?php
			}
			if ( ! empty( $myvoice_info_widget_email ) ) {
				?>
                <li><a href="mailto:<?php echo sanitize_email( $myvoice_info_widget_email ); ?>" target="_top"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo sanitize_email( $myvoice_info_widget_email ); ?></a></li>
				<?php
			}

			echo ' </ul>';
			echo $after_widget;
		}

		/********* Starting Twitter Widget Admin Form *********/

		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array(
				'myvoice_image'               => get_template_directory_uri() . '/assets/images/ft-logo.png',
				'myvoice_info_widget_text'    => 'Suspendisse nec est nunc. Sed sed sem in libero lacinia suscipit non ac neque. Integer scelerisque tellus est.',
				'myvoice_info_widget_address' => '9311 Sherman Ave. Groton, CT 06340',
				'myvoice_info_widget_number'  => '+13456789',
				'myvoice_info_widget_email'   => 'info@myvoice.com',
			) );

			$myvoice_image               = $instance['myvoice_image'];
			$myvoice_info_widget_text    = $instance['myvoice_info_widget_text'];
			$myvoice_info_widget_address = $instance['myvoice_info_widget_address'];
			$myvoice_info_widget_number  = $instance['myvoice_info_widget_number'];
			$myvoice_info_widget_email   = $instance['myvoice_info_widget_email'];
			?>
			<p>
                <label for="<?php echo $this->get_field_id( 'myvoice_image' ); ?>"><?php _e( 'Image:', 'myvoice' ); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id( 'myvoice_image' ); ?>" name="<?php echo $this->get_field_name( 'myvoice_image' ); ?>" type="text" value="<?php echo esc_url( $myvoice_image ); ?>" />
                <button class="upload_image_button button button-primary"><?php esc_html_e( 'Upload Image', 'myvoice' ); ?></button>
            </p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_text' ) ); ?>"><?php esc_html_e( 'Information', 'myvoice' ); ?></label>
				<textarea class="widefat" rows="5" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_text' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_info_widget_text' ) ); ?>"><?php echo esc_attr( $myvoice_info_widget_text ); ?></textarea>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_address' ) ); ?>"><?php esc_html_e( 'Address', 'myvoice' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_address' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_info_widget_address' ) ); ?>" type="text" value="<?php echo esc_attr( $myvoice_info_widget_address ); ?>"/>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_number' ) ); ?>"><?php esc_html_e( 'Number', 'myvoice' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_number' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_info_widget_number' ) ); ?>" type="text" value="<?php echo esc_html( $myvoice_info_widget_number ); ?>"/>
			</p>
			<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_email' ) ); ?>"><?php esc_html_e( 'Email', 'myvoice' ); ?></label>
				<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'myvoice_info_widget_email' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'myvoice_info_widget_email' ) ); ?>" type="text" value="<?php echo sanitize_email( $myvoice_info_widget_email ); ?>"/>
			</p>
			<?php
		}


		/**
		 * Starting Twitter Widget Update Function.
		 */
		public function update( $new_instance, $old_instance ) {
			$instance                                = $old_instance;
			$instance['myvoice_image']               = strip_tags( $new_instance['myvoice_image'] );
			$instance['myvoice_info_widget_text']    = strip_tags( $new_instance['myvoice_info_widget_text'] );
			$instance['myvoice_info_widget_address'] = strip_tags( $new_instance['myvoice_info_widget_address'] );
			$instance['myvoice_info_widget_number']  = strip_tags( $new_instance['myvoice_info_widget_number'] );
			$instance['myvoice_info_widget_email']   = strip_tags( $new_instance['myvoice_info_widget_email'] );

			return $instance;
		}

	}
}
			register_widget( 'My_Voice_Footer_Info_Widget' );
?>
