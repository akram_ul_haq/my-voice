<?php
/**
 * Featured Courses Widget
 *
 * @package My Voice
 *
 * @version 1.0
 */

if ( ! class_exists( 'Featured_Courses_Widget' ) ) {
	/**
	 * Starting Featured Recipe Widget For Footer
	 */
	class Featured_Courses_Widget extends WP_Widget {
		function __construct() {
			parent::__construct(
				'featured_courses_widget', // Base ID.
				__( 'My Voice: Featured Courses Widget', 'myvoice' ), // Name.
				array( 'description' => __( 'This widget show featured courses.', 'myvoice' ), ) // Args.
			);
		}

		/**
		 * Starting Featured Recipe Widget Function
		 *
		 * @param $args array Adding paramters.
		 *
		 * @param $instance object - Instance careated.
		 */
		public function widget( $args, $instance ) {
			extract( $args );

			$title = apply_filters( 'widget_title', $instance['title'] );
			if ( empty( $title ) ) {
				$title = false;
			}

			$courses_count = $instance['post_per_page'];

			echo $before_widget;
			echo '<h3>' . $title . '</h3>
						<ul class="tnit-featured-listed">';
			$args = array( 'post_type' => 'courses', 'posts_per_page' => $courses_count );

			$rrf_posts_query = new WP_Query( $args );

			if ( $rrf_posts_query->have_posts() ) :
				while ( $rrf_posts_query->have_posts() ) :
					$rrf_posts_query->the_post();
					?>
					<li>
						<div class="feature-inner-holder">
							<div class="sm-thumb">
								<?php if ( has_post_thumbnail() ) { ?>
									<a href="<?php the_permalink(); ?>" class="img-box"><?php the_post_thumbnail( 'most-rated-thumb' ); ?></a>
								<?php } ?>
							</div>
							<div class="text-holder">
								<p class="entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
								<ul class="tnit-meta-listed">
									<li>
										<i class="fa fa-calendar-minus-o" aria-hidden="true"></i> <?php the_date(); ?>
									</li>
									<li>
										<i class="fa fa-comments-o" aria-hidden="true"></i>
										<?php comments_number( '0', '1', '%' ); ?>
									</li>
								</ul>
							</div>
						</div>
					</li>
				<?php
				endwhile;
			endif;
			echo '</ul>';
			echo $after_widget;
		}


		/********* Starting Featured Recipe Widget Admin From *********/

		public function form( $instance ) {
			$instance = wp_parse_args( (array) $instance, array(
				'title'          => __( 'FEATURED COURSES', 'myvoice' ),
				'post_per_page'  => 2,
				'excerpt_length' => 10,
			) );

			$title         = $instance['title'];
			$courses_count = $instance['post_per_page'];

			?>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Widget Title', 'myvoice' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>"/>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'post_per_page' ); ?>"><?php _e( 'Number of Courses', 'myvoice' ); ?></label>
				<input class="widefat" id="<?php echo $this->get_field_id( 'post_per_page' ); ?>" name="<?php echo $this->get_field_name( 'post_per_page' ); ?>" type="text" value="<?php echo $courses_count; ?>"/>
			</p>
			<?php
		}


		/********* Starting Featured Recipe Widget Update Function *********/

		public function update( $new_instance, $old_instance ) {
			$instance                   = $old_instance;
			$instance['title']          = strip_tags( $new_instance['title'] );
			$instance['post_per_page']  = intval( $new_instance['post_per_page'] );
			$instance['excerpt_length'] = intval( $new_instance['excerpt_length'] );

			return $instance;

		}

	}
	register_widget( 'Featured_Courses_Widget' );
}

?>