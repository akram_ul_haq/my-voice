<?php

if ( ! function_exists( 'is_woocommerce_activated' ) ) {
	/**
	 * Check whether WooCommerce plugin is activated or not
	 * @return bool
	 */
	function is_woocommerce_activated() {
		if ( class_exists( 'WooCommerce' ) ) {
			return true;
		} else {
			return false;
		}
	}
}

if ( ! function_exists( 'inspiry_woo_widgets_init' ) ) {
	/**
	 * Register widgets area.
	 */
	function inspiry_woo_widgets_init() {

		register_sidebar( array(
			'name'          => esc_html__( 'Shop Sidebar', 'myvoice' ),
			'id'            => 'shop',
			'description'   => esc_html__( 'Add shop sidebar widgets here.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget clearfix %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		) );
	}

	add_action( 'widgets_init', 'inspiry_woo_widgets_init', 15 );
}

if ( ! function_exists( 'inspiry_add_to_cart_dropdown' ) ) {
	/**
	 * Add to cart dropdown for Quick View Cart in Header
	 *
	 * @param $fragments
	 *
	 * @return mixed
	 */
	function inspiry_add_to_cart_dropdown( $fragments ) {
		global $woocommerce;
		ob_start();
		?>
		<div class="cart-inner">
			<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="cart-link">
				<div class="cart-icon">
					<i><?php include( get_template_directory() . '/images/icons/cart.svg' ); ?></i>
				</div>
			</a>
			<div class="nav-dropdown">
				<div class="nav-dropdown-inner">
					<?php
						if ( sizeof( $woocommerce->cart->cart_contents ) > 0 ) :
							?>
							<div class="cart_list">
								<?php
									foreach ( $woocommerce->cart->cart_contents as $cart_item_key => $cart_item ) :
										$_product = $cart_item['data'];
										if ( $_product->exists() && $cart_item['quantity'] > 0 ) :
											?>
											<div class="row mini-cart-item">
												<div class="col-sm-2">
													<?php echo apply_filters( 'woocommerce_cart_item_remove_link',
														sprintf( '<a href="%s" class="remove" title="%s"><i class="fa fa-close"></i></a>',
															esc_url( $woocommerce->cart->get_remove_url( $cart_item_key ) ),
															esc_html__( 'Remove this item', 'myvoice' ) ), $cart_item_key );
													?>
												</div>
												<div class="col-sm-7">
													<?php
														$product_title = $_product->get_title();
														echo '<a class="cart_list_product_title" href="' . get_permalink( $cart_item['product_id'] ) . '">' . apply_filters( 'woocommerce_cart_widget_product_title', $product_title, $_product ) . '</a>';
														echo '<div class="cart_list_product_price">' . wc_price( $_product->get_price() ) . ' /</div>';
														echo '<div class="cart_list_product_quantity">' . esc_html__( 'Quantity', 'myvoice' ) . ': ' . $cart_item['quantity'] . '</div>';
													?>
												</div>
												<div class="col-sm-3">
													<?php echo '<a class="cart_list_product_img" href="' . get_permalink( $cart_item['product_id'] ) . '">' . $_product->get_image() . '</a>'; ?>
												</div>
											</div><!-- end row -->
											<?php
										endif;
									endforeach;
								?>
							</div>
							<div class="minicart_total_checkout">
								<?php esc_html_e( 'Cart Subtotal', 'myvoice' ); ?>
								<span><?php echo $woocommerce->cart->get_cart_total(); ?></span>
							</div>
							<a href="<?php echo esc_url( $woocommerce->cart->get_cart_url() ); ?>" class="button-mini-cart"><?php esc_html_e( 'View Cart', 'myvoice' ); ?></a>
							<a href="<?php echo esc_url( $woocommerce->cart->get_checkout_url() ); ?>" class="button-mini-cart"><?php esc_html_e( 'Proceed to Checkout', 'myvoice' ); ?></a>
							<?php
						else:
							echo '<p class="empty-cart-message">' . esc_html__( 'No products in the cart.', 'myvoice' ) . '</p>';
						endif;
					?>
				</div>
			</div>
		</div>
		<?php
		$fragments['.cart-inner'] = ob_get_clean();

		return $fragments;
	}

	add_filter( 'add_to_cart_fragments', 'inspiry_add_to_cart_dropdown' );
}

if ( class_exists( 'woocommerce' ) ) {

	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
	remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
	remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
	remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
	remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );
	remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

	// change priority on single template.
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 10 );
	add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 20 );

	if ( ! function_exists( 'inspiry_shop_products_per_page' ) ) {
		/**
		 * Shop - products per page
		 * @return int
		 */
		function inspiry_shop_products_per_page() {
			$get_products_per_page = get_theme_mod( 'inspiry_shop_products_per_page' );
			if ( ! empty( $get_products_per_page ) ) {
				return intval( $get_products_per_page );
			} else {

				$default_posts_per_page = get_option( 'posts_per_page' );

				return $default_posts_per_page;
			}
		}

		add_filter( 'loop_shop_per_page', 'inspiry_shop_products_per_page' );
	}

}

?>