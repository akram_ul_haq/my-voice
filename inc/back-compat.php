<?php
/**
 * My Voice back compat functionality
 *
 * Prevents My Voice from running on WordPress versions prior to 4.7,
 * since this theme is not meant to be backward compatible beyond that and
 * relies on many newer functions and markup changes introduced in 4.7.
 *
 * @package My Voice
 * @since my voice 1.0
 */

/**
 * Prevent switching to my voice on old versions of WordPress.
 *
 * Switches to the default theme.
 *
 * @since My Voice 1.0
 */
function my_voice_switch_theme() {
	switch_theme( WP_DEFAULT_THEME );
	unset( $_GET['activated'] );
	add_action( 'admin_notices', 'my_voice_upgrade_notice' );
}
add_action( 'after_switch_theme', 'my_voice_switch_theme' );

/**
 * Adds a message for unsuccessful theme switch.
 *
 * Prints an update nag after an unsuccessful attempt to switch to
 * My Voice on WordPress versions prior to 4.7.
 *
 * @since My Voice 1.0
 *
 * @global string $wp_version WordPress version.
 */
function my_voice_upgrade_notice() {
	$message = sprintf( __( 'My Voice requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'myvoice' ), $GLOBALS['wp_version'] );
	printf( '<div class="error"><p>%s</p></div>', $message );
}

/**
 * Prevents the Customizer from being loaded on WordPress versions prior to 4.7.
 *
 * @since My Voice 1.0
 *
 * @global string $wp_version WordPress version.
 */
function my_voice_customize() {
	wp_die( sprintf( __( 'My Voice requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'myvoice' ), $GLOBALS['wp_version'] ), '', array(
		'back_link' => true,
	) );
}
add_action( 'load-customize.php', 'my_voice_customize' );

/**
 * Prevents the Theme Preview from being loaded on WordPress versions prior to 4.7.
 *
 * @since My Voice 1.0
 *
 * @global string $wp_version WordPress version.
 */
function myvoice_preview() {
	if ( isset( $_GET['preview'] ) ) {
		wp_die( sprintf( __( 'My Voice requires at least WordPress version 4.7. You are running version %s. Please upgrade and try again.', 'myvoice' ), $GLOBALS['wp_version'] ) );
	}
}
add_action( 'template_redirect', 'myvoice_preview' );
