<?php
/**
 * Register Meta Boxes
 *
 * @package My Voice
 * @since 1.0.0
 */

add_filter( 'rwmb_meta_boxes', 'my_voice_register_meta_boxes' );

if ( ! function_exists( 'my_voice_register_meta_boxes' ) ) {

	/**
	 * Register meta boxes function.
	 *
	 * @param array $meta_boxes - Array of meta boxes of the theme.
	 *
	 * @return array
	 *
	 * @since 1.0.0
	 */
	function my_voice_register_meta_boxes( $meta_boxes ) {

		$prefix = 'MY_VOICE_';

		// Slider meta box for sliders post.
		$meta_boxes[] = array(
			'id'         => 'slider-meta-box',
			'title'      => esc_html__( 'Slider Details', 'myvoice' ),
			'post_types' => array( 'sliders' ),
			'context'    => 'normal',
			'priority'   => 'high',
			'fields'     => array(
				array(
					'name' => esc_html__( 'Sub Title', 'myvoice' ),
					'desc' => esc_html__( 'Please provide sub title', 'myvoice' ),
					'id'   => "{$prefix}slider_sub_title",
					'type' => 'text',
				),

				array(
					'name'             => esc_html__( 'Slider Background Image', 'myvoice' ),
					'desc'             => esc_html__( 'Please provide Slider Background Image', 'myvoice' ),
					'id'               => "{$prefix}slider_bg_image",
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'max_status'       => false,

				),
				array(
					'name'             => esc_html__( 'Slider Teacher Image', 'myvoice' ),
					'desc'             => esc_html__( 'Please provide Slider Teacher Image', 'myvoice' ),
					'id'               => "{$prefix}slider_teacher_image",
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
					'max_status'       => false,
				),
				array(
					'name' => esc_html__( 'Slider Button Text', 'myvoice' ),
					'desc' => esc_html__( 'Please provide Slider Button Text', 'myvoice' ),
					'id'   => "{$prefix}slider_button_text",
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Slider Button URL', 'myvoice' ),
					'desc' => esc_html__( 'Please provide Slider Button URL', 'myvoice' ),
					'id'   => "{$prefix}slider_button_url",
					'type' => 'text',
				),
			),
		);

		// Video embed code meta box for video post format.
		$meta_boxes[] = array(
			'id'         => 'video-meta-box',
			'title'      => esc_html__( 'Video', 'myvoice' ),
			'post_types' => array( 'post' ),
			'context'    => 'normal',
			'priority'   => 'high',
			'fields'     => array(
				array(
					'name' => esc_html__( 'Video URL', 'myvoice' ),
					'desc' => esc_html__( 'Please provide the video url.', 'myvoice' ),
					'id'   => "{$prefix}embed_url",
					'type' => 'text',
				),
			),
		);

		// Video embed code meta box for video post format.
		$meta_boxes[] = array(
			'id'         => 'video-post-meta-box',
			'title'      => esc_html__( 'Video', 'myvoice' ),
			'post_types' => array( 'video' ),
			'context'    => 'normal',
			'priority'   => 'high',
			'fields'     => array(
				array(
					'name' => esc_html__( 'Video Embed Code', 'myvoice' ),
					'desc' => esc_html__( 'If you are not using self hosted videos then please provide the video embed code and remove the width and height attributes.', 'myvoice' ),
					'id'   => "{$prefix}embed_code",
					'type' => 'textarea',
					'cols' => '20',
					'rows' => '3',
				),
			),
		);

		// Banner Meta Box.
		$meta_boxes[] = array(
			'id'         => 'banner-meta-box',
			'title'      => esc_html__( 'Page Banner', 'myvoice' ),
			'post_types' => array( 'page' ),
			'context'    => 'normal',
			'priority'   => 'low',
			'fields'     => array(
				array(
					'name' => esc_html__( 'Banner Title', 'myvoice' ),
					'id'   => "{$prefix}banner_title",
					'desc' => esc_html__( 'Please provide the Banner Title, Otherwise the Page Title will be displayed in its place.', 'myvoice' ),
					'type' => 'text',
				),
				array(
					'name'             => esc_html__( 'Banner Image', 'myvoice' ),
					'id'               => "{$prefix}page_banner_image",
					'desc'             => esc_html__( 'Please upload the Banner Image. Otherwise the default banner image from theme options will be displayed.', 'myvoice' ),
					'type'             => 'image_advanced',
					'max_file_uploads' => 1,
				),
			),
		);

		// Banner Meta Box.
		$meta_boxes[] = array(
			'id'         => 'contact-page-meta-box',
			'title'      => esc_html__( 'Contact Details', 'myvoice' ),
			'post_types' => array( 'page' ),
			'context'    => 'normal',
			'priority'   => 'high',
			'fields'     => array(
				array(
					'name' => esc_html__( 'Contact From Shortcode', 'myvoice' ),
					'id'   => "{$prefix}contact_form_shortcode",
					'desc' => esc_html__( 'Please contact from 7 shortcode here.', 'myvoice' ),
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'Sidebar Contact Title', 'myvoice' ),
					'id'   => "{$prefix}contact_details_title",
					'desc' => esc_html__( 'Please provide title', 'myvoice' ),
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Details', 'myvoice' ),
					'id'   => "{$prefix}contact_details_detail",
					'desc' => esc_html__( 'Please provide title', 'myvoice' ),
					'type' => 'textarea',
				),
				array(
					'name' => esc_html__( 'Location', 'myvoice' ),
					'id'   => "{$prefix}contact_details_location",
					'desc' => esc_html__( 'Please provide location', 'myvoice' ),
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Phone Number', 'myvoice' ),
					'id'   => "{$prefix}contact_details_number",
					'desc' => esc_html__( 'Please provide phone number', 'myvoice' ),
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Email', 'myvoice' ),
					'id'   => "{$prefix}contact_details_email",
					'desc' => esc_html__( 'Please provide email', 'myvoice' ),
					'type' => 'text',
				),

				array(
					'name' => esc_html__( 'Skype', 'myvoice' ),
					'id'   => "{$prefix}contact_details_skype",
					'desc' => esc_html__( 'Please provide skype', 'myvoice' ),
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Website Address', 'myvoice' ),
					'id'   => "{$prefix}contact_details_website",
					'desc' => esc_html__( 'Please provide website address', 'myvoice' ),
					'type' => 'text',
				),
			),
		);

		// Event Meta Box.
		$meta_boxes[] = array(
			'id'         => 'banner-meta-box',
			'title'      => esc_html__( 'Event Details', 'myvoice' ),
			'post_types' => array( 'events' ),
			'context'    => 'normal',
			'priority'   => 'low',
			'fields'     => array(
				array(
					'name'       => esc_html__( 'Date and Time', 'myvoice' ),
					'id'         => "{$prefix}event_date_time",
					'desc'       => esc_html__( 'Please select date and time on which event will happen.', 'myvoice' ),
					'type'       => 'datetime',
					'js_options' => array(
						'showTimepicker' => true,
						'controlType'    => 'select',
						'showSecond'     => true,
						'oneLine'        => true,
						'timestamp'      => true,
					),
				),
				array(
					'name' => esc_html__( 'Location', 'myvoice' ),
					'id'   => "{$prefix}event_location",
					'desc' => esc_html__( 'Please provide the Location', 'myvoice' ),
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Timing', 'myvoice' ),
					'id'   => "{$prefix}event_timing",
					'desc' => esc_html__( 'Please provide the Timing', 'myvoice' ),
					'type' => 'text',
				),
			),
		);

		// Video embed code meta box for video post format.
		$meta_boxes[] = array(
			'id'         => 'video-meta-box',
			'title'      => esc_html__( 'Course Features', 'myvoice' ),
			'post_types' => array( 'courses' ),
			'context'    => 'normal',
			'priority'   => 'high',
			'fields'     => array(
				array(
					'name'       => esc_html__( 'Course Features Title', 'myvoice' ),
					'desc'       => esc_html__( 'Please provide course features title.', 'myvoice' ),
					'id'         => "{$prefix}course_features_title",
					'type'       => 'text',
					'js_options' => array(
						'appendText'      => __( '(yyyy-mm-dd)', 'myvoice' ),
						'autoSize'        => true,
						'buttonText'      => __( 'Select Date', 'myvoice' ),
						'dateFormat'      => __( 'yy-mm-dd', 'myvoice' ),
						'numberOfMonths'  => 2,
						'showButtonPanel' => true,
					),
				),
				array(
					'name' => esc_html__( 'Starting Date', 'myvoice' ),
					'desc' => esc_html__( 'Please provide starting date.', 'myvoice' ),
					'id'   => "{$prefix}course_starting_date",
					'type' => 'date',
				),
				array(
					'name' => esc_html__( 'Duration', 'myvoice' ),
					'desc' => esc_html__( 'Please provide duration in months.', 'myvoice' ),
					'id'   => "{$prefix}course_duration_months",
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Timing', 'myvoice' ),
					'desc' => esc_html__( 'Please provide course timing.', 'myvoice' ),
					'id'   => "{$prefix}course_timing",
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Institution', 'myvoice' ),
					'desc' => esc_html__( 'Please provide institution Name.', 'myvoice' ),
					'id'   => "{$prefix}course_institution_name",
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Seats Available', 'myvoice' ),
					'desc' => esc_html__( 'Please provide seats available.', 'myvoice' ),
					'id'   => "{$prefix}course_seats_available",
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Level', 'myvoice' ),
					'desc' => esc_html__( 'Please Level.', 'myvoice' ),
					'id'   => "{$prefix}course_level",
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Course Fee', 'myvoice' ),
					'desc' => esc_html__( 'Please provide course fee using currency sign.', 'myvoice' ),
					'id'   => "{$prefix}course_price",
					'type' => 'text',
				),
				array(
					'name' => esc_html__( 'Enroll Now URL', 'myvoice' ),
					'desc' => esc_html__( 'Please provide course Enroll Now URL.', 'myvoice' ),
					'id'   => "{$prefix}course_enroll_url",
					'type' => 'text',
				),
			),
		);
		// Apply a filter before returning meta boxes.
		$meta_boxes = apply_filters( 'myvoice_theme_meta', $meta_boxes );

		return $meta_boxes;

	}
}

