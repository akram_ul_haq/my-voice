<?php


function my_voice_about_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'name'       => '',
		'percentage' => '0',
	), $atts ) );

	return '<!--Skill Item Start-->
			<div class="tnit-skill-item">
				<div class="tnit-progress-bar position" data-percent="' . $percentage . '" data-duration="2000" data-color="#ccc,#f2b604"></div>
				<div class="tnit-text">
					<h4>' . $name . '</h4>
				</div>
			</div>
			<!--Skill Item End-->';
}

add_shortcode( 'skill_level', 'my_voice_about_shortcode' );


function facts( $atts, $content = null ) {

	return '<div class="facts-inner-outer">
	<ul class="tnit-facts-listed">
	' . do_shortcode( $content ) . '
		</ul>
</div>
	';
}

add_shortcode( 'facts', 'facts' );


function facts_list( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'icon'   => '',
		'name'   => '',
		'number' => '',
	), $atts ) );


	return '<li class="facts-item">
			<i class="icomoon ' . $icon . '" aria-hidden="true"></i>
			<div class="tnit-text">
				<strong class="tnit-counter">' . $number . '</strong>
				<span>' . $name . '</span>
			</div>
		</li>';
}

add_shortcode( 'facts_list', 'facts_list' );


function testimonials( $atts, $content = null ) {

	return '<div id="tnit-testimonial-slider" class="owl-carousel">' . do_shortcode( $content ) . '</div>';
}

add_shortcode( 'testimonials', 'testimonials' );


function testimonial_list( $atts, $content = null ) {

	return '<!--Testimonial Item Start--><figure class="tnit-testimonial-item">' . do_shortcode( $content ) . '</figure><!--Testimonial Item End-->';
}

add_shortcode( 'testimonial_list', 'testimonial_list' );


function testimonial_list_details( $atts, $content = null ) {

	return '<figcaption class="tnit-caption"><i class="fa fa-quote-left" aria-hidden="true"></i>' . $content . '</figcaption>';
}

add_shortcode( 'testimonial_list_details', 'testimonial_list_details' );


?>