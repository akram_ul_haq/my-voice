<?php
/**
 * Custom Post Types and Taxonomies
 *
 * @since 1.0.0
 * @package My Voice
 */

if ( ! function_exists( 'myvoice_video_init' ) ) {
	/**
	 * Register a custom post type called "video".
	 *
	 * @see get_post_type_labels() for label keys.
	 */
	function myvoice_video_init() {
		$labels = array(
			'name'                  => _x( 'Videos', 'Post type general name', 'myvoice' ),
			'singular_name'         => _x( 'Video', 'Post type singular name', 'myvoice' ),
			'menu_name'             => _x( 'Videos', 'Admin Menu text', 'myvoice' ),
			'name_admin_bar'        => _x( 'Video', 'Add New on Toolbar', 'myvoice' ),
			'add_new'               => __( 'Add New', 'myvoice' ),
			'add_new_item'          => __( 'Add New Video', 'myvoice' ),
			'new_item'              => __( 'New Video', 'myvoice' ),
			'edit_item'             => __( 'Edit Video', 'myvoice' ),
			'view_item'             => __( 'View Video', 'myvoice' ),
			'all_items'             => __( 'All Videos', 'myvoice' ),
			'search_items'          => __( 'Search Videos', 'myvoice' ),
			'parent_item_colon'     => __( 'Parent Videos:', 'myvoice' ),
			'not_found'             => __( 'No books found.', 'myvoice' ),
			'not_found_in_trash'    => __( 'No books found in Trash.', 'myvoice' ),
			'featured_image'        => _x( 'Video Cover Image', 'Overrides the “Featured Image” phrase for this post type.', 'myvoice' ),
			'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type.', 'myvoice' ),
			'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type.', 'myvoice' ),
			'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type.', 'myvoice' ),
			'archives'              => _x( 'Video archives', 'The post type archive label used in nav menus. Default “Post Archives”.', 'myvoice' ),
			'insert_into_item'      => _x( 'Insert into video', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post).', 'myvoice' ),
			'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post).', 'myvoice' ),
			'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”.', 'myvoice' ),
			'items_list_navigation' => _x( 'Videos list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”.', 'myvoice' ),
			'items_list'            => _x( 'Videos list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”.', 'myvoice' ),
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'myvoice-videos' ),
			'capability_type'    => 'post',
			'menu_icon'          => 'dashicons-format-video',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'taxonomies'         => array( 'post_tag' ),
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'post_tag' ),
		);

		register_post_type( 'video', $args );
	}

	add_action( 'init', 'myvoice_video_init' );
}

if ( ! function_exists( 'myvoice_create_video_taxonomies' ) ) {

	/**
	 * Create two taxonomies, genres and writers for the post type "video".
	 *
	 * @see register_post_type() for registering custom post types.
	 */
	function myvoice_create_video_taxonomies() {
		// Add new taxonomy, make it hierarchical (like categories).
		$labels = array(
			'name'              => _x( 'Video Types', 'taxonomy general name', 'myvoice' ),
			'singular_name'     => _x( 'Video Type', 'taxonomy singular name', 'myvoice' ),
			'search_items'      => __( 'Search Video Types', 'myvoice' ),
			'all_items'         => __( 'All Video Types', 'myvoice' ),
			'parent_item'       => __( 'Parent Video Type', 'myvoice' ),
			'parent_item_colon' => __( 'Parent Video Type:', 'myvoice' ),
			'edit_item'         => __( 'Edit Video Type', 'myvoice' ),
			'update_item'       => __( 'Update Video Type', 'myvoice' ),
			'add_new_item'      => __( 'Add New Video Type', 'myvoice' ),
			'new_item_name'     => __( 'New Video Type Name', 'myvoice' ),
			'menu_name'         => __( 'Video Type', 'myvoice' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'video-type' ),
		);

		register_taxonomy( 'video-type', array( 'video' ), $args );
	}

	// hook into the init action and call create_book_taxonomies when it fires.
	add_action( 'init', 'myvoice_create_video_taxonomies', 0 );
}


/**
 * Register a custom post type called "course".
 *
 * @see get_post_type_labels() for label keys.
 */
function myvoice_course_init() {
	$labels = array(
		'name'                  => _x( 'Courses', 'Post type general name', 'myvoice' ),
		'singular_name'         => _x( 'Course', 'Post type singular name', 'myvoice' ),
		'menu_name'             => _x( 'Courses', 'Admin Menu text', 'myvoice' ),
		'name_admin_bar'        => _x( 'Course', 'Add New on Toolbar', 'myvoice' ),
		'add_new'               => __( 'Add New', 'myvoice' ),
		'add_new_item'          => __( 'Add New Course', 'myvoice' ),
		'new_item'              => __( 'New Course', 'myvoice' ),
		'edit_item'             => __( 'Edit Course', 'myvoice' ),
		'view_item'             => __( 'View Course', 'myvoice' ),
		'all_items'             => __( 'All Courses', 'myvoice' ),
		'search_items'          => __( 'Search Courses', 'myvoice' ),
		'parent_item_colon'     => __( 'Parent Courses:', 'myvoice' ),
		'not_found'             => __( 'No books found.', 'myvoice' ),
		'not_found_in_trash'    => __( 'No books found in Trash.', 'myvoice' ),
		'featured_image'        => _x( 'Course Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'archives'              => _x( 'Course archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'myvoice' ),
		'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'myvoice' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'myvoice' ),
		'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'myvoice' ),
		'items_list_navigation' => _x( 'Courses list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'myvoice' ),
		'items_list'            => _x( 'Courses list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'myvoice' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'myvoice-courses' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'menu_icon'          => 'dashicons-book',
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
	);

	register_post_type( 'courses', $args );
}

add_action( 'init', 'myvoice_course_init' );


/**
 * Register a custom post type called "book".
 *
 * @see get_post_type_labels() for label keys.
 */
function myvoice_event_init() {
	$labels = array(
		'name'                  => _x( 'Events', 'Post type general name', 'myvoice' ),
		'singular_name'         => _x( 'Event', 'Post type singular name', 'myvoice' ),
		'menu_name'             => _x( 'Events', 'Admin Menu text', 'myvoice' ),
		'name_admin_bar'        => _x( 'Event', 'Add New on Toolbar', 'myvoice' ),
		'add_new'               => __( 'Add New', 'myvoice' ),
		'add_new_item'          => __( 'Add New Event', 'myvoice' ),
		'new_item'              => __( 'New Event', 'myvoice' ),
		'edit_item'             => __( 'Edit Event', 'myvoice' ),
		'view_item'             => __( 'View Event', 'myvoice' ),
		'all_items'             => __( 'All Events', 'myvoice' ),
		'search_items'          => __( 'Search Events', 'myvoice' ),
		'parent_item_colon'     => __( 'Parent Events:', 'myvoice' ),
		'not_found'             => __( 'No books found.', 'myvoice' ),
		'not_found_in_trash'    => __( 'No books found in Trash.', 'myvoice' ),
		'featured_image'        => _x( 'Event Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'archives'              => _x( 'Event archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'myvoice' ),
		'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'myvoice' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'myvoice' ),
		'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'myvoice' ),
		'items_list_navigation' => _x( 'Events list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'myvoice' ),
		'items_list'            => _x( 'Events list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'myvoice' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'myvoice-events' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'menu_icon'          => 'dashicons-calendar-alt',
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'comments' ),
	);

	register_post_type( 'events', $args );
}

add_action( 'init', 'myvoice_event_init' );


if ( ! function_exists( 'myvoice_create_event_taxonomies' ) ) {

	/**
	 * Create two taxonomies, genres and writers for the post type "events".
	 *
	 * @see register_post_type() for registering custom post types.
	 */
	function myvoice_create_event_taxonomies() {
		// Add new taxonomy, make it hierarchical (like categories).
		$labels = array(
			'name'              => _x( 'Event Months', 'taxonomy general name', 'myvoice' ),
			'singular_name'     => _x( 'Event Month', 'taxonomy singular name', 'myvoice' ),
			'search_items'      => __( 'Search Event Months', 'myvoice' ),
			'all_items'         => __( 'All Event Months', 'myvoice' ),
			'parent_item'       => __( 'Parent Event Month', 'myvoice' ),
			'parent_item_colon' => __( 'Parent Event Month:', 'myvoice' ),
			'edit_item'         => __( 'Edit Event Month', 'myvoice' ),
			'update_item'       => __( 'Update Event Month', 'myvoice' ),
			'add_new_item'      => __( 'Add New Event Month', 'myvoice' ),
			'new_item_name'     => __( 'New Event Month Name', 'myvoice' ),
			'menu_name'         => __( 'Event Month', 'myvoice' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'event-month' ),
		);

		register_taxonomy( 'event-month', array( 'events' ), $args );
	}

	// hook into the init action and call create_book_taxonomies when it fires.
	add_action( 'init', 'myvoice_create_event_taxonomies', 0 );
}


/**
 * Register a custom post type called "book".
 *
 * @see get_post_type_labels() for label keys.
 */
function my_voice_slider_post() {
	$labels = array(
		'name'                  => _x( 'Sliders', 'Post type general name', 'myvoice' ),
		'singular_name'         => _x( 'Slider', 'Post type singular name', 'myvoice' ),
		'menu_name'             => _x( 'Sliders', 'Admin Menu text', 'myvoice' ),
		'name_admin_bar'        => _x( 'Slider', 'Add New on Toolbar', 'myvoice' ),
		'add_new'               => __( 'Add New', 'myvoice' ),
		'add_new_item'          => __( 'Add New Slider', 'myvoice' ),
		'new_item'              => __( 'New Slider', 'myvoice' ),
		'edit_item'             => __( 'Edit Slider', 'myvoice' ),
		'view_item'             => __( 'View Slider', 'myvoice' ),
		'all_items'             => __( 'All Sliders', 'myvoice' ),
		'search_items'          => __( 'Search Sliders', 'myvoice' ),
		'parent_item_colon'     => __( 'Parent Sliders:', 'myvoice' ),
		'not_found'             => __( 'No books found.', 'myvoice' ),
		'not_found_in_trash'    => __( 'No books found in Trash.', 'myvoice' ),
		'featured_image'        => _x( 'Slider Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'myvoice' ),
		'archives'              => _x( 'Slider archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'myvoice' ),
		'insert_into_item'      => _x( 'Insert into book', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'myvoice' ),
		'uploaded_to_this_item' => _x( 'Uploaded to this book', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'myvoice' ),
		'filter_items_list'     => _x( 'Filter books list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'myvoice' ),
		'items_list_navigation' => _x( 'Sliders list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'myvoice' ),
		'items_list'            => _x( 'Sliders list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'myvoice' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'menu_icon'          => 'dashicons-format-gallery',
		'rewrite'            => array( 'slug' => 'sliders' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title' ),
	);

	register_post_type( 'sliders', $args );
}

add_action( 'init', 'my_voice_slider_post' );