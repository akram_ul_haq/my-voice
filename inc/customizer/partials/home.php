<?php
/**
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_pages_customizer' ) ) :

	/**
	 * Pages Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_customizer( WP_Customize_Manager $wp_customize ) {

		$wp_customize->add_panel( 'my_voice_home_panel', array(
			'title'    => __( 'Home', 'myvoice' ),
			'priority' => 123,
		) );

	}

	add_action( 'customize_register', 'my_voice_home_customizer' );
endif;


/**
 * Blog Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/blog.php';


/**
 * Courses Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/courses.php';


/**
 * Events Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/events.php';


/**
 * Events Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/video.php';


/**
 * Features Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/features.php';

/**
 * Partners Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/partners.php';

/**
 * About Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/about.php';


/**
 * Slider Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home/slider-social.php';
