<?php
/**
 * Customizer settings for Header
 *
 * @package RH
 */

if ( ! function_exists( 'my_voice_header_customizer' ) ) :
	function my_voice_header_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Header Panel
		 */
		$wp_customize->add_panel( 'my_voice_header_panel', array(
			'title'    => __( 'Header', 'myvoice' ),
			'priority' => 80,
		) );

	}

	add_action( 'customize_register', 'my_voice_header_customizer' );
endif;


/**
 * Social Icons
 */

require_once MY_VOICE_CUSTOMIZER . 'partials/header/social-icons.php';

/**
 * Contact Information
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/header/contact-information.php';


/**
 * Others
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/header/others.php';
