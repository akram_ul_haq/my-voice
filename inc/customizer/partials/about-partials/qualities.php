<?php
/**
 * Section: `Qualities`
 * Panel: `About`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_about_qualities_section_customizer' ) ) :

	/**
	 * About section in the About Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_about_qualities_section_customizer( WP_Customize_Manager $wp_customize ) {

		$wp_customize->add_section( 'my_voice_about_qualities_section', [
			'title' => __( 'Qualities Section', 'myvoice' ),
			'panel' => 'my_voice_about_page_panel',
		] );


		$wp_customize->add_setting( 'my_voice_about_qualities_hide', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_about_qualities_hide', array(
			'label'   => __( 'Hide Qualities section from about page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_about_qualities_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_qualities_section_contents', [
			'type' => 'option',
		] );
		$wp_customize->add_control( new WP_Customize_Teeny_Control( $wp_customize, 'my_voice_qualities_section_contents', [
			'label'   => __( 'Contents Info', 'myvoice' ),
			'section' => 'my_voice_about_qualities_section',
		] ) );


		$wp_customize->add_setting( 'my_voice_about_qualities_skills_hide', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_about_qualities_skills_hide', array(
			'label'   => __( 'Hide Skills section from Qualities Section in About page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_about_qualities_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );


		$wp_customize->add_setting( 'my_voice_skills_section', array(
			'type'              => 'option',
			'sanitize_callback' => 'customizer_repeater_sanitize',
		) );
		$wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'my_voice_skills_section', array(
			'label'                                 => __( 'Skills', 'myvoice' ),
			'section'                               => 'my_voice_about_qualities_section',
			'customizer_repeater_image_control'     => false,
			'customizer_repeater_icon_control'      => false,
			'customizer_repeater_title_control'     => true,
			'customizer_repeater_subtitle_control'  => true,
			'customizer_repeater_text_control'      => false,
			'customizer_repeater_link_control'      => false,
			'customizer_repeater_shortcode_control' => false,
			'customizer_repeater_repeater_control'  => false,
		) ) );
	}

	add_action( 'customize_register', 'my_voice_about_qualities_section_customizer' );
endif;


if ( ! function_exists( 'my_voice_about_qualities_section_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_about_qualities_section_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_about_qualities_hide',
			'my_voice_about_qualities_skills_hide',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_about_qualities_section_defaults' );
endif;
