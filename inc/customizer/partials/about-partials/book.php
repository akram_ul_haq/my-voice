<?php
/**
 * Section: `Book`
 * Panel: `About`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_about_book_section_customizer' ) ) :

	/**
	 * About section in the About Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_about_book_section_customizer( WP_Customize_Manager $wp_customize ) {

		$wp_customize->add_section( 'my_voice_about_book_section', [
			'title' => __( 'Book Section', 'myvoice' ),
			'panel' => 'my_voice_about_page_panel',
		] );


		$wp_customize->add_setting( 'my_voice_about_book_hide', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_about_book_hide', array(
			'label'   => __( 'Hide book section from about page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_about_book_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_about_book_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_voice_about_book_image', array(
			'label'       => esc_html__( 'Book Section Image', 'myvoice' ),
			'description' => '',
			'section'     => 'my_voice_about_book_section',
		) ) );

		$wp_customize->add_setting( 'my_voice_about_book_section_contents', [
			'type' => 'option',
		] );
		$wp_customize->add_control( new WP_Customize_Teeny_Control( $wp_customize, 'my_voice_about_book_section_contents', [
			'label'   => __( 'Contents Info', 'myvoice' ),
			'section' => 'my_voice_about_book_section',
		] ) );

		$wp_customize->add_setting( 'my_voice_about_audio_book_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_voice_about_audio_book_image', array(
			'label'       => esc_html__( 'Audio Book Image', 'myvoice' ),
			'description' => '',
			'section'     => 'my_voice_about_book_section',
		) ) );

		$wp_customize->add_setting( 'my_voice_about_audio_book_url', array(
			'type'              => 'option',
			'default'           => '#',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'my_voice_about_audio_book_url', array(
			'label'   => __( 'Audio Book URL', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_about_book_section',
		) );

		$wp_customize->add_setting( 'my_voice_about_download_audio_book_url', array(
			'type'              => 'option',
			'default'           => '#',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'my_voice_about_download_audio_book_url', array(
			'label'   => __( 'Download Audio Book URL', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_about_book_section',
		) );
	}

	add_action( 'customize_register', 'my_voice_about_book_section_customizer' );
endif;


if ( ! function_exists( 'my_voice_about_book_section_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_about_book_section_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_about_book_hide',
			'my_voice_about_audio_book_url',
			'my_voice_about_download_audio_book_url',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_about_book_section_defaults' );
endif;
