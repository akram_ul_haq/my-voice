<?php
/**
 * Section: `Certificate`
 * Panel: `About`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_about_certificate_section_customizer' ) ) :

	/**
	 * Certificate section in the About Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_about_certificate_section_customizer( WP_Customize_Manager $wp_customize ) {

		$wp_customize->add_section( 'my_voice_about_certificate_section', [
			'title' => __( 'Certificate Section', 'myvoice' ),
			'panel' => 'my_voice_about_page_panel',
		] );


		$wp_customize->add_setting( 'my_voice_about_certificate_hide', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_about_certificate_hide', array(
			'label'   => __( 'Hide Certificate section from about page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_about_certificate_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_certificate_section_title', array(
			'type'              => 'option',
			'default'           => 'MY CERTIFICATIONS',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_certificate_section_title', array(
			'label'   => __( 'Title for Certificate about page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_about_certificate_section',
		) );



		$wp_customize->add_setting( 'my_voice_certificate_section', array(
			'type'              => 'option',
			'sanitize_callback' => 'customizer_repeater_sanitize',
		) );
		$wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'my_voice_certificate_section', array(
			'label'                                 => __( 'Certificate', 'myvoice' ),
			'section'                               => 'my_voice_about_certificate_section',
			'customizer_repeater_image_control'     => true,
			'customizer_repeater_icon_control'      => true,
			'customizer_repeater_title_control'     => false,
			'customizer_repeater_subtitle_control'  => false,
			'customizer_repeater_text_control'      => false,
			'customizer_repeater_link_control'      => false,
			'customizer_repeater_shortcode_control' => false,
			'customizer_repeater_repeater_control'  => false,
		) ) );
	}

	add_action( 'customize_register', 'my_voice_about_certificate_section_customizer' );
endif;


if ( ! function_exists( 'my_voice_about_certificate_section_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_about_certificate_section_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_about_certificate_hide',
			'my_voice_certificate_section_title',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_about_certificate_section_defaults' );
endif;
