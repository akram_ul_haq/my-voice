<?php
/**
 * Section: `Events`
 * Panel: `Pages`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_event_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_event_customizer( WP_Customize_Manager $wp_customize ) {
		$categories_array = array();

		$categories = get_terms( 'event-month', 'orderby=name' );
		foreach ( $categories as $category ) {
			$categories_array[ $category->term_id ] = $category->name;
		}
		$beginner_item    = array( ' ' => __( 'Select Category', 'myvoice' ) );
		$categories_array = array_merge( $beginner_item, $categories_array );
		/**
		 * Video Section
		 */
		$wp_customize->add_section( 'my_voice_event_section', array(
			'title' => esc_html__( 'Events', 'myvoice' ),
			'panel' => 'my_voice_pages_panel',
		) );

		$wp_customize->add_setting( 'my_voice_related_event_posts', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_related_event_posts', array(
			'label'   => __( 'Hide Related event from event single page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_event_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_related_event_title', array(
			'type'              => 'option',
			'default'           => 'Related Events',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_related_event_title', array(
			'label'   => __( 'Title for related event on event single page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_event_section',
		) );


		$wp_customize->add_setting( 'my_voice_related_event_category', array(
			'type'              => 'option',
			'sanitize_callback' => 'customizer_sanitize_select',
		) );
		$wp_customize->add_control( 'my_voice_related_event_category', array(
				'label'       => esc_html__( 'Related Events Category', 'myvoice' ),
				'type'        => 'select',
				'description' => esc_html__( 'Select category for related event. Default related events will be shown if you will not any category.', 'myvoice' ),
				'section'     => 'my_voice_event_section',
				'choices'     => $categories_array,
			)
		);
	}

	add_action( 'customize_register', 'my_voice_event_customizer' );
endif;


if ( ! function_exists( 'my_voice_event_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  2.6.3
	 */
	function my_voice_event_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_related_event_posts',
			'my_voice_related_event_title',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_event_defaults' );
endif;
