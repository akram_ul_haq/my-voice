<?php
/**
 * Section: `Blog`
 * Panel: `Pages`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_blog_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function  my_voice_blog_customizer( WP_Customize_Manager $wp_customize ) {
		$categories_array = array();

		$categories = get_categories( array(
			'orderby' => 'name',
			'parent'  => 0,
		) );
		foreach ( $categories as $category ) {
			$categories_array[ $category->term_id ] = $category->name;
		}
		$beginner_item    = array( ' ' => __( 'Select Category', 'myvoice' ) );
		$categories_array = array_merge( $beginner_item, $categories_array );
		/**
		 * Blog Section
		 */
		$wp_customize->add_section( 'my_voice_blog_section', array(
			'title' => esc_html__( 'Blog', 'myvoice' ),
			'panel' => 'my_voice_pages_panel',
		) );
		$wp_customize->add_setting( 'my_voice_related_blog_posts', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_related_blog_posts', array(
			'label'   => __( 'Hide Related posts from blog single page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_blog_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_related_blog_title', array(
			'type'              => 'option',
			'default'           => 'RELATED BLOGS',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_related_blog_title', array(
			'label'   => __( 'Title for related posts on blog single page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_blog_section',
		) );


		$wp_customize->add_setting( 'my_voice_related_blog_category', array(
			'type'              => 'option',
			'sanitize_callback' => 'customizer_sanitize_select',
		) );
		$wp_customize->add_control( 'my_voice_related_blog_category', array(
				'label'       => esc_html__( 'Related Blog Category', 'myvoice' ),
				'type'        => 'select',
				'description' => esc_html__( 'Select category for related posts. Default related post will be shown if you will not any category.', 'myvoice' ),
				'section'     => 'my_voice_blog_section',
				'choices'     => $categories_array,
			)
		);
	}

	add_action( 'customize_register', 'my_voice_blog_customizer' );
endif;


if ( ! function_exists( 'my_voice_blog_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  2.6.3
	 */
	function  my_voice_blog_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_related_blog_posts',
			'my_voice_related_blog_title'
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_blog_defaults' );
endif;
