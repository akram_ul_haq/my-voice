<?php
/**
 * Section: `Blog`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_courses_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_courses_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Blog Section
		 */
		$wp_customize->add_section( 'my_voice_home_courses_section', array(
			'title'    => esc_html__( 'Courses', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 122,
		) );
		$wp_customize->add_setting( 'my_voice_home_courses_posts', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_courses_posts', array(
			'label'   => __( 'Hide course posts from home page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_courses_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_home_courses_title', array(
			'type'              => 'option',
			'default'           => 'LEARNING CLASSES',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_home_courses_title', array(
			'label'   => __( 'Title for home posts on courses single page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_courses_section',
		) );

		/*$wp_customize->add_setting( 'my_voice_home_courses_watermark_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_voice_home_courses_watermark_image', array(
			'label'       => esc_html__( 'Watermark Image', 'myvoice' ),
			'description' => '',
			'section'     => 'my_voice_home_courses_section',
		) ) );*/
	}

	add_action( 'customize_register', 'my_voice_home_courses_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_courses_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_courses_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_courses_posts',
			'my_voice_home_courses_title',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_courses_defaults' );
endif;
