<?php
/**
 * Section: `About`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_about_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_about_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * About Section
		 */
		$wp_customize->add_section( 'my_voice_home_about_section', array(
			'title'    => esc_html__( 'About', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 118,
		) );
		$wp_customize->add_setting( 'my_voice_home_about', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_about', array(
			'label'   => __( 'Hide about section from home page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_about_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_home_about_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_voice_home_about_image', array(
			'label'       => esc_html__( 'About Image', 'myvoice' ),
			'description' => '',
			'section'     => 'my_voice_home_about_section',
		) ) );

		$wp_customize->add_setting( 'my_voice_home_about_image_url', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'my_voice_home_about_image_url', array(
			'label'   => __( 'About Image Video URL.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_about_section',
		) );

		$wp_customize->add_setting( 'my_voice_home_about_title', array(
			'type'              => 'option',
			'default'           => 'BEFORE YOU PROCEED KNOW WHO AM I ?',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_home_about_title', array(
			'label'   => __( 'Title for about on home page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_about_section',
		) );


		$wp_customize->add_setting( 'my_voice_home_about_textarea', array(
			'type' => 'option',
		) );
		$wp_customize->add_control( 'my_voice_home_about_textarea', array(
			'label'   => __( 'Details for about on home page.', 'myvoice' ),
			'type'    => 'textarea',
			'section' => 'my_voice_home_about_section',
		) );


		$wp_customize->add_setting( 'my_voice_home_about_button_title', array(
			'type'              => 'option',
			'default'           => 'Read More',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_home_about_button_title', array(
			'label'   => __( 'Button title for about page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_about_section',
		) );

		$wp_customize->add_setting( 'my_voice_home_about_pages', array(
			'type' => 'option',
		) );
		$wp_customize->add_control( 'my_voice_home_about_pages', array(
			'label'       => esc_html__( 'About Page', 'myvoice' ),
			'type'        => 'dropdown-pages',
			'description' => esc_html__( 'Select about page template', 'myvoice' ),
			'section'     => 'my_voice_home_about_section',
		) );

		/*$wp_customize->add_setting( 'my_voice_home_about_watermark_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_voice_home_about_watermark_image', array(
			'label'       => esc_html__( 'Watermark Image', 'myvoice' ),
			'description' => '',
			'section'     => 'my_voice_home_about_section',
		) ) );*/
	}

	add_action( 'customize_register', 'my_voice_home_about_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_about_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_about_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_about',
			'my_voice_home_about_title',
			'my_voice_home_about_button_title',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_about_defaults' );
endif;
