<?php
/**
 * Section: `Partners`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_partners_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_partners_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Partners Section
		 */
		$wp_customize->add_section( 'my_voice_home_partners_section', array(
			'title'    => esc_html__( 'Partners', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 125,
		) );
		$wp_customize->add_setting( 'my_voice_home_partners', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_partners', array(
			'label'   => __( 'Hide partners from home page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_partners_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_home_partners_details', array(
			'sanitize_callback' => 'customizer_repeater_sanitize',
		) );
		$wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'my_voice_home_partners_details', array(
			'label'                                 => __( 'Partners', 'myvoice' ),
			'section'                               => 'my_voice_home_partners_section',
			'customizer_repeater_image_control'     => true,
			'customizer_repeater_icon_control'      => true,
			'customizer_repeater_title_control'     => false,
			'customizer_repeater_subtitle_control'  => false,
			'customizer_repeater_text_control'      => false,
			'customizer_repeater_link_control'      => true,
			'customizer_repeater_shortcode_control' => false,
			'customizer_repeater_repeater_control'  => false,
		) ) );
	}

	add_action( 'customize_register', 'my_voice_home_partners_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_partners_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_partners_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_partners',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_partners_defaults' );
endif;