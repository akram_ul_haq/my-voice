<?php
/**
 * Section: `Events`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_events_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_events_customizer( WP_Customize_Manager $wp_customize ) {
		$categories_array = array();

		$categories = get_terms( 'event-month', array(
			'hide_empty' => false,
		) );
		foreach ( $categories as $category ) {
			$categories_array[ $category->slug ] = $category->name;
		}
		$beginner_item    = array( ' ' => __( 'Select Category', 'myvoice' ) );
		$categories_array = array_merge( $beginner_item, $categories_array );
		/**
		 * Blog Section
		 */
		$wp_customize->add_section( 'my_voice_home_events_section', array(
			'title'    => esc_html__( 'Events', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 124,
		) );
		$wp_customize->add_setting( 'my_voice_home_events_posts', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_events_posts', array(
			'label'   => __( 'Hide event posts from home page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_events_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_home_events_title', array(
			'type'              => 'option',
			'default'           => 'COMING UP NEXT',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_home_events_title', array(
			'label'   => __( 'Title for home posts on events single page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_events_section',
		) );


		$wp_customize->add_setting( 'my_voice_home_events_category', array(
			'type'              => 'option',
			'sanitize_callback' => 'customizer_sanitize_select',
		) );
		$wp_customize->add_control( 'my_voice_home_events_category', array(
			'label'       => esc_html__( 'Home Events Category', 'myvoice' ),
			'type'        => 'select',
			'description' => esc_html__( 'Select category for home events.', 'myvoice' ),
			'section'     => 'my_voice_home_events_section',
			'choices'     => $categories_array,
		) );

		$wp_customize->add_setting( 'my_voice_home_events_button_title', array(
			'type'              => 'option',
			'default'           => 'See All Events',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_home_events_button_title', array(
			'label'   => __( 'Button title for events page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_events_section',
		) );

		$wp_customize->add_setting( 'my_voice_home_events_pages', array(
			'type' => 'option',
		) );
		$wp_customize->add_control( 'my_voice_home_events_pages', array(
			'label'       => esc_html__( 'Events Page', 'myvoice' ),
			'type'        => 'dropdown-pages',
			'description' => esc_html__( 'Select event page template', 'myvoice' ),
			'section'     => 'my_voice_home_events_section',
		) );
	}

	add_action( 'customize_register', 'my_voice_home_events_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_events_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_events_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_events_posts',
			'my_voice_home_events_title',
			'my_voice_home_events_button_title',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_events_defaults' );
endif;
