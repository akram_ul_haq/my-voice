<?php
/**
 * Section: `Video`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_videos_customizer' ) ) :

	/**
	 * Video section in the Home Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_videos_customizer( WP_Customize_Manager $wp_customize ) {
		$categories_array = array();

		$categories = get_terms( 'video-type', array(
			'hide_empty' => false,
		) );
		foreach ( $categories as $category ) {
			$categories_array[ $category->slug ] = $category->name;
		}
		$beginner_item    = array( ' ' => __( 'Select Category', 'myvoice' ) );
		$categories_array = array_merge( $beginner_item, $categories_array );
		/**
		 * Blog Section
		 */
		$wp_customize->add_section( 'my_voice_home_videos_section', array(
			'title'    => esc_html__( 'Video', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 120,
		) );
		$wp_customize->add_setting( 'my_voice_home_videos_posts', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_videos_posts', array(
			'label'   => __( 'Hide video posts from home page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_videos_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_home_videos_title', array(
			'type'              => 'option',
			'default'           => 'FEATURED LECTURES',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_home_videos_title', array(
			'label'   => __( 'Title for home posts on videos single page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_videos_section',
		) );


		$wp_customize->add_setting( 'my_voice_home_videos_category', array(
			'type'              => 'option',
			'sanitize_callback' => 'customizer_sanitize_select',
		) );
		$wp_customize->add_control( 'my_voice_home_videos_category', array(
			'label'       => esc_html__( 'Home Blog Category', 'myvoice' ),
			'type'        => 'select',
			'description' => esc_html__( 'Select category for home posts. Default home post will be shown if you will not any category.', 'myvoice' ),
			'section'     => 'my_voice_home_videos_section',
			'choices'     => $categories_array,
		) );

		/*$wp_customize->add_setting( 'my_voice_home_videos_watermark_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_voice_home_videos_watermark_image', array(
			'label'       => esc_html__( 'Watermark Image', 'myvoice' ),
			'description' => '',
			'section'     => 'my_voice_home_videos_section',
		) ) );*/

	}

	add_action( 'customize_register', 'my_voice_home_videos_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_videos_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_videos_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_videos_posts',
			'my_voice_home_videos_title',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_videos_defaults' );
endif;
