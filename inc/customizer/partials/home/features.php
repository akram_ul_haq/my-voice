<?php
/**
 * Section: `Facts`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_features_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_features_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Blog Section
		 */
		$wp_customize->add_section( 'my_voice_home_features_section', array(
			'title'    => esc_html__( 'Features', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 119,
		) );
		$wp_customize->add_setting( 'my_voice_home_features', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_features', array(
			'label'   => __( 'Hide Home posts from home page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_features_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_home_features_title', array(
			'type'              => 'option',
			'default'           => 'WHAT DO YOU WANT',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'my_voice_home_features_title', array(
			'label'   => __( 'Title for features on features single page.', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_home_features_section',
		) );


		$wp_customize->add_setting( 'my_voice_home_features_details', array(
			'sanitize_callback' => 'customizer_repeater_sanitize',
		) );
		$wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'my_voice_home_features_details', array(
			'label'                                 => __( 'Features', 'myvoice' ),
			'description'                           => __( 'For icons, please go <a href="https://icomoon.io/app/">HERE</a> Select icon and then generate font from bottom right and get code.', 'myvoice' ),
			'section'                               => 'my_voice_home_features_section',
			'customizer_repeater_image_control'     => false,
			'customizer_repeater_icon_control'      => false,
			'customizer_repeater_title_control'     => true,
			'customizer_repeater_subtitle_control'  => true,
			'customizer_repeater_text_control'      => true,
			'customizer_repeater_link_control'      => false,
			'customizer_repeater_shortcode_control' => false,
			'customizer_repeater_repeater_control'  => false,
		) ) );

		/*$wp_customize->add_setting( 'my_voice_home_features_watermark_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'my_voice_home_features_watermark_image', array(
			'label'       => esc_html__( 'Watermark Image', 'myvoice' ),
			'description' => '',
			'section'     => 'my_voice_home_features_section',
		) ) );*/
	}

	add_action( 'customize_register', 'my_voice_home_features_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_features_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_features_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_features',
			'my_voice_home_features_title',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_features_defaults' );
endif;

/**
 * Filter to modify input label in repeater control
 * You can filter by control id and input name.
 *
 * @param string $string Input label.
 * @param string $id Input id.
 * @param string $control Control name.
 *
 * @modified 1.1.41
 *
 * @return string
 */
function features_repeater_labels( $string, $id, $control ) {
	if ( 'my_voice_home_features_details' === $id ) {
		if ( 'customizer_repeater_subtitle_control' === $control ) {
			return esc_html__( 'icon name', 'myvoice' );
		}
	}

	return $string;
}

add_filter( 'repeater_input_labels_filter', 'features_repeater_labels', 10, 3 );
