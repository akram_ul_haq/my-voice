<?php
/**
 * Section: `Facts`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_facts_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_facts_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Blog Section
		 */
		$wp_customize->add_section( 'my_voice_home_facts_section', array(
			'title'    => esc_html__( 'Facts', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 121,
		) );
		$wp_customize->add_setting( 'my_voice_home_facts', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_facts', array(
			'label'   => __( 'Hide facts from home page.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_facts_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_home_fact_details', array(
			'type' => 'option',
		) );
		$wp_customize->add_control( 'my_voice_home_fact_details', array(
			'label'       => __( 'Facts', 'myvoice' ),
			'description' => __( 'Only shortcodes work here.', 'myvoice' ),
			'type'        => 'textarea',
			'section'     => 'my_voice_home_facts_section',
		) );
	}

	add_action( 'customize_register', 'my_voice_home_facts_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_facts_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_facts_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_facts',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_facts_defaults' );
endif;
