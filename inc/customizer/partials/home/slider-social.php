<?php
/**
 * Section: `Slider Social`
 * Panel: `Home`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_home_slider_social_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_home_slider_social_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Blog Section
		 */
		$wp_customize->add_section( 'my_voice_home_slider_social_section', array(
			'title'    => esc_html__( 'Slider Social', 'myvoice' ),
			'panel'    => 'my_voice_home_panel',
			'priority' => 117,
		) );
		$wp_customize->add_setting( 'my_voice_home_social_list', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_home_social_list', array(
			'label'   => __( 'Hide social from home page slider.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_home_slider_social_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );


		$wp_customize->add_setting( 'my_voice_home_slider_social', array(
			'sanitize_callback' => 'customizer_repeater_sanitize',
		) );
		$wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'my_voice_home_slider_social', array(
			'label'                                 => __( 'Slider Social', 'myvoice' ),
			'section'                               => 'my_voice_home_slider_social_section',
			'customizer_repeater_image_control'     => false,
			'customizer_repeater_icon_control'      => true,
			'customizer_repeater_title_control'     => true,
			'customizer_repeater_subtitle_control'  => false,
			'customizer_repeater_text_control'      => false,
			'customizer_repeater_link_control'      => false,
			'customizer_repeater_shortcode_control' => true,
			'customizer_repeater_repeater_control'  => false,
		) ) );
	}

	add_action( 'customize_register', 'my_voice_home_slider_social_customizer' );
endif;


if ( ! function_exists( 'my_voice_home_slider_social_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since 1.0
	 */
	function my_voice_home_slider_social_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_home_social_list',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_home_slider_social_defaults' );
endif;
