<?php
/**
 * Customizer settings for general
 *
 * @package RH
 */

if ( ! function_exists( 'my_voice_general_customizer' ) ) :
	function my_voice_general_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * General Panel.
		 */
		$wp_customize->add_panel( 'my_voice_general_panel', array(
			'title'    => __( 'General', 'myvoice' ),
			'priority' => 79,
		) );

	}

	add_action( 'customize_register', 'my_voice_general_customizer' );
endif;


/**
 * Social Icons
 */

require_once MY_VOICE_CUSTOMIZER . 'partials/header/social-icons.php';



if ( ! function_exists( 'my_voice_general_facts_section_customizer' ) ) :

	/**
	 * Facts section in the General Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_general_facts_section_customizer( WP_Customize_Manager $wp_customize ) {

		$wp_customize->add_section( 'my_voice_general_facts_section', [
			'title' => __( 'Facts Section', 'myvoice' ),
			'panel' => 'my_voice_general_panel',
		] );


		$wp_customize->add_setting( 'my_voice_general_facts_hide', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'my_voice_general_facts_hide', array(
			'label'   => __( 'Hide facts from all places.', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_general_facts_section',
			'choices' => array(
				'false' => esc_html__( 'Yes', 'myvoice' ),
				'true'  => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'my_voice_general_facts', array(
			'type'              => 'option',
			'sanitize_callback' => 'customizer_repeater_sanitize',
		) );
		$wp_customize->add_control( new Customizer_Repeater( $wp_customize, 'my_voice_general_facts', array(
			'label'                                 => __( 'Facts', 'myvoice' ),
			'section'                               => 'my_voice_general_facts_section',
			'customizer_repeater_image_control'     => false,
			'customizer_repeater_icon_control'      => true,
			'customizer_repeater_title_control'     => true,
			'customizer_repeater_subtitle_control'  => true,
			'customizer_repeater_text_control'      => false,
			'customizer_repeater_link_control'      => false,
			'customizer_repeater_shortcode_control' => false,
			'customizer_repeater_repeater_control'  => false,
		) ) );
	}

	add_action( 'customize_register', 'my_voice_general_facts_section_customizer' );
endif;


if ( ! function_exists( 'my_voice_general_facts_section_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_general_facts_section_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'my_voice_general_facts_hide',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_general_facts_section_defaults' );
endif;