<?php
/**
 * Panel: `About`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_about_page_customizer' ) ) :

	/**
	 * Pages Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_about_page_customizer( WP_Customize_Manager $wp_customize ) {

		$wp_customize->add_panel( 'my_voice_about_page_panel', array(
			'title'    => __( 'About', 'myvoice' ),
			'priority' => 124,
		) );

	}

	add_action( 'customize_register', 'my_voice_about_page_customizer' );
endif;

/**
 * About Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/about-partials/about.php';


/**
 * Qualities Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/about-partials/qualities.php';

/**
 * Book Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/about-partials/book.php';

/**
 * Testimonials Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/about-partials/testimonials.php';

/**
 * Certificate Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/about-partials/certificate.php';
