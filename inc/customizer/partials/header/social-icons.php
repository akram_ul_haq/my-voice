<?php
/**
 * Section: Social Icons
 * Panel: Header
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_social_icons_customizer' ) ) :

	/**
	 * A my_voice_social_icons_customizer.
	 *
	 * @param $wp_customize
	 * @since 1.0
	 */

	function my_voice_social_icons_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Social Icons Section
		 */
		$wp_customize->add_section( 'my_voice_header_social_icons', array(
			'title' => __( 'Social Icons', 'myvoice' ),
			'panel' => 'my_voice_general_panel',
		) );

		/* Enable/Disable Social Icons */
		$wp_customize->add_setting( 'theme_show_social_menu', array(
			'type'    => 'option',
			'default' => 'true',
		) );
		$wp_customize->add_control( 'theme_show_social_menu', array(
			'label'   => __( 'Show or Hide Social Icons', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_header_social_icons', // Required, core or custom.
			'choices' => array(
				'true'  => __( 'Show', 'myvoice' ),
				'false' => __( 'Hide', 'myvoice' ),
			),
		) );

		/* Facebook URL */
		$wp_customize->add_setting( 'theme_facebook_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_facebook_link', array(
			'label'   => __( 'Facebook URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

		/* Twitter URL */
		$wp_customize->add_setting( 'theme_twitter_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_twitter_link', array(
			'label'   => __( 'Twitter URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

		/* LinkedIn URL */
		$wp_customize->add_setting( 'theme_linkedin_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_linkedin_link', array(
			'label'   => __( 'LinkedIn URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

		/* Google Plus URL */
		$wp_customize->add_setting( 'theme_google_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_google_link', array(
			'label'   => __( 'Google Plus URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

		/* Instagram URL */
		$wp_customize->add_setting( 'theme_instagram_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_instagram_link', array(
			'label'   => __( 'Instagram URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

		/* YouTube URL */
		$wp_customize->add_setting( 'theme_youtube_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_youtube_link', array(
			'label'   => __( 'YouTube URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

		/* Skype Username */
		$wp_customize->add_setting( 'theme_skype_username', array(
			'type'      => 'option',
			'transport' => 'postMessage',
		) );
		$wp_customize->add_control( 'theme_skype_username', array(
			'label'   => __( 'Skype Username', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_header_social_icons',
		) );

		/* Pinterest URL */
		$wp_customize->add_setting( 'theme_pinterest_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_pinterest_link', array(
			'label'   => __( 'Pinterest URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

		/* RSS URL */
		$wp_customize->add_setting( 'theme_rss_link', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control( 'theme_rss_link', array(
			'label'   => __( 'RSS URL', 'myvoice' ),
			'type'    => 'url',
			'section' => 'my_voice_header_social_icons',
		) );

	}

	add_action( 'customize_register', 'my_voice_social_icons_customizer' );
endif;

if ( ! function_exists( 'my_voice_social_icons_defaults' ) ) :

	/**
	 * my_voice_social_icons_defaults.
	 *
	 * @since  1.0
	 */
	function my_voice_social_icons_defaults( WP_Customize_Manager $wp_customize ) {
		$social_icons_settings_ids = array(
			'theme_show_social_menu'
		);
		my_voice_initialize_defaults( $wp_customize, $social_icons_settings_ids );
	}
	add_action( 'customize_save_after', 'my_voice_social_icons_defaults' );
endif;