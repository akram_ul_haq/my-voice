<?php
/**
 * Section: Contact Information
 * Panel: Header
 *
 * @package My Voice
 *
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_contact_information_customizer' ) ) :

	/**
	 * A my_voice_contact_information_customizer.
	 *
	 * @param  WP_Customize_Manager $wp_customize
	 *
	 * @since  1.0
	 */

	function my_voice_contact_information_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Header Contact Information Section
		 */
		$wp_customize->add_section( 'my_voice_header_contact_info', array(
			'title' => __( 'Contact Information', 'myvoice' ),
			'panel' => 'my_voice_header_panel',
		) );

		/* Header Email */
		$wp_customize->add_setting( 'theme_header_email', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'sanitize_email',
		) );
		$wp_customize->add_control( 'theme_header_email', array(
			'label'   => __( 'Email Address', 'myvoice' ),
			'type'    => 'email',
			'section' => 'my_voice_header_contact_info',
		) );

		/* Header Phone Number */
		$wp_customize->add_setting( 'theme_header_phone', array(
			'type'              => 'option',
			'transport'         => 'postMessage',
			'sanitize_callback' => 'sanitize_text_field',
		) );
		$wp_customize->add_control( 'theme_header_phone', array(
			'label'   => __( 'Phone Number', 'myvoice' ),
			'type'    => 'tel',
			'section' => 'my_voice_header_contact_info',
		) );

	}

	add_action( 'customize_register', 'my_voice_contact_information_customizer' );
endif;