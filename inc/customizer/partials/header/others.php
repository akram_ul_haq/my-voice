<?php
/**
 * Section: Others
 * Panel: Header
 *
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_header_others_customizer' ) ) :

	/**
	 * A my_voice_header_others_customizer.
	 *
	 * @param  WP_Customize_Manager $wp_customize
	 *
	 * @since 1.0
	 */
	function my_voice_header_others_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Header Others
		 */
		$wp_customize->add_section( 'my_voice_header_others', array(
			'title' => __( 'Others', 'myvoice' ),
			'panel' => 'my_voice_header_panel',
		) );

		/* Sticky Header */
		$wp_customize->add_setting( 'my_voice_login_text', array(
			'type'    => 'option',
			'default' => '',
		) );
		$wp_customize->add_control( 'my_voice_login_text', array(
			'label'   => __( 'Login or Register Modal Title', 'myvoice' ),
			'type'    => 'text',
			'section' => 'my_voice_header_others',
		) );

	}

	add_action( 'customize_register', 'my_voice_header_others_customizer' );
endif;
