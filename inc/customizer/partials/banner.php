<?php
/**
 * Section: `Banner`
 * Panel: `Header`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_banner_customizer' ) ) :

	/**
	 * Banner section in the Header Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_banner_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Banner Section
		 */
		$wp_customize->add_section( 'my_voice_banner_section', array(
			'title'    => esc_html__( 'Banner', 'myvoice' ),
			'priority' => 90,
		) );

		$wp_customize->add_setting( 'theme_general_banner_image', array(
			'type'              => 'option',
			'sanitize_callback' => 'esc_url_raw',
		) );
		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'theme_general_banner_image',
				array(
					'label'       => esc_html__( 'Header Banner Image', 'myvoice' ),
					'description' => __( 'Required minimum height is 230px and minimum width is 2000px.', 'myvoice' ),
					'section'     => 'my_voice_banner_section',
				)
			)
		);

		$wp_customize->add_setting( 'theme_banner_titles', array(
			'type'    => 'option',
			'default' => 'false',
		) );
		$wp_customize->add_control( 'theme_banner_titles', array(
			'label'   => __( 'Hide Title From Image Banner', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_banner_section',
			'choices' => array(
				'true'  => esc_html__( 'Yes', 'myvoice' ),
				'false' => esc_html__( 'No', 'myvoice' ),
			),
		) );

		$wp_customize->add_setting( 'theme_banner_breadcrumbs', array(
			'type'    => 'option',
			'default' => 'false',
		) );
		$wp_customize->add_control( 'theme_banner_breadcrumbs', array(
			'label'   => __( 'Hide Breadcrumbs From Image Banner', 'myvoice' ),
			'type'    => 'radio',
			'section' => 'my_voice_banner_section',
			'choices' => array(
				'true'  => esc_html__( 'Yes', 'myvoice' ),
				'false' => esc_html__( 'No', 'myvoice' ),
			),
		) );

	}

	add_action( 'customize_register', 'my_voice_banner_customizer' );
endif;


if ( ! function_exists( 'my_voice_banner_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  2.6.3
	 */
	function my_voice_banner_defaults( WP_Customize_Manager $wp_customize ) {
		$banner_settings_ids = array(
			'theme_banner_titles',
			'theme_banner_breadcrumbs',
		);
		my_voice_initialize_defaults( $wp_customize, $banner_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_banner_defaults' );
endif;
