<?php
/**
 * Panel: `Pages`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( ' my_voice_pages_customizer' ) ) :

	/**
	 * Pages Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_pages_customizer( WP_Customize_Manager $wp_customize ) {

		$wp_customize->add_panel( 'my_voice_pages_panel', array(
			'title'    => __( 'Pages', 'myvoice' ),
			'priority' => 125,
		) );

	}

	add_action( 'customize_register', 'my_voice_pages_customizer' );
endif;

/**
 * Blog Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/pages/blog.php';


/**
 * Video Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/pages/video.php';

/**
 * Event Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/pages/events.php';