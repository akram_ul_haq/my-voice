<?php
/**
 * Section: `Footer`
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_footer_customizer' ) ) :

	/**
	 * Footer section in the Footer Panel.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  1.0
	 */
	function my_voice_footer_customizer( WP_Customize_Manager $wp_customize ) {

		/**
		 * Footer Section
		 */
		$wp_customize->add_section( 'my_voice_footer_section', array(
			'title'    => esc_html__( 'Footer', 'myvoice' ),
			'priority' => 126,
		) );

		$wp_customize->add_setting( 'theme_footer_copyright', array(
			'type'    => 'option',
			'default' => '',
		) );
		$wp_customize->add_control( 'theme_footer_copyright', array(
			'label'   => __( 'Site Copyright', 'myvoice' ),
			'type'    => 'textarea',
			'section' => 'my_voice_footer_section',
		) );
	}

	add_action( 'customize_register', 'my_voice_footer_customizer' );
endif;


if ( ! function_exists( 'my_voice_footer_defaults' ) ) :

	/**
	 * Default settings.
	 *
	 * @param object $wp_customize - Instance of WP_Customize_Manager.
	 *
	 * @since  2.6.3
	 */
	function my_voice_footer_defaults( WP_Customize_Manager $wp_customize ) {
		$footer_settings_ids = array(
			'theme_footer_titles',
			'theme_footer_breadcrumbs',
		);
		my_voice_initialize_defaults( $wp_customize, $footer_settings_ids );
	}

	add_action( 'customize_save_after', 'my_voice_footer_defaults' );
endif;
