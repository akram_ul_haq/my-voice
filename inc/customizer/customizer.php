<?php
/**
 * My Voice: Customizer
 *
 * @package My Voice
 * @since 1.0
 */

/**
 * The Theme Customizer.
 */

if ( ! function_exists( 'customizer_sanitize_select' ) ) {
	function customizer_sanitize_select( $input, $setting ) {

		// input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only.
		$input = sanitize_key( $input );

		// get the list of possible select options.
		$choices = $setting->manager->get_control( $setting->id )->choices;

		// return input if valid or return default option.
		return ( array_key_exists( $input, $choices ) ? $input : $setting->default );

	}
}

if ( ! function_exists( 'my_voice_initialize_defaults' ) ) :
	/**
	 * Helper function to initialize default values for settings as customizer api do not do so by default.
	 *
	 * @param WP_Customize_Manager $wp_customize - Instance of WP_Customize_Manager.
	 * @param $my_voice_settings_ids - Settings ID of the theme.
	 *
	 * @since 1.0.0
	 */
	function my_voice_initialize_defaults( WP_Customize_Manager $wp_customize, $my_voice_settings_ids ) {
		if ( is_array( $my_voice_settings_ids ) && ! empty( $my_voice_settings_ids ) ) {
			foreach ( $my_voice_settings_ids as $setting_id ) {
				$setting = $wp_customize->get_setting( $setting_id );
				if ( $setting ) {
					add_option( $setting->id, $setting->default );
				}
			}
		}
	}
endif;


/**
 * Custom Controls
 */
require_once MY_VOICE_CUSTOMIZER . 'custom/load.php';

/**
 * Editor Controls
 */
require_once MY_VOICE_CUSTOMIZER . 'custom/editor.php';


/**
 * General Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/general.php';


/**
 * Header Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/header.php';

/**
 * Banner Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/banner.php';

/**
 * Home Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/home.php';

/**
 * About Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/about.php';


/**
 * Pages Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/pages.php';

/**
 * Footer Settings
 */
require_once MY_VOICE_CUSTOMIZER . 'partials/footer.php';


/**
 * Filter to modify input label in repeater control
 * You can filter by control id and input name.
 *
 * @param string $string Input label.
 * @param string $id Input id.
 * @param string $control Control name.
 *
 * @modified 1.1.41
 *
 * @return string
 */
function repeater_labels( $string, $id, $control ) {
	if ( 'my_voice_skills_section' === $id ) {
		if ( 'customizer_repeater_subtitle_control' === $control ) {
			return esc_html__( 'Percentage digit only.', 'myvoice' );
		}
	}

	if ( 'my_voice_testimonials_section' === $id ) {
		if ( 'customizer_repeater_title_control' === $control ) {
			return esc_html__( 'Client Name.', 'myvoice' );
		}
		if ( 'customizer_repeater_subtitle_control' === $control ) {
			return esc_html__( 'Client Designation.', 'myvoice' );
		}
	}

	if ( 'my_voice_general_facts' === $id ) {
		if ( 'customizer_repeater_title_control' === $control ) {
			return esc_html__( 'Fact Name.', 'myvoice' );
		}
		if ( 'customizer_repeater_subtitle_control' === $control ) {
			return esc_html__( 'Fact Percentage digit only.', 'myvoice' );
		}
	}

	return $string;
}

add_filter( 'repeater_input_labels_filter', 'repeater_labels', 10, 3 );