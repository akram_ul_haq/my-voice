<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package My Voice
 * @since 1.0
 */

if ( ! function_exists( 'my_voice_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 */
	function my_voice_posted_on() {
		$by = ' ';

		if ( ! is_singular() ) {
			$by = 'by';
		}
		// Get the author name; wrap it in a link.
		$byline = sprintf(
		/* translators: %s: post author */
			__( $by . ' %s', 'myvoice' ),
			'<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>'
		);

		// Finally, let's write all of this to the page.
		echo '<span class="byline"> ' . $byline . '</span>';
	}
endif;