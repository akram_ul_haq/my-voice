<?php
/**
 * Template Name: Course Template
 *
 * Page template for course.
 *
 * @since    1.0.0
 * @package My Voice
 */

get_header();

/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Video Section Start-->
		<section class="tnit-courses-section pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 col-xs-12">
						<div class="tnit-courses-item tnit-courses-detail-info">
							<div class="tnit-text">
								<?php
								while ( have_posts() ) :
								the_post();
								?>
								<h2><?php the_title(); ?></h2>
								<div class="top-holder">
									<!--Inner Holder Start-->
									<div class="inner-holder">
										<?php echo get_avatar( get_the_author_meta( 'user_email' ), 30 ); ?>
										<h6><?php esc_html_e( 'Teacher', 'myvoice' ); ?>
											<span><?php my_voice_posted_on(); ?></span></h6>
									</div><!--Inner Holder End-->
									<!--Inner Holder Start-->
                                    <?php
                                    $rating_class = Stars_Rating();
                                    if( isset( $rating_class ) && ! empty( $rating_class ) ) :
                                    ?>
									<div class="inner-holder">
										<span class="review"><?php esc_html_e( 'Review', 'myvoice' ); ?></span>
										<?php $rating_class->rating_average(); ?>
									</div><!--Inner Holder End-->
									 <?php endif; ?>
									<!--Inner Holder Start-->
									<div class="inner-holder">
                                                <span class="user">
	                                                <?php esc_html_e( 'Students', 'myvoice' ); ?>
	                                                <em>
		                                                <?php
		                                                $args = array(
			                                                'meta_query'  => array(
				                                                array(
					                                                'key'     => 'my_voice_course',
					                                                'value'   => get_the_ID(),
					                                                'compare' => '=',
				                                                ),
			                                                ),
			                                                'count_total' => true,
		                                                );

		                                                $users = new WP_User_Query( $args );
		                                                echo esc_html( $users->get_total() );
		                                                ?>
	                                                </em>
                                                </span>

									</div><!--Inner Holder End-->
                                    <div class="bottom-text">

										<?php
										$course_price = get_post_meta( get_the_ID(), 'MY_VOICE_course_price', true );
										if ( ! empty( $course_price ) ) :
											?>
                                            <strong class="price"><span><?php esc_html_e( 'Course Fee:', 'myvoice' ); ?></span> <?php echo esc_html( $course_price ); ?>
                                            </strong>
										<?php else : ?>
                                            <strong class="price"><span><?php esc_html_e( 'Course Fee:', 'myvoice' ); ?></span> <?php esc_html_e( 'Free', 'myvoice' ); ?>
                                            </strong>
										<?php endif; ?>

										<?php
										$course_enroll_url = get_post_meta( get_the_ID(), 'MY_VOICE_course_enroll_url', true );
										if ( ! empty( $course_enroll_url ) ) :
											?>
                                            <a href="<?php echo esc_url( $course_enroll_url ); ?>" class="btn-enroll"><?php esc_html_e( 'Enroll Now', 'myvoice' ); ?></a>
										<?php endif; ?>
                                    </div>
								</div>
							</div>
							<figure class="tnit-thumb">
								<?php the_post_thumbnail( 'my-voice-single-featured-image' ); ?>
							</figure>
                            <div class="col-md-5 col-sm-12 col-xs-12" style="float: right; padding-right: 0;">
                                <!--Feature Box Start-->
                                <div class="tnit-features-box">
									<?php $course_features_title = get_post_meta( get_the_ID(), 'MY_VOICE_course_features_title', true ); ?>
                                    <h4><?php echo $course_features_title ? $course_features_title : __( 'COURSE FEATURES', 'myvoice' ); ?> </h4>
                                    <ul class="tnit-feature-listed">
										<?php
										$course_starting_date = get_post_meta( get_the_ID(), 'MY_VOICE_course_starting_date', true );
										if ( ! empty( $course_starting_date ) ) :
											?>
                                            <li>
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
												<?php esc_html_e( 'Starts', 'myvoice' ); ?>
                                                <span><?php echo esc_html( $course_starting_date ); ?></span>
                                            </li>
										<?php endif; ?>

										<?php
										$course_duration_months = get_post_meta( get_the_ID(), 'MY_VOICE_course_duration_months', true );
										if ( ! empty( $course_duration_months ) ) :
											?>
                                            <li>
                                                <i class="fa fa-hourglass-start" aria-hidden="true"></i>
												<?php esc_html_e( 'Duration', 'myvoice' ); ?>
                                                <span><?php echo esc_html( $course_duration_months ); ?></span>
                                            </li>
										<?php endif; ?>

										<?php
										$course_timing = get_post_meta( get_the_ID(), 'MY_VOICE_course_timing', true );
										if ( ! empty( $course_timing ) ) :
											?>
                                            <li>
                                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
												<?php esc_html_e( 'Timing', 'myvoice' ); ?>
                                                <span><?php echo esc_html( $course_timing ); ?></span>
                                            </li>
										<?php endif; ?>

										<?php
										$course_institution_name = get_post_meta( get_the_ID(), 'MY_VOICE_course_institution_name', true );
										if ( ! empty( $course_institution_name ) ) :
											?>
                                            <li>
                                                <i class="fa fa-university" aria-hidden="true"></i>
												<?php esc_html_e( 'Institution', 'myvoice' ); ?>
                                                <span><?php echo esc_html( $course_institution_name ); ?></span>
                                            </li>
										<?php endif; ?>

										<?php
										$course_seats_available = get_post_meta( get_the_ID(), 'MY_VOICE_course_seats_available', true );
										if ( ! empty( $course_seats_available ) ) :
											?>
                                            <li>
                                                <i class="fa fa-clock-o" aria-hidden="true"></i>
												<?php esc_html_e( 'Seats Available', 'myvoice' ); ?>
                                                <span><?php echo esc_html( $course_seats_available ); ?></span>
                                            </li>
										<?php endif; ?>

										<?php
										$course_level = get_post_meta( get_the_ID(), 'MY_VOICE_course_level', true );
										if ( ! empty( $course_level ) ) :
											?>
                                            <li>
                                                <i class="fa fa-sort-alpha-desc" aria-hidden="true"></i>
												<?php esc_html_e( 'Level', 'myvoice' ); ?>
                                                <span><?php echo esc_html( $course_level ); ?></span>
                                            </li>
										<?php endif; ?>
                                    </ul>

                                </div><!--Feature Box End-->
                            </div>
								<?php the_content(); ?>
							<?php endwhile; // End of the loop. ?>
							<!--Tabs Outer Start-->
							<div class="tl-tabs-outer">
								<!-- Nav tabs -->
								<ul class="nav nav-tabs">
									<li role="presentation" class="active">
										<a href="#tl-tab-list1" aria-controls="tl-tab-list1" role="tab" data-toggle="tab">
											<?php esc_html_e( 'Leave A Replly', 'myvoice' ); ?>
										</a>
									</li>
									<li role="presentation">
										<a href="#tl-tab-list2" aria-controls="tl-tab-list2" role="tab" data-toggle="tab">
											<?php esc_html_e( 'Comments', 'myvoice' ); ?>
										</a>
									</li>
								</ul>
								<!-- Nav tabs End-->

								<!-- Tab Content Start -->
								<div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="tl-tab-list1">
										<!--Leave Review Outer Start-->
										<div class="tl-review-outer">
											<?php
											if ( is_user_logged_in() ) :
												$commets_calss = 'col-xs-12';
											else :
												$commets_calss = 'col-md-6 col-sm-6 col-xs-12';
											endif;

											$args          = '';
											$commenter     = wp_get_current_commenter();
											$user          = wp_get_current_user();
											$user_identity = $user->exists() ? $user->display_name : '';

											$args = wp_parse_args( $args );
											if ( ! isset( $args['format'] ) ) {
												$args['format'] = current_theme_supports( 'html5', 'comment-form' ) ? 'html5' : 'xhtml';
											}

											$req           = get_option( 'require_name_email' );
											$aria_req      = ( $req ? " aria-required='true'" : '' );
											$html_req      = ( $req ? " required='required'" : '' );
											$html5         = 'html5' === $args['format'];
											$custom_fields = array(
												'author' => '<div class="col-md-6 col-sm-6 col-xs-12"><div class="inner-holder">' . '<label for="author">' . __( 'Name', 'myvoice' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
												            '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" maxlength="245" ' . $aria_req . $html_req . ' /></div>',
												'email'  => '<div class="inner-holder"><label for="email">' . __( 'Email', 'myvoice' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' . '<input id="email" name="email" ' . ( $html5 ? 'type="email"' : 'type="text"' ) . ' value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" maxlength="100" aria-describedby="email-notes"' . $aria_req . $html_req . ' /></div></div>',
												'url'    => '',
											);

											$comments_arg = array(
												'fields'               => $custom_fields,
												'class_form'           => 'tl-review-form row',
												'title_reply'          => __( 'Leave a REPLY', 'myvoice' ),
												'title_reply_to'       => __( 'Leave a REPLY to %s', 'myvoice' ),
												'class_submit'         => 'btn-submit',
												'comment_notes_before' => '<p class="col-xs-12 comment-notes"><span id="email-notes">' . __( 'Your email address will not be published.', 'myvoice' ) . '</span></p>',
												'submit_field'         => '<div class="col-xs-12 form-submit">%1$s %2$s</div>',
												'title_reply_before'   => '<div class="block-title"><h3 id="reply-title" class="comment-reply-title">',
												'title_reply_after'    => '</h3></div>',
												'comment_field'        => '<div class="' . $commets_calss . '"> <div class="inner-holder"><label for="comment">' . _x( 'Message *', 'noun', 'myvoice' ) . '</label> <textarea id="comment" name="comment" aria-required="true" required="required"></textarea></div></div>',

											);
											comment_form( $comments_arg );
											?>
										</div><!--Leave Review Outer End-->

									</div>
									<div role="tabpanel" class="tab-pane" id="tl-tab-list2">
										<div class="row">
											<?php if ( is_single() && isset( $post->post_author ) ) : ?>
												<div class="col-md-6 col-sm-6 col-xs-12">
													<!--Comment Box Start-->
													<div class="tl-comments-box">
														<ul class="tl-comments-listed">
															<li>
																<div class="tl-comments-inner">
																	<figure class="tl-thumb">
																		<?php echo get_avatar( get_the_author_meta( 'user_email' ), 60 ); ?>
																	</figure>
																	<div class="text-holder">
																		<h4>
																			<?php
																			the_author();
																			$u_data     = get_userdata( $user->ID );
																			$registered = $u_data->user_registered;
																			?>
																			–
																			<span class="date">
																					<?php echo date( "M d, Y", strtotime( $registered ) ); ?>
																				</span>
																		</h4>
																		<?php
																		$user_description = get_the_author_meta( 'user_description', $post->post_author );
																		if ( $user_description ) {
																			echo '<p>' . esc_html( $user_description ) . '</p>';
																		}
																		?>
																	</div>
																</div>
															</li>
														</ul>
													</div><!--Comment Box End-->
												</div>
											<?php endif; ?>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<!--Comment Box Start-->
												<?php
												if ( comments_open() || get_comments_number() ) :
													comments_template();
												endif;
												?>
												<!--Comment Box End-->
											</div>

										</div>
									</div>
								</div>
								<!-- Tab Content End -->
							</div>
							<!--Tabs Outer End-->
						</div><!--Courses Item End-->

					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php
							if ( is_active_sidebar( 'default-course-sidebar' ) ) {
								dynamic_sidebar( 'default-course-sidebar' );
							}
							?>
						</aside>
						<!--Sidebar Outer End-->
					</div>
				</div>
			</div>
		</section><!--Video Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>
