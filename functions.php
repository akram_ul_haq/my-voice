<?php
/**
 * My Voice functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package My Voice
 * @since 1.0
 */

/**
 * My Voice only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';

	return;
}

// Theme version.
define( 'MY_VOICE_THEME_VERSION', '1.0' );

// Customizer Path.
define( 'MY_VOICE_CUSTOMIZER', get_theme_file_path() . '/inc/customizer/' );
define( 'MY_VOICE_INC', get_theme_file_path() . '/inc/' );
define( 'MY_VOICE_TEMP_PARTS', get_theme_file_path() . '/template-parts/' );
define( 'MY_VOICE_ASSETS', get_theme_file_path() . '/assets/' );


/**
 * Defined html tags to be used in
 * wp_kses function across the theme.
 */
$my_voice_allowed_tags = array(
	'a'      => array(
		'href'  => array(),
		'title' => array(),
		'alt'   => array(),
		'rel'   => array(),
	),
	'ul'     => array(
		'class' => array(),
		'id'    => array(),
	),
	'i'      => array(
		'class' => array(),
		'id'    => array(),
	),
	'span'   => array(
		'class' => array(),
		'id'    => array(),
	),
	'li'     => array(),
	'b'      => array(),
	'br'     => array(),
	'div'    => array(
		'class' => array(),
		'id'    => array(),
	),
	'em'     => array(),
	'strong' => array(),
);


if ( ! function_exists( 'my_voice_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function my_voice_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
		 * If you're building a theme based on My Voice, use a find and replace
		 * to change 'myvoice' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'myvoice' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * Enable support for WooCommerce in this theme.
		 *
		 * @link https://docs.woocommerce.com/document/declare-woocommerce-support-in-third-party-theme/
		 */
		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-slider' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_image_size( 'my-voice-featured-image', 400, 400, true );
		add_image_size( 'my-voice-home-featured-image', 400, 250, true );

		add_image_size( 'my-voice-single-featured-image', 1200, 9999, true );

		add_image_size( 'my-voice-related-featured-image', 215, 215, true );

		add_image_size( 'my-voice-video-featured-image', 600, 400, true );

		add_image_size( 'my-voice-events-image', 660, 120, true );

		add_image_size( 'my-voice-thumbnail-avatar', 100, 100, true );

		add_image_size( 'my-voice-course-image', 270, 260, true );

		// Set the default content width.
		$GLOBALS['content_width'] = 1170;

		// This theme uses wp_nav_menu() in two locations.
		register_nav_menus( array(
			'top' => __( 'Top Menu', 'myvoice' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );


		// Add theme support for Custom Logo.
		add_theme_support( 'custom-logo', array(
			'width'       => 225,
			'height'      => 70,
			'flex-height' => true,
			'flex-width'  => true,
		) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );
	}

	add_action( 'after_setup_theme', 'my_voice_setup' );
}

if ( ! function_exists( 'my_voice_content_width' ) ) {
	/**
	 * Set the content width in pixels, based on the theme's design and stylesheet.
	 *
	 * Priority 0 to make it available to lower priority callbacks.
	 *
	 * @global int $content_width
	 */
	function my_voice_content_width() {

		$content_width = $GLOBALS['content_width'];

		// Get layout.
		$page_layout = get_theme_mod( 'page_layout' );

		// Check if layout is one column.
		if ( 'one-column' === $page_layout ) {
			if ( my_voice_is_frontpage() ) {
				$content_width = 1170;
			} elseif ( is_page() ) {
				$content_width = 992;
			}
		}

		// Check if is single post and there is no sidebar.
		if ( is_single() && ! is_active_sidebar( 'default-sidebar' ) ) {
			$content_width = 1170;
		}

		/**
		 * Filter My Voice content width of the theme.
		 *
		 * @since My Voice 1.0
		 *
		 * @param int $content_width Content width in pixels.
		 */
		$GLOBALS['content_width'] = apply_filters( 'my_voice_content_width', $content_width );
	}

	add_action( 'template_redirect', 'my_voice_content_width', 0 );
}


if ( ! function_exists( 'my_voice_widgets_init' ) ) {
	/**
	 * Register widget area.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
	 */
	function my_voice_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Default Sidebar', 'myvoice' ),
			'id'            => 'default-sidebar',
			'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Page Sidebar', 'myvoice' ),
			'id'            => 'default-page-sidebar',
			'description'   => __( 'Add widgets here to appear in your footer.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Video Sidebar', 'myvoice' ),
			'id'            => 'default-video-sidebar',
			'description'   => __( 'Add widgets here to appear in your Video Page.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Courses Sidebar', 'myvoice' ),
			'id'            => 'default-course-sidebar',
			'description'   => __( 'Add widgets here to appear in your Courses Page.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Events Sidebar', 'myvoice' ),
			'id'            => 'default-events-sidebar',
			'description'   => __( 'Add widgets here to appear in your Events Page.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );

		register_sidebar( array(
			'name'          => __( 'Homepage Events Section Sidebar', 'myvoice' ),
			'id'            => 'default-homepage-events-sidebar',
			'description'   => __( 'This sidebar support olny twitter widget.', 'myvoice' ),
			'before_widget' => '',
			'after_widget'  => '',
			'before_title'  => '<div class="tnit-heading-outer"><h3 class="widget-title">',
			'after_title'   => '</h3></div>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer one', 'myvoice' ),
			'id'            => 'footer-sidebar-one',
			'description'   => __( 'Add widgets here to appear in your footer first column.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget widget-about %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer two', 'myvoice' ),
			'id'            => 'footer-sidebar-two',
			'description'   => __( 'Add widgets here to appear in your footer second column.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Three', 'myvoice' ),
			'id'            => 'footer-sidebar-three',
			'description'   => __( 'Add widgets here to appear in your footer third column.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Four', 'myvoice' ),
			'id'            => 'footer-sidebar-four',
			'description'   => __( 'Add widgets here to appear in your footer fourth column.', 'myvoice' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="widget-title widget-appointment">',
			'after_title'   => '</h4>',
		) );
	}

	add_action( 'widgets_init', 'my_voice_widgets_init' );
}

if ( ! function_exists( 'my_voice_javascript_detection' ) ) {
	/**
	 * Handles JavaScript detection.
	 *
	 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
	 *
	 * @since My Voice 1.0
	 */
	function my_voice_javascript_detection() {
		echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
	}

	add_action( 'wp_head', 'my_voice_javascript_detection', 0 );

}

if ( ! function_exists( 'my_voice_pingback_header' ) ) {
	/**
	 * Add a pingback url auto-discovery header for singularly identifiable articles.
	 */
	function my_voice_pingback_header() {
		if ( is_singular() && pings_open() ) {
			printf( '<link rel="pingback" href="%s">' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
		}
	}

	add_action( 'wp_head', 'my_voice_pingback_header' );

}

if ( ! function_exists( 'my_voice_scripts' ) ) {
	/**
	 * Enqueue scripts and styles.
	 */
	function my_voice_scripts() {

		// Theme stylesheet.
		wp_enqueue_style( 'my-voice-style', get_stylesheet_uri() );
		wp_enqueue_style( 'my-voice-theme-style', get_theme_file_uri( '/assets/css/style.css' ) );
		wp_enqueue_style( 'my-voice-bootstrap-style', get_theme_file_uri( '/assets/css/bootstrap.css' ) );
		wp_enqueue_style( 'my-voice-owl-carousel-style', get_theme_file_uri( '/assets/css/owl-carousel.css' ) );
		wp_enqueue_style( 'my-voice-theme-color-style', get_theme_file_uri( '/assets/css/theme-color.css' ) );
		wp_enqueue_style( 'my-voice-responsive-style', get_theme_file_uri( '/assets/css/responsive.css' ) );
		wp_enqueue_style( 'my-voice-animate-style', get_theme_file_uri( '/assets/css/animate.css' ) );
		wp_enqueue_style( 'my-voice-jquery-scrollbar-style', get_theme_file_uri( '/assets/css/jquery.scrollbar.css' ) );
		wp_enqueue_style( 'my-voice-font-awesome-style', get_theme_file_uri( '/assets/css/font-awesome.min.css' ) );
		wp_enqueue_style( 'my-voice-icomoon-style', get_theme_file_uri( '/assets/css/icomoon.css' ) );

		// Theme Scripts.
		wp_enqueue_script( 'my-voice-bootstrap', get_theme_file_uri( '/assets/js/bootstrap.min.js' ), array( 'jquery' ), '3.3.7', false );
		wp_enqueue_script( 'my-voice-html5shiv', get_theme_file_uri( '/assets/js/html5shiv.js' ), array( 'jquery' ), '1.0', false );
		wp_enqueue_script( 'my-voice-owl-carousel', get_theme_file_uri( '/assets/js/owl-carousel.js' ), array( 'jquery' ), '1.0', false );
		wp_enqueue_script( 'my-voice-modernizr', get_theme_file_uri( '/assets/js/modernizr.custom.js' ), array( 'jquery' ), '2.7.1', false );
		wp_enqueue_script( 'my-voice-counterup', get_theme_file_uri( '/assets/js/jquery.counterup.min.js' ), array( 'jquery' ), '1.0', false );
		wp_enqueue_script( 'my-voice-scrollbar', get_theme_file_uri( '/assets/js/jquery.scrollbar.js' ), array( 'jquery' ), '0.2.11', false );
		wp_enqueue_script( 'my-voice-video-player', get_theme_file_uri( '/assets/js/video-player.js' ), array( 'jquery' ), '1.1', true );
		wp_enqueue_script( 'my-voice-circliful', get_theme_file_uri( '/assets/js/jquery.circliful.js' ), array( 'jquery' ), '1.0', false );
		wp_enqueue_script( 'my-voice-progressbar', get_theme_file_uri( '/assets/js/jQuery-plugin-progressbar.js' ), array( 'jquery' ), '1.0', true );
		wp_enqueue_script( 'my-voice-waypoints', get_theme_file_uri( '/assets/js/waypoints.min.js' ), array( 'jquery' ), '1.0', false );
		wp_enqueue_script( 'my-voice-script', get_theme_file_uri( '/assets/js/script.js' ), array( 'jquery' ), '1.0', true );

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
		if ( is_page_template( 'templates/contact-us.php' ) ) {

			wp_enqueue_script( 'my-voice-map-url', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyAPf96eskAPXvkyDLPyYhxSCAKIziCUG_E', array( 'jquery' ), '1.0', false );
			wp_enqueue_script( 'my-voice-map-script', get_theme_file_uri( '/assets/js/map.js' ), array( 'jquery' ), '1.0', true );

		}
	}

	add_action( 'wp_enqueue_scripts', 'my_voice_scripts' );
}


if ( ! function_exists( 'my_voice_content_image_sizes_attr' ) ) {
	/**
	 * Add custom image sizes attribute to enhance responsive image functionality
	 * for content images.
	 *
	 * @since My Voice 1.0
	 *
	 * @param string $sizes A source size value for use in a 'sizes' attribute.
	 * @param array $size Image size. Accepts an array of width and height values in pixels (in that order).
	 *
	 * @return string A source size value for use in a content image 'sizes' attribute.
	 */
	function my_voice_content_image_sizes_attr( $sizes, $size ) {
		$width = $size[0];

		if ( 740 <= $width ) {
			$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
		}

		if ( is_active_sidebar( 'default-sidebar' ) || is_archive() || is_search() || is_home() || is_page() ) {
			if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
				$sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
			}
		}

		return $sizes;
	}

	add_filter( 'wp_calculate_image_sizes', 'my_voice_content_image_sizes_attr', 10, 2 );
}


if ( ! function_exists( 'my_voice_header_image_tag' ) ) {
	/**
	 * Filter the `sizes` value in the header image markup.
	 *
	 * @since My Voice 1.0
	 *
	 * @param string $html The HTML image tag markup being filtered.
	 * @param object $header The custom header object returned by 'get_custom_header()'.
	 * @param array $attr Array of the attributes for the image tag.
	 *
	 * @return string The filtered header image HTML.
	 */
	function my_voice_header_image_tag( $html, $header, $attr ) {
		if ( isset( $attr['sizes'] ) ) {
			$html = str_replace( $attr['sizes'], '100vw', $html );
		}

		return $html;
	}

	add_filter( 'get_header_image_tag', 'my_voice_header_image_tag', 10, 3 );
}


if ( ! function_exists( 'my_voice_post_thumbnail_sizes_attr' ) ) {
	/**
	 * Add custom image sizes attribute to enhance responsive image functionality
	 * for post thumbnails.
	 *
	 * @since My Voice 1.0
	 *
	 * @param array $attr Attributes for the image markup.
	 * @param int $attachment Image attachment ID.
	 * @param array $size Registered image size or flat array of height and width dimensions.
	 *
	 * @return array A source size value for use in a post thumbnail 'sizes' attribute.
	 */
	function my_voice_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
		if ( is_archive() || is_search() || is_home() ) {
			$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		} else {
			$attr['sizes'] = '100vw';
		}

		return $attr;
	}

	add_filter( 'wp_get_attachment_image_attributes', 'my_voice_post_thumbnail_sizes_attr', 10, 3 );

}


if ( ! function_exists( 'my_voice_front_page_template' ) ) {
	/**
	 * Use front-page.php when Front page displays is set to a static page.
	 *
	 * @since My Voice 1.0
	 *
	 * @param string $template front-page.php.
	 *
	 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
	 */
	function my_voice_front_page_template( $template ) {
		return is_home() ? '' : $template;
	}

	add_filter( 'frontpage_template', 'my_voice_front_page_template' );
}


if ( ! function_exists( 'the_excerpt_max_char_length' ) ) {
	/**
	 * Use get_the_excerpt() to print an excerpt by specifying a maximium number of characters.
	 *
	 * @since My Voice 1.0
	 *
	 * @param string $charlength front-page.php.
	 */
	function the_excerpt_max_char_length( $charlength ) {
		$excerpt = get_the_excerpt();
		$charlength ++;

		if ( mb_strlen( $excerpt ) > $charlength ) {
			$subex   = mb_substr( $excerpt, 0, $charlength - 5 );
			$exwords = explode( ' ', $subex );
			$excut   = - ( mb_strlen( $exwords[ count( $exwords ) - 1 ] ) );
			if ( $excut < 0 ) {
				echo mb_substr( $subex, 0, $excut );
			} else {
				echo $subex;
			}
			echo '[...]';
		} else {
			echo $excerpt;
		}
	}
}

/**
 * This file contains pagination functions
 */

if ( ! function_exists( 'theme_pagination' ) ) {
	/**
	 * Pagination
	 *
	 * @param string $pages
	 */
	function theme_pagination( $pages = '' ) {

		global $paged;

		if ( is_page_template( 'templates/home.php' ) ) {
			$paged = intval( get_query_var( 'page' ) );
		}

		if ( empty( $paged ) ) {
			$paged = 1;
		}

		$prev       = $paged - 1;
		$next       = $paged + 1;
		$range      = 3; // only change it to show more links.
		$show_items = ( $range * 2 ) + 1;

		if ( '' === $pages ) {
			global $wp_query;
			$pages = $wp_query->max_num_pages;
			if ( ! $pages ) {
				$pages = 1;
			}
		}

		if ( 1 !== $pages ) {
			echo "<ul class='pagination'>";
			echo ( $paged > 2 && $paged > $range + 1 && $show_items < $pages ) ? "<li class='page-item'><a href='" . get_pagenum_link( 1 ) . "' class='page-link'>" . __( 'First', 'myvoice' ) . "</a></li> " : "";
			echo ( $paged > 1 && $show_items < $pages ) ? "<li class='page-item'><a href='" . get_pagenum_link( $prev ) . "' class='page-link' ><i class='fa fa-angle-left'></i></a></li> " : "";

			for ( $i = 1; $i <= $pages; $i ++ ) {
				if ( 1 !== $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $show_items ) ) {
					echo ( $paged === $i ) ? "<li class='page-item'><a href='" . get_pagenum_link( $i ) . "' class='page-link current' >" . $i . "</a></li> " : "<li class='page-item'><a href='" . get_pagenum_link( $i ) . "' class='page-link'>" . $i . "</a></li> ";
				}
			}

			echo ( $paged < $pages && $show_items < $pages ) ? "<li class='page-item'><a href='" . get_pagenum_link( $next ) . "' class='page-link' ><i class='fa fa-angle-right'></i></a></li> " : "";
			echo ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $show_items < $pages ) ? "<li class='page-item'><a href='" . get_pagenum_link( $pages ) . "' class='page-link' >" . __( 'Last', 'myvoice' ) . "</a></li> " : "";
			echo '</ul>';
		}
	}
}


/**
 * Comments Walker Class
 */
class Comment_Walker extends Walker_Comment {
	var $tree_type = 'comment';
	var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );

	// constructor – wrapper for the comments list.
	function __construct() {
		?>

        <ul class="comments-list tl-comments-listed">

	<?php }

	// start_lvl – wrapper for child comments list.
function start_lvl( &$output, $depth = 0, $args = array() ) {
	$GLOBALS['comment_depth'] = $depth + 2;
	?>

    <ul class="child-comments comments-list">

<?php }

	// end_lvl – closing wrapper for child comments list.
function end_lvl( &$output, $depth = 0, $args = array() ) {
	$GLOBALS['comment_depth'] = $depth + 2;
	?>

    </ul>

<?php }

	// start_el – HTML for comment template.
function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
	$depth ++;
	$GLOBALS['comment_depth'] = $depth;
	$GLOBALS['comment']       = $comment;
	$parent_class             = ( empty( $args['has_children'] ) ? '' : 'parent' );

	if ( 'article' === $args['style'] ) {
		$tag       = 'article';
		$add_below = 'comment';
	} else {
		$tag       = 'article';
		$add_below = 'comment';
	} ?>

    <li <?php comment_class( empty( $args['has_children'] ) ? 'clearfix' : 'clearfix' ) ?>
            id="comment-<?php comment_ID() ?>" itemprop="comment" itemscope itemtype="http://schema.org/Comment">
    <div class="tl-comments-inner">
        <figure class="tl-thumb">
			<?php
			$defaults_arg = array(
				// get_avatar_data() args.
				'size'  => 60,
				'class' => 'media-object',

			);

			echo get_avatar( $comment, 60, '', '', $defaults_arg ); ?>
        </figure>
        <div class="text-holder">
            <h5>
                <cite class="fn"><?php printf( __( '%s', 'myvoice' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?></cite>
                <span class="divider">-</span>
                <a class="light-gray" href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
                    <time datetime="<?php comment_time( 'c' ); ?>">
						<?php printf( __( '%1$s', 'myvoice' ), get_comment_date(), get_comment_time() ); ?>
                    </time>
                </a>
            </h5>
            <p><?php comment_text(); ?></p>
            <a href="#">
				<?php comment_reply_link( array_merge( array( 'before' => '' ), array(
					'depth'     => $depth,
					'max_depth' => $args['max_depth']
				) ) ); ?>
            </a>
        </div>
    </div>
<?php }

	// end_el – closing HTML for comment template.
function end_el( &$output, $comment, $depth = 0, $args = array() ) {
	?>

    </li>

	<?php
}

	// destructor – closing wrapper for the comments list.
	function __destruct() {
		?>

        </ul>

		<?php
	}

}

/**
 * Metabox Show/Hide
 */
function metabox_show_hide() {
	if ( is_admin() ) {
		$script = <<< EOF
<script type='text/javascript'>
	jQuery(document).ready(function($) {
		var contactMeta = $('#contact-page-meta-box');
		$(contactMeta).hide();
		'templates/contact-us.php' === $('#page_template').val() ? $(contactMeta).show(): '';
		$('#page_template').on('change', function() {
			if('templates/contact-us.php' === $(this).val() ){
				$(contactMeta).show();
			}else{
				 $(contactMeta).hide();
			}
		   
		});
	});
</script>
EOF;
		echo $script;
	}
}

add_action( 'admin_footer', 'metabox_show_hide' );

function get_page_by_slug($page_slug, $output = OBJECT, $post_type = 'page' ) {
	global $wpdb;
	$page = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_name = %s AND post_type= %s AND post_status = 'publish'", $page_slug, $post_type ) );
	if ( $page )
		return get_post($page, $output);
	return null;
}
/**
 * Check if it is a blog page.
 */
function is_blog() {
	global $post;
	$posttype = get_post_type( $post );

	return ( ( 'post' === $posttype ) ) ? true : false;
}

add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );

/**
 * Ensure cart contents update when products are added to the cart via AJAX (place the following in functions.php).
 * Used in conjunction with https://gist.github.com/DanielSantoro/1d0dc206e242239624eb71b2636ab148
 * Compatible with 3.0.1+, for lower versions, remove "woocommerce_" from the first mention on Line 4.
 *
 * @since My Voice 1.0
 *
 * @param string $fragments front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>

    <!--Cart Holder Start-->
    <div class="tnit-cart-holder">
        <!--Button Cart-->
        <div class="btn-cart">
            <span class="icomoon icon-shopping-bag"></span>
            <span class="num"><?php echo sprintf( _n( '%d', '%d', $woocommerce->cart->cart_contents_count, 'myvoice' ), $woocommerce->cart->cart_contents_count ); ?></span>
            <!-- Cart Listed Start-->
            <ul class="tnit-cart-listed">
                <li>
                    <div class="inner-cart">
                        <div class="text">
                            <a href="<?php echo $woocommerce->cart->get_cart_url(); ?>" class="tnit-btn-cart">
                                <i class="fa fa-shopping-cart" aria-hidden="true"></i>
								<?php esc_html_e( 'Cart', 'myvoice' ); ?>
                            </a>
                            <strong class="price"><?php esc_html_e( 'Total: ', 'myvoice' );
								echo $woocommerce->cart->get_cart_total();
								?>
                            </strong>
                        </div>
                    </div>
                </li>
            </ul><!-- Cart Listed End-->
        </div><!--Button Cart End-->
    </div>
    <!--Cart Holder End-->

	<?php

	$fragments['div.tnit-cart-holder'] = ob_get_clean();

	return $fragments;

}

function add_extra_user_fields( $user ) {
	$args  = array(
		'post_type'      => 'courses',
		'posts_per_page' => - 1,
	);
	$query = new WP_Query( $args );
	?>
    <h3><?php esc_html_e( 'Select Courses', 'myvoice' ); ?></h3>
    <select name="my_voice_course">
        <option value=" "><?php esc_html_e( "Select Course", 'myvoice' ) ?></option>
		<?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
            <option value="<?php the_ID(); ?>"
				<?php if ( get_user_meta( $user->ID, "my_voice_course", true ) == get_the_ID() ) {
					echo 'selected';
				} ?>>
				<?php the_title();
				wp_reset_query(); ?>
            </option>
		<?php endwhile; endif; ?>
    </select>
	<?php
}

add_action( 'show_user_profile', 'add_extra_user_fields' );
add_action( 'edit_user_profile', 'add_extra_user_fields' );

function save_extra_user_fields( $user_id ) {
	update_user_meta( $user_id, 'my_voice_course', sanitize_text_field( $_POST['my_voice_course'] ) );
}

add_action( 'personal_options_update', 'save_extra_user_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_fields' );


/**
 * Woo Commerce templates.
 */
if ( file_exists( MY_VOICE_INC . 'woocommerce.php' ) ) {
	require_once MY_VOICE_INC . 'woocommerce.php';
}

/**
 * Custom template tags for this theme.
 */
if ( file_exists( MY_VOICE_INC . 'template-tags.php' ) ) {
	require_once MY_VOICE_INC . 'template-tags.php';
}
/**
 * Additional features to allow styling of the templates.
 */
if ( file_exists( MY_VOICE_INC . 'template-functions.php' ) ) {
	require_once MY_VOICE_INC . 'template-functions.php';
}
/**
 * Customizer additions.
 */

if ( file_exists( MY_VOICE_CUSTOMIZER . 'customizer.php' ) ) {
	require_once MY_VOICE_CUSTOMIZER . 'customizer.php';
}

/*
 * TGM plugin activation
 */
if ( file_exists( MY_VOICE_INC . 'tgm/my-voice-required-plugins.php' ) ) {
	require_once MY_VOICE_INC . 'tgm/my-voice-required-plugins.php';
}

/*
 * Metabox
 */
if ( file_exists( MY_VOICE_INC . 'config-meta-boxes.php' ) ) {
	require_once MY_VOICE_INC . 'config-meta-boxes.php';
}

/*
 * Breadcrumbs
 */
if ( file_exists( MY_VOICE_INC . 'breadcrumbs.php' ) ) {
	require_once MY_VOICE_INC . 'breadcrumbs.php';
}

/*
 * Twitter Widget
 */
if ( file_exists( MY_VOICE_INC . 'widgets/twitter/my-voice-tweets.php' ) ) {
	require_once MY_VOICE_INC . 'widgets/twitter/my-voice-tweets.php';
}


/*
 * Recent Courses Widget
 */
if ( file_exists( MY_VOICE_INC . 'widgets/featured-courses/class-featured-courses-widget.php' ) ) {
	require_once MY_VOICE_INC . 'widgets/featured-courses/class-featured-courses-widget.php';
}


/*
 * Post Like System
 */
if ( file_exists( MY_VOICE_INC . 'like/post-like.php' ) ) {
	require_once MY_VOICE_INC . 'like/post-like.php';
}

/*
 * Instagram Widget
 */
if ( file_exists( MY_VOICE_INC . 'widgets/instagram/instagram.php' ) ) {
	require_once MY_VOICE_INC . 'widgets/instagram/instagram.php';
}

/*
 * Footer Info Widget
 */
if ( file_exists( MY_VOICE_INC . 'widgets/footer-info-widget/footer-info-widget.php' ) ) {
	require_once MY_VOICE_INC . 'widgets/footer-info-widget/footer-info-widget.php';
}

/*
 * Book Widget
 */
if ( file_exists( MY_VOICE_INC . 'widgets/book-widget/book-widget.php' ) ) {
	require_once MY_VOICE_INC . 'widgets/book-widget/book-widget.php';
}

/*
 * Follow Me Widget
 */
if ( file_exists( MY_VOICE_INC . 'widgets/follow-me/follow-me.php' ) ) {
	require_once MY_VOICE_INC . 'widgets/follow-me/follow-me.php';
}

/*
 * Post Types Added
 */
if ( file_exists( MY_VOICE_INC . 'post-types.php' ) ) {
	require_once MY_VOICE_INC . 'post-types.php';
}

/*
 * About Shortcode
 */
if ( file_exists( MY_VOICE_INC . 'shortcodes/about.php' ) ) {
	require_once MY_VOICE_INC . 'shortcodes/about.php';
}

/*
 * About Shortcode
 */
if ( file_exists( MY_VOICE_INC . 'shortcodes/registration.php' ) ) {
	require_once MY_VOICE_INC . 'shortcodes/registration.php';
}

/*
 * One Click Demo
 */
if ( file_exists( MY_VOICE_ASSETS . 'demo/demo.php' ) ) {
	require_once MY_VOICE_ASSETS . 'demo/demo.php';
}