<?php
/**
 * Template Name: Contact Template
 *
 * Page template for contact.
 *
 * @since    1.0.0
 * @package My Voice
 */

get_header();

/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Contact Section Start-->
		<section class="tnit-contact-section pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-12 col-xs-12">
						<!--Leave Review Outer Start-->
						<div class="tl-review-outer">
							<h3><?php esc_html_e( 'Leave A Message', 'myvoice' ); ?></h3>
							<?php
							while ( have_posts() ) :
								the_post();
								the_content();
								echo '</br></br>';
							endwhile; // End of the loop.
							?>
							<!--Review Form Start-->
									<?php echo do_shortcode( get_post_meta( get_the_ID(), 'MY_VOICE_contact_form_shortcode', true ) ); ?>
								<!--Review Form End-->
						</div><!--Leave Review Outer End-->
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer tnit-sidebar-outer_v2">
							<div class="widget widget-contact">
								<?php
								$contact_details_title    = get_post_meta( get_the_ID(), 'MY_VOICE_contact_details_title', true );
								$contact_details_detail   = get_post_meta( get_the_ID(), 'MY_VOICE_contact_details_detail', true );
								$contact_details_location = get_post_meta( get_the_ID(), 'MY_VOICE_contact_details_location', true );
								$contact_details_number   = get_post_meta( get_the_ID(), 'MY_VOICE_contact_details_number', true );
								$contact_details_email    = get_post_meta( get_the_ID(), 'MY_VOICE_contact_details_email', true );
								$contact_details_skype    = get_post_meta( get_the_ID(), 'MY_VOICE_contact_details_skype', true );
								$contact_details_website  = get_post_meta( get_the_ID(), 'MY_VOICE_contact_details_website', true );

								if ( $contact_details_title ) {
									echo '<h3>' . esc_html( $contact_details_title ) . '</h3>';
								}

								if ( $contact_details_detail ) {
									echo '<p>' . esc_textarea( $contact_details_detail ) . '</p>';
								}
								echo '<ul class="about-info-listed about-info-listed_v2">';
								if ( $contact_details_location ) {
									echo '<li><i class="fa fa-map-marker" aria-hidden="true"></i>' . esc_html( $contact_details_location ) . '</li>';
								}
								if ( $contact_details_number ) {
									echo '<li><a href="' .esc_html__('tel:', 'myvoice') . esc_html( $contact_details_number ) . '"><i class="fa fa-phone" aria-hidden="true"></i>' . esc_html( $contact_details_number ) . '</a></li>';
								}
								if ( $contact_details_email ) {
									echo '<li><a href="mailto:' . esc_html( $contact_details_email ) . '" target="_top"><i class="fa fa-envelope" aria-hidden="true"></i>' . esc_html( $contact_details_email ) . '</a></li>';
								}
								if ( $contact_details_skype ) {
									echo '<li><i class="fa fa-skype" aria-hidden="true"></i>' . esc_html( $contact_details_skype ) . '</li>';
								}
								if ( $contact_details_website ) {
									echo '<li><i class="fa fa-globe" aria-hidden="true"></i>' . esc_url( $contact_details_website ) . '</li>';
								}
								echo '</ul>';
								?>
							</div>
						</aside>
					</div>
				</div>
			</div>
		</section><!--Contact Section End-->

		<!--Map Section Start-->
		<section class="tnit-map-section">
			<div id="tnit-map"></div>
		</section><!--Map Section End-->

	</div>
	<!--Main Content End-->
<?php get_footer(); ?>