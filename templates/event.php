<?php
/**
 * Template Name: Event Template
 *
 * Template for all event posts.
 *
 * @since    1.0.0
 * @package My Voice
 */

get_header();


/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Event Section Start-->
		<section class="tnit-event-section pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 col-xs-12">
						<!--Heading Outer start-->
						<div class="tnit-heading-outer">
							<h2><?php esc_html_e( 'Coming up next', 'myvoice' ); ?></h2>
						</div><!--Heading Outer End-->

						<!--Event Listed Start-->
						<ul class="tnit-even-listed">
							<?php
							$args = array(
								'post_type'      => 'events',
								'posts_per_page' => 10,
							);
							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) :
								/* Start the Loop */
								while ( $loop->have_posts() ) :
									$loop->the_post();

									/*
									 * Include the Post-Format-specific template for the content.
									 * If you want to override this in a child theme, then include a file
									 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
									 */
									get_template_part( 'template-parts/post/content', 'event' );

								endwhile;

							else :

								get_template_part( 'template-parts/post/content', 'none' );
							endif;

							?>

						</ul><!--Event Listed End-->

						<!--Pagination Row Start-->
						<div class="tnit-pagination-row pd-t70">
							<nav aria-label="navigation">
								<?php theme_pagination(); ?>
							</nav>
						</div><!--Pagination Row End-->

					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php
							if ( is_active_sidebar( 'default-events-sidebar' ) ) {
								dynamic_sidebar( 'default-events-sidebar' );
							}
							?>

						</aside>
						<!--Sidebar Outer End-->
					</div>
				</div>
			</div>
		</section><!--Event Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>