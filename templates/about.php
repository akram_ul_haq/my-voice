<?php
/**
 * Template Name: About Template
 *
 * Page template for about.
 *
 * @since    1.0.0
 * @package My Voice
 */

get_header();

/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<?php if ( 'true' === get_option( 'my_voice_about_about_hide' ) ) : ?>
			<!--About Section Start-->
			<section class="tnit-about-section_v2">
				<div class="container">
					<div class="row">
						<?php if ( ! empty( get_option( 'my_voice_about_about_image' ) ) ) : ?>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<!--About Thumb Start-->
								<figure class="tnit-about-thumb">
									<img src="<?php echo esc_url( get_option( 'my_voice_about_about_image' ) ); ?>" alt="<?php bloginfo( 'title' ); ?>">
								</figure><!--About Thumb End-->
							</div>
						<?php endif; ?>
						<div class="col-md-7 col-sm-12 col-xs-12">
							<?php if ( ! empty( get_option( 'my_voice_about_section_contents' ) ) ) : ?>
								<!--About Text Start-->
								<div class="tnit-about-text">
									<?php echo wp_kses_post( get_option( 'my_voice_about_section_contents' ) ); ?>
								</div><!--About Text End-->
							<?php endif; ?>
							<?php
							$slider_social_list = get_theme_mod( 'my_voice_home_slider_social' );
							if ( ! empty( $slider_social_list ) ) {
								?>
								<div class="row tnit-social-row_v2">
									<?php
									/*This returns a json so we have to decode it*/
									$slider_social_list_decoded = json_decode( $slider_social_list );
									foreach ( $slider_social_list_decoded as $repeater_item ) {
										?>
										<div class="col-md-3 col-sm-6 col-xs-12">
											<!--Socail Box Start-->
											<div class="tnit-social-box">
												<a href="#"><i class="fa <?php echo esc_html( $repeater_item->icon_value ); ?>" aria-hidden="true"></i></a>
												<div class="tnit-text">
													<strong><?php echo do_shortcode( htmlspecialchars_decode( $repeater_item->shortcode ) ); ?></strong>
													<span><?php echo esc_html( $repeater_item->title ); ?></span>
												</div>
											</div><!--Socail Box End-->
										</div>
										<?php
									}
									?>
								</div>
								<?php
							}
							?>
						</div>
					</div>
				</div>
			</section><!--About Section End-->
		<?php endif; ?>
		<?php if ( 'true' === get_option( 'my_voice_about_qualities_hide' ) ) : ?>
			<!--Qualities Section Start-->
			<section class="tnit-qualities-section pd-tb70">
				<div class="container">
					<div class="row">
						<?php if ( ! empty( get_option( 'my_voice_qualities_section_contents' ) ) ) : ?>
							<div class="col-md-7 col-sm-12 col-xs-12">
								<!--Qualities Text Start-->
								<div class="tnit-qualities-text">
									<?php echo wp_kses_post( get_option( 'my_voice_qualities_section_contents' ) ); ?>
								</div>
							</div>
						<?php endif; ?>
						<?php if ( ! empty( get_option( 'my_voice_about_qualities_skills_hide' ) ) ) : ?>
							<div class="col-md-5 col-sm-12 col-xs-12">
								<!--Skill Outer Start-->
								<div class="tnit-skill-outer">
									<div class="row">
										<?php
										/*This returns a json so we have to decode it*/
										$slider_skils_list_decoded = json_decode( get_option( 'my_voice_skills_section' ) );
										foreach ( $slider_skils_list_decoded as $repeater_item ) {
											?>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<!--Skill Item Start-->
												<div class="tnit-skill-item">
													<div class="tnit-progress-bar position" data-percent="<?php echo esc_html( $repeater_item->subtitle ); ?>" data-duration="2000" data-color="#ccc,#ed7001"></div>
													<div class="tnit-text">
														<?php echo esc_html( $repeater_item->title ); ?>
													</div>
												</div><!--Socail Box End-->
											</div>
											<?php
										}
										?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</section><!--Qualities Section End-->
		<?php endif; ?>
		<?php if ( 'true' === get_option( 'my_voice_about_book_hide' ) ) : ?>
            <!--Book Section Start-->
			<section class="tnit-book-section pd-tb70">
				<div class="container">
					<div class="row">
						<?php if ( ! empty( get_option( 'my_voice_about_book_image' ) ) ) : ?>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<!--Book Thumb Start-->
								<div class="tnit-book-thumb">
									<img src="<?php echo esc_url( get_option( 'my_voice_about_book_image' ) ); ?>" alt="">
								</div><!--Book Thumb End-->
							</div>
						<?php endif; ?>
						<?php if ( ! empty( get_option( 'my_voice_about_book_section_contents' ) ) ) : ?>
							<div class="col-md-6 col-sm-12 col-xs-12">
								<!--Book Text Start-->
								<div class="tnit-book-text">
									<?php echo wp_kses_post( get_option( 'my_voice_about_book_section_contents' ) ); ?>
									<div class="inner-holder">
										<div class="row">
											<?php if ( ! empty( get_option( 'my_voice_about_audio_book_image' ) ) ) : ?>
												<div class="col-md-6 col-sm-6 col-xs-12">
												<figure class="screen-img">
													<img src="<?php echo esc_url( get_option( 'my_voice_about_audio_book_image' ) ); ?>" alt="">
													<figcaption class="tnit-caption">
														<a href="<?php echo esc_url( get_option( 'my_voice_about_audio_book_url' ) ); ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/play-btn.png" alt=""></a>
													</figcaption>
												</figure>
											</div>
											<?php endif; ?>
											<div class="col-md-6 col-sm-6 col-xs-12">
												<div class="audio-text">
													<span><?php esc_html_e( 'Now Available as an', 'myvoice' ); ?><span class="th-color"><?php esc_html_e( 'AudioBook', 'myvoice' ); ?></span></span>
													<a href="<?php echo esc_url( get_option( 'my_voice_about_audio_book_url' ) ); ?>" class="btn-play"><?php esc_html_e( 'Play now', 'myvoice' ); ?></a>
												</div>
											</div>
										</div>
									</div>
									<a href="<?php echo esc_url( get_option( 'my_voice_about_download_audio_book_url' ) ); ?>" class="tnit-btn-download"><i class="fa fa-download" aria-hidden="true"></i><?php esc_html_e( 'Download Book', 'myvoice' ); ?></a>
								</div><!--Book Text End-->
							</div>
						<?php endif; ?>
					</div>
				</div>
			</section><!--Book Section End-->
		<?php endif; ?>
		<?php if ( 'true' === get_option( 'my_voice_general_facts_hide' ) ) : ?>
            <!--Facts Section Start-->
            <section class="tnit-facts-section">
                <div class="container">
                    <!--Facts Inner Outer Start-->
                    <div class="facts-inner-outer">
						<?php if ( ! empty( get_option( 'my_voice_general_facts' ) ) ) : ?>
                            <ul class="tnit-facts-listed">
								<?php
								/*This returns a json so we have to decode it*/
								$slider_facts_list_decoded = json_decode( get_option( 'my_voice_general_facts' ) );
								foreach ( $slider_facts_list_decoded as $repeater_item ) {
									?>
                                    <li class="facts-item">
                                        <i class="<?php echo esc_html( $repeater_item->icon_value ); ?>" aria-hidden="true"></i>
                                        <div class="tnit-text">
                                            <strong class="tnit-counter"><?php echo esc_html( $repeater_item->subtitle ); ?></strong>
                                            <span><?php echo esc_html( $repeater_item->title ); ?></span>
                                        </div>
                                    </li>
									<?php
								}
								?>

                            </ul>
						<?php endif; ?>
                    </div><!--Facts Inner Outer End-->
                </div>
            </section><!--Facts Section End-->
		<?php endif; ?>
		<?php if ( 'true' === get_option( 'my_voice_about_testimonials_hide' ) ) : ?>
            <!--Testimonial Section Start-->
            <section class="tnit-testimonial-section tnit-scaletop pd-tb70">
                <div class="container">
                    <!--Heading Outer start-->
                    <div class="tnit-heading-outer">
                        <h2><?php echo esc_html( get_option( 'my_voice_testimonials_section_title' ) ); ?></h2>
                    </div><!--Heading Outer End-->
					<?php if ( ! empty( get_option( 'my_voice_testimonials_section' ) ) ) : ?>
                        <!--Testimonial Slider Start-->
                        <div id="tnit-testimonial-slider" class="owl-carousel">
                            <!--Testimonial Item Start-->
							<?php
							/*This returns a json so we have to decode it*/
							$slider_testimonial_list_decoded = json_decode( get_option( 'my_voice_testimonials_section' ) );
							foreach ( $slider_testimonial_list_decoded as $repeater_item ) {
								?>
                                <figure class="tnit-testimonial-item">
                                    <img src="<?php echo esc_url( $repeater_item->image_url ); ?>" alt="">
                                    <figcaption class="tnit-caption">
                                        <i class="fa fa-quote-left" aria-hidden="true"></i>
                                        <p><?php echo esc_html( $repeater_item->text ); ?></p>
                                        <h4><?php echo esc_html( $repeater_item->subtitle ); ?></h4>
                                        <span class="name"><?php echo esc_html( $repeater_item->title ); ?></span>
                                    </figcaption>
                                </figure><!--Testimonial Item End-->
								<?php
							}
							?>

                        </div>
					<?php endif; ?>
                </div><!--Testimonial Slider End-->
            </section><!--Testimonial Section End-->
		<?php endif; ?>
		<?php if ( 'true' === get_option( 'my_voice_about_certificate_hide' ) ) : ?>
			<!--Certification Section Start-->
			<section class="tnit-certificate-section pd-t70">
				<div class="container">
					<!--Heading Outer start-->
					<div class="tnit-heading-outer">
						<h2><?php echo esc_html( get_option( 'my_voice_certificate_section_title' ) ); ?></h2>
					</div><!--Heading Outer End-->
					<?php if ( ! empty( get_option( 'my_voice_certificate_section' ) ) ) : ?>
						<!--Certificate Start-->
						<div class="row">
							<?php
							/*This returns a json so we have to decode it*/
							$slider_certificate_list_decoded = json_decode( get_option( 'my_voice_certificate_section' ) );
							foreach ( $slider_certificate_list_decoded as $repeater_item ) {
								?>
								<!--Certificate Item Start-->
								<div class="col-md-3 col-sm-3 col-xs-12">
									<!--Certificate Item Start-->
									<div class="certificate-item">
										<img src="<?php echo esc_url( $repeater_item->image_url ); ?>" alt="">
									</div><!--Certificate Item End-->
								</div>
								<?php
							}
							?>
						</div>
					<?php endif; ?>
				</div>
			</section><!--Certification Section End-->
		<?php endif; ?>
        <?php
		while ( have_posts() ) :
			the_post();
			the_content();
		endwhile; // End of the loop.
		?>
	</div><!--Main Content End-->
<?php get_footer(); ?>