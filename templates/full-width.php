<?php
/**
 * Template Name: Full Width Template
 *
 * Page template for about.
 *
 * The template for displaying Full Width pages
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

get_header();

/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );

?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<div class="container">
			<!--Inner Video Outer Start-->
				<?php
				while ( have_posts() ) :
					the_post();
					the_content();
				endwhile; // End of the loop.
				?>
		</div>
	</div><!--Main Content End-->
<?php get_footer(); ?>