<?php
/**
 * Template Name: Course Template
 *
 * Page template for course.
 *
 * @since    1.0.0
 * @package My Voice
 */

get_header();

/*
 * Banner
 */
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Video Section Start-->
		<section class="tnit-courses-section pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-8 col-xs-12">
						<!--Course Top Outer Start-->
						<div class="tnit-course-top-outer">
							<!--Heading Outer start-->
							<div class="tnit-heading-outer">
								<h3><?php esc_html_e( 'Learning Classes', 'myvoice' ); ?></h3>
							</div><!--Heading Outer End-->
							<div class="tnit-tabs-rightouter">
								<span>
									<?php
									$args = array(
										'post_type'      => 'courses',
										'posts_per_page' => 12,
									);
									$loop = new WP_Query( $args );
									if ( $loop->post_count ) :

										esc_html_e( 'Showing ', 'myvoice' );
										echo esc_html( $loop->post_count );
										esc_html_e( ' of ', 'myvoice' );
										echo esc_html( $loop->found_posts );
										esc_html_e( ' Results', 'myvoice' );
									endif;
									?>
								</span>
								<!-- Nav tabs -->
								<ul class="nav nav-tabs tnit-tabs-nav" role="tablist">
									<li role="presentation" class="active">
										<a href="#tab1" aria-controls="tab1" data-toggle="tab">
											<i class="fa fa-th-large" aria-hidden="true"></i>
										</a>
									</li>
									<li role="presentation">
										<a href="#tab2" aria-controls="tab2" data-toggle="tab">
											<i class="fa fa-list" aria-hidden="true"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>

						<!--Tabs Outer Start-->
						<div class="tl-tabs-outer">
							<!-- Tab Holder Start-->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="tab1">
									<div class="row">
										<?php
										if ( $loop->have_posts() ) :
											/* Start the Loop */
											while ( $loop->have_posts() ) :
												$loop->the_post();

												/*
												 * Include the Post-Format-specific template for the content.
												 * If you want to override this in a child theme, then include a file
												 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
												 */
												get_template_part( 'template-parts/post/content', 'course' );

											endwhile;

										else :

											get_template_part( 'template-parts/post/content', 'none' );
										endif;
										?>
									</div>
								</div>

								<div role="tabpanel" class="tab-pane" id="tab2">
									<div class="tl-tabs-list-outer">
										<div class="row">
											<?php
											$args = array(
												'post_type'      => 'courses',
												'posts_per_page' => 12,
											);
											$loop = new WP_Query( $args );
											if ( $loop->have_posts() ) :
												/* Start the Loop */
												while ( $loop->have_posts() ) :
													$loop->the_post();

													/*
													 * Include the Post-Format-specific template for the content.
													 * If you want to override this in a child theme, then include a file
													 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
													 */
													get_template_part( 'template-parts/post/content', 'course-full-width' );

												endwhile;

											else :

												get_template_part( 'template-parts/post/content', 'none' );
											endif;
											?>
										</div>
									</div>
								</div>
							</div><!-- Tab Holder End-->
						</div><!--Tabs Outer End-->

						<!--Pagination Row Start-->
						<div class="tnit-pagination-row">
							<nav aria-label="navigation">
								<?php theme_pagination(); ?>
							</nav>
						</div>
						<!--Pagination Row End-->
					</div>
					<div class="col-md-3 col-sm-4 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php
							if ( is_active_sidebar( 'default-course-sidebar' ) ) {
								dynamic_sidebar( 'default-course-sidebar' );
							}
							?>

						</aside>
						<!--Sidebar Outer End-->
					</div>
				</div>
			</div>
		</section><!--Video Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>