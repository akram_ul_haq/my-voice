<?php
/**
 * Template Name: Home Template
 *
 * Page template for homepage.
 *
 * @since 	1.0.0
 * @package My Voice
 */

get_header();

?>

	<!--Main Content Start-->
	<div class="tnit-main-content">
		<?php
		get_template_part( 'template-parts/home/home', 'slider' );
		get_template_part( 'template-parts/home/home', 'about' );
		get_template_part( 'template-parts/home/home', 'features' );
		get_template_part( 'template-parts/home/home', 'video' );
		get_template_part( 'template-parts/home/home', 'facts' );
		get_template_part( 'template-parts/home/home', 'courses' );
		get_template_part( 'template-parts/home/home', 'blog' );
		get_template_part( 'template-parts/home/home', 'events' );
		get_template_part( 'template-parts/home/home', 'partners' );
		?>

	</div><!--Main Content End-->

<?php get_footer(); ?>
