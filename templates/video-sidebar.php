<?php
/**
 * Template Name: Video Sidebar Template
 *
 * Page template for Video Posts.
 *
 * @since    1.0.0
 * @package My Voice
 */
get_header();

/*
* Banner
*/
get_template_part( 'template-parts/banner/banner', 'image' );
?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Video Section Start-->
		<section class="tnit-video-section tnit-video-section_v2 tnit-video-section_v3 pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-9 col-sm-12 col-xs-12">
						<?php
						$tax_terms = get_terms( 'video-type' );
						foreach ( $tax_terms as $term ) {
							$args = array(
								'post_type'      => 'video',
								'posts_per_page' => 6,
								'tax_query'      => array(
									array(
										'taxonomy' => $term->taxonomy,
										'field'    => 'slug',
										'terms'    => $term->slug,
									),
								),
							);
							$loop = new WP_Query( $args );
							if ( $loop->have_posts() ) :

								?>
								<div class="tnit-video-inner-outer">
								<!--Heading Outer start-->
								<div class="tnit-heading-outer">
									<h2><?php echo esc_html( $term->name ); ?></h2>
								</div><!--Heading Outer End-->
								<div class="row">
								<?php

								/* Start the Loop */
								while ( $loop->have_posts() ) :
									$loop->the_post();

									/*
									* Include the Post-Format-specific template for the content.
									* If you want to override this in a child theme, then include a file
									* called content-___.php (where ___ is the Post Format name) and that will be used instead.
									*/
									get_template_part( 'template-parts/post/content', 'video' );

								endwhile;

							else :

								get_template_part( 'template-parts/post/content', 'none' );

							endif;
							?>
							</div>
							<div class="tnit-btn-holder">
								<a href="<?php echo esc_url( get_term_link( $term->term_taxonomy_id ) ); ?>" class="tnit-btn-style_v2"><?php esc_html_e( 'See All Videos', 'myvoice' ); ?></a>
							</div>
							</div>
							<?php
						}
						wp_reset_postdata();
						?>

					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<!--Sidebar Outer Start-->
						<aside class="tnit-sidebar-outer">
							<?php
							if ( is_active_sidebar( 'default-video-sidebar' ) ) {
								dynamic_sidebar( 'default-video-sidebar' );
							}
							?>

						</aside>
						<!--Sidebar Outer End-->
					</div>
				</div>
			</div>
		</section><!--Video Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>