<?php
/**
 * Template for displaying search forms in My Voice
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

?>
<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<!--Widget Start-->
<div class="widget widget-search">
	<!--Widget Form Start-->
	<form role="search" method="get" class="tnit-form-search" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="text" id="<?php echo $unique_id; ?>" placeholder="<?php echo esc_attr_x( 'Type your keywords', 'placeholder', 'myvoice' ); ?>" value="<?php echo get_search_query(); ?>" name="s"  />
		<button class="btn-submit" type="submit">
			<i class="fa fa-search" aria-hidden="true"></i>
		</button>
	</form><!--Widget Form End-->
</div><!--Widget End-->
