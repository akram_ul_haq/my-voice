<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package My Voice
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<!--Main Content Start-->
	<div class="tnit-main-content">
		<!--Page 404 Section Start-->
		<section class="tnit-404-section pd-tb70">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2 col-sm-offset-2">
						<!--404 Inner Content Start-->
						<div class="tnit-error-inner">
							<strong class="title">404</strong>
							<p><?php esc_html_e( 'We are extremely sorry for the inconvenience. Hopefully we will be back much faster that you think.', 'myvoice' ); ?></p>
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="tnit-btn-style1"><?php esc_html_e( 'Go Back', 'myvoice' ); ?></a><br><br>
							<?php get_search_form(); ?>
						</div>
					</div>
				</div>
			</div>
		</section><!--Page 404 Section End-->

	</div><!--Main Content End-->
<?php get_footer(); ?>
